#%%
# Computes light curve plots for the reference stars in the IR
# starting from data downloaded from ASSASN survey

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#%%
source_path='ESO354/Star_light_curve/'
gen_path='/Users/Ros/Google_Drive/Valparaiso/Light_curves/Data/SMARTS_Data/'
path=gen_path+source_path
output_file_folder='/Users/Ros/Google_Drive/Valparaiso/Light_curves/Data/My_light_curves_tables/'+source_path

# read csv file
data=pd.read_csv(output_file_folder+'light_curve_ASSASN.csv')
data.HJD=data.HJD-2400000
data.HJD=np.trunc(data.HJD).astype(int)
# Group info by band
obs_by_band = data.groupby('Filter')
obs_by_band.groups
#%%
data.columns
grouped_v=obs_by_band.get_group('V')
grouped_v=grouped_v.groupby('HJD')
flux_v=grouped_v['flux(mJy)'].agg(np.mean)
error_v=grouped_v['flux_err'].agg(np.std)

grouped_g=obs_by_band.get_group('g')
grouped_g=grouped_g.groupby('HJD')
flux_g=grouped_g['flux(mJy)'].agg(np.mean)
error_g=grouped_g['flux_err'].agg(np.std)
#%%
grouped_v.groups.keys()
#%%
with plt.style.context(['seaborn-white','seaborn-bright']):
   fig = plt.figure()
   ax1 = fig.add_subplot(111)
   ax1.set_ylim([12.2,13.7])
   plt.errorbar(grouped_v.groups.keys(), flux_v, fmt='o', ms=3)
   #plt.errorbar(mjd, k*dm+(flux-flux.mean())/flux.std(), yerr=err, fmt='o', ms=3)
#%%
with plt.style.context(['seaborn-white','seaborn-bright']):
   fig = plt.figure()
   ax1 = fig.add_subplot(111)
   #ax1.set_ylim([16.11560,16.11580])
   plt.errorbar(grouped_v.groups.keys(), error_v, fmt='o', ms=3)
   #plt.errorbar(mjd, k*dm+(flux-flux.mean())/flux.std(), yerr=err, fmt='o', ms=3)
#%%
with plt.style.context(['seaborn-white','seaborn-bright']):
   fig = plt.figure()
   ax1 = fig.add_subplot(111)
   ax1.set_ylim([9,10.5])
   plt.errorbar(grouped_g.groups.keys(), flux_g, fmt='o', ms=3)
   #plt.errorbar(mjd, k*dm+(flux-flux.mean())/flux.std(), yerr=err, fmt='o', ms=3)


#%%
