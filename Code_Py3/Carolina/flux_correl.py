#%%
#-*- coding: utf-8 -*-
#!/usr/bin/python
"""
Plot flux-flux corr for sim curves
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from statsmodels.formula.api import ols
from scipy.stats import pearsonr
from scipy.stats import spearmanr
import glob
from matplotlib.lines import Line2D
from adjustText import adjust_text
#import linmix

#cols= 4*["#1f77b4","#fda000","#e55171","#44aa99","#332288","#e6a1eb","#0b672a","#f6d740","#bb2aa7","#6ab1d1"]

gal='NGC4151'
taus=['1.0','726.0','2316.0','5000.0','9999.0']
Xs=[-9.999,-0.997,0.002,1.003,4.608]
#gal='NGC3783'
#taus=['1.0','5.0','271.0','1675.0','5000.0']
#Xs=[-1.607,-1.067,0.00013,1.0008,2.454]

base_path='/users/Ros/Google_drive/Valparaiso/Light_curves/Code_Py3/Carolina/'
path=base_path+'output/'+gal+'/'

#read light_curve
lc=pd.read_csv(base_path+'LightCurves/lightcurves/'+gal+'_lc.csv',header=0,usecols=['T(days)','flux_cont(ph/cm^2/s)','flux_cont_err(ph/cm^2/s)','flux_Fe(ph/cm^2/s)','flux_Fe_err(ph/cm^2/s)'],na_values='ul')

average_cont = lc['flux_cont(ph/cm^2/s)'].mean()
lc['flux_cont']=(lc['flux_cont(ph/cm^2/s)']-average_cont)/average_cont
lc['flux_cont_err']=lc['flux_cont_err(ph/cm^2/s)']/average_cont

average_line = lc['flux_Fe(ph/cm^2/s)'].mean()
lc['flux_Fe']=(lc['flux_Fe(ph/cm^2/s)']-average_line)/average_line
lc['flux_Fe_err']=lc['flux_Fe_err(ph/cm^2/s)']/average_line

# plot
fig,ax = plt.subplots(figsize=[9, 6])

for tau,X in zip(taus,Xs):
   curve = pd.read_csv(path+f'sim_LC_tau{tau}_1.csv',squeeze=True,index_col=0) 

   model_fit = ols(formula="smooth_curve_noise ~ sim_curve_noise", data=curve).fit()
  # print(model_fit.summary())
   a0 = model_fit.params['Intercept']
   a1 = model_fit.params['sim_curve_noise']
   e0 = model_fit.bse['Intercept']
   e1 = model_fit.bse['sim_curve_noise']
   intercept = a0
   slope = a1
   uncertainty_in_intercept = e0
   uncertainty_in_slope = e1

   ax.plot(curve.sim_curve_noise,intercept+slope*curve.sim_curve_noise,zorder=0)
   ax.scatter(curve.sim_curve_noise,curve.smooth_curve_noise,label=f'X({tau})={X}, slope={slope:.3f}',edgecolor='black')


model_fit = ols(formula="flux_Fe ~ flux_cont", data=lc).fit()
# print(model_fit.summary())
a0 = model_fit.params['Intercept']
a1 = model_fit.params['flux_cont']
e0 = model_fit.bse['Intercept']
e1 = model_fit.bse['flux_cont']
intercept = a0
slope = a1
uncertainty_in_intercept = e0
uncertainty_in_slope = e1
ax.plot(lc['flux_cont'],intercept+slope*lc['flux_cont'],zorder=0)
ax.scatter(lc['flux_cont'],lc['flux_Fe'],label=f'Data, slope={slope:.3f}',marker='x')#,edgecolor='black'

#ax.set_yscale('log')
leg=ax.legend(title=gal)
leg._legend_box.align= "left"
ax.set_xlabel('Continuum flux (arbitrary units)')
ax.set_ylabel('Fe K$\\alpha$ flux (arbitrary units)')
plt.savefig(path+f'Flux_flux_{gal}.pdf', format = 'pdf', bbox_inches = 'tight',transparent=True) ;

#%%
# plot all best tau simulations
gals_df = pd.read_csv(base_path+'output/mass_size_rel.csv',squeeze=True,header=0,index_col=0) 
text_pars=dict(horizontalalignment='right', verticalalignment='bottom', bbox=dict(facecolor='gray', alpha=0.5))
for gal, row in gals_df.iterrows():
   if np.isnan(row.tau_best):
      #print('skipping ',gal)
      gals_df.loc[gal,'sim_slope_14']=np.nan
      gals_df.loc[gal,'sim_slope_50']=np.nan
      gals_df.loc[gal,'sim_slope_86']=np.nan
      gals_df.loc[gal,'data_slope']=np.nan
      gals_df.loc[gal,'data_slope_err']=np.nan
      continue
   
   #print(row.index)
   path=base_path+'output/'+gal+'/'
   lc_paths=glob.glob(path+f'sim_LC_tau{row.tau_best}_*.csv')
   lc_paths=sorted(lc_paths)

   # plot
   fig,(ax,ax2) = plt.subplots(ncols=2,figsize=[14, 6])
   slopes=np.empty(len(lc_paths))
   slopes[:] = np.NaN
   for i,lc_p in enumerate(lc_paths):
      curve = pd.read_csv(lc_p,squeeze=True,index_col=0) 

      #model_fit = ols(formula="smooth_curve_noise ~ sim_curve_noise", data=curve).fit()
      #a0 = model_fit.params['Intercept']
      #a1 = model_fit.params['sim_curve_noise']
      #e0 = model_fit.bse['Intercept']
      #e1 = model_fit.bse['sim_curve_noise']
      #intercept = a0
      #slope = a1
      #uncertainty_in_intercept = e0
      #uncertainty_in_slope = e1

      # polyfit returns Polynomial coefficients, highest power first
      notnullpoints = np.logical_not(curve.smooth_curve_noise.isnull())
      coeffs =np.polyfit(curve.sim_curve_noise[notnullpoints], curve.smooth_curve_noise[notnullpoints], deg=1)
      #print(coeffs)
      slopes[i]=coeffs[0]

      ax.plot(curve.sim_curve_noise,coeffs[1]+coeffs[0]*curve.sim_curve_noise,zorder=0, c='C0',alpha=0.2)

   gals_df.loc[gal,'sim_slope_14']=np.percentile(slopes,14)
   gals_df.loc[gal,'sim_slope_50']=np.percentile(slopes,50)
   gals_df.loc[gal,'sim_slope_86']=np.percentile(slopes,86)

   #read observed light_curve
   lc=pd.read_csv(base_path+'LightCurves/lightcurves/'+gal+'_lc.csv',header=0,usecols=['T(days)','flux_cont(ph/cm^2/s)','flux_cont_err(ph/cm^2/s)','flux_Fe(ph/cm^2/s)','flux_Fe_err(ph/cm^2/s)'],na_values='ul')

   average_cont = lc['flux_cont(ph/cm^2/s)'].mean()
   lc['flux_cont']=(lc['flux_cont(ph/cm^2/s)']-average_cont)/average_cont
   lc['flux_cont_err']=lc['flux_cont_err(ph/cm^2/s)']/average_cont

   average_line = lc['flux_Fe(ph/cm^2/s)'].mean()
   lc['flux_Fe']=(lc['flux_Fe(ph/cm^2/s)']-average_line)/average_line
   lc['flux_Fe_err']=lc['flux_Fe_err(ph/cm^2/s)']/average_line
   #print(lc[['flux_cont','flux_cont_err','flux_Fe','flux_Fe_err']])

   """
   # fit and plot observed light curve with LinMix
   delta = lc.flux_Fe_err.isnull()
   notdelta = np.logical_not(delta)
   ycens = lc.flux_Fe.copy()
   ycens_err = lc.flux_Fe_err.copy()
   ycens[notdelta] = 8.
   ycens_err[delta] = 1.
   print(lc.flux_cont_err)
   linmix_df=pd.DataFrame([lc.flux_cont, lc.flux_cont_err, ycens, ycens_err, delta],columns=['flux_cont','flux_cont_err','flux_fe','flux_Fe_err','delta'])
   print(linmix_df)
   lmcens  = linmix.LinMix(lc.flux_cont, ycens, lc.flux_cont_err, ycens_err, delta=delta, K=2)
   lmcens.run_mcmc(silent=True)
   fig,ax = plt.subplots(figsize=[7, 6])
   ax.errorbar(lc.flux_cont[delta], ycens[delta], lc.flux_cont_err[delta], lc.flux_Fe_err[delta], ls=' ')
   ax.errorbar(lc.flux_cont[notdelta], ycens[notdelta], yerr=0.3, uplims=np.ones(sum(notdelta), dtype=bool), ls=' ', c='b')
   print('length:',len(lmcens.chain))
   xs = np.arange(lc.flux_cont[notdelta].min()-1, lc.flux_cont[notdelta].max()+1)
   for i in range(0, len(lmcens.chain), 25):
      ys = lmcens.chain[i]['alpha'] + xs * lmcens.chain[i]['beta']
      ax.plot(xs, ys, color='g', alpha=0.02)
   ax.set_xlabel(r'Flux cont')
   ax.set_ylabel(r'Flux Fe')
   ax.set_ylim(lc.flux_Fe[notdelta].min()-1, lc.flux_Fe[notdelta].max()+1)
   fig.tight_layout()
   plt.savefig(path+f'Flux_flux_{gal}_besttau_censfit.pdf', format = 'pdf', bbox_inches = 'tight',transparent=True) ;
   plt.show()
   """
   if gal=='NGC4151':
      print(gal)
      #print(flux_cont_good.values)
      #[-0.4469363,  -0.62044516, -0.61308169, -0.61038859,  0.34688273,  0.34053851, 0.98976225,  0.96186327,  1.44869849, -0.64239351,-0.16740101, -0.71191746, 0.51591833, -0.4566805,  -0.34686438, -0.26099961,  0.2802051,   0.80065091, 0.65691045,  0.96184224, -0.28017695,  0.23787832,  0.73122551, -0.41889278,-0.51380583, -0.61044636, -0.4068905,  -0.61375565, -0.31541052, -0.25634708, -0.63786361,  0.09584125,  0.11817549, -0.19655622]
      #print(flux_Fe_good.values)
      #[-0.21088474, -0.26066343, -0.25889428, -0.33355753, -0.27432222, -0.37940849, 0.02361321,  0.12817831,  0.12024037, -0.07824785, -0.11133282, -0.56489666,-0.35368384,  0.15586409, -0.10986924,  0.12567069,  0.22097218,  0.08701887, -0.07756072,  0.04282619, -0.00147804,  0.08635398,  0.07066631, -0.32743597, -0.43608759 , 0.13834053, 0.27036967,  0.10910768,  0.26399728,  0.37425312, 0.22448655, 0.71430751,  0.73301576, 0.64618741]
      #print('#####')
      #print(lc.flux_Fe_err[notnullpoints].values)
      #[0.07068624, 0.02229843, 0.02468403, 0.01414388, 0.05837639, 0.04050687, 0.04991337, 0.05007025, 0.05355754, 0.02113535, 0.02451348, 0.04985432, 0.08046161, 0.25332281, 0.18560596, 0.05322704, 0.08196002, 0.06701242, 0.06595101, 0.07463964, 0.046916,   0.07164848, 0.07093502, 0.03671978, 0.05130404, 0.02647549, 0.0279981,  0.02287296,0.02945345, 0.02971549, 0.02357981, 0.03341991, 0.03501619, 0.03083194]

      #LINFIT( X, Y, CHISQR=chisqr, /DOUBLE, MEASURE_ERRORS=yerr)
   #   print(lc.flux_cont_err.isnull())
   #   print(lc.flux_Fe_err.isnull())


   #model_fit = ols(formula="flux_Fe ~ flux_cont", data=lc).fit()   
   #a0 = model_fit.params['Intercept']
   #a1 = model_fit.params['flux_Fe']
   #e0 = model_fit.bse['Intercept']
   #e1 = model_fit.bse['flux_Fe']
   #intercept = a0
   #slope = a1
   #uncertainty_in_intercept = e0
   #uncertainty_in_slope = e1
   #print('stats_pkg:',slope,intercept)

   notnullpoints = np.logical_not(lc.flux_Fe_err.isnull())
   flux_cont_good=lc.flux_cont[notnullpoints]
   flux_Fe_good=lc.flux_Fe[notnullpoints]
   w=1/lc.flux_Fe_err[notnullpoints]

   pearson_coeff=pearsonr(flux_cont_good,flux_Fe_good)
   spearmanrank=spearmanr(flux_cont_good,flux_Fe_good,nan_policy='omit')
   everything =np.polyfit(flux_cont_good, flux_Fe_good, deg=1, full=True, w=w)
   coeffs=everything[0]
   chisqr=everything[1]
   slope=coeffs[0]
   ax.plot(lc['flux_cont'],coeffs[1]+slope*lc['flux_cont'],zorder=0, c='C1')
   #ax.scatter(lc['flux_cont'],lc['flux_Fe'],label=f'Data, slope={slope:.3f}',edgecolor='black',marker='x', c='C1')
   ax.errorbar(lc.flux_cont,lc.flux_Fe,label=f'Data, slope={slope:.3f}',
            xerr=lc.flux_cont_err, yerr=lc.flux_Fe_err,
            xuplims=lc.flux_cont_err.isnull(), uplims=lc.flux_Fe_err.isnull(),
            marker='o', markersize=3,linestyle='none',c='C1',markeredgecolor='Black')
   gals_df.loc[gal,'data_slope']=slope
   gals_df.loc[gal,'data_slope_err']=uncertainty_in_slope

   #ax.set_yscale('log')
   legend_elements = [Line2D([0], [0], marker='o', color='C1', lw=1.5, markersize=3,markeredgecolor='Black', label=f'Data, slope={slope:.3f}', markerfacecolor='C1'),
                   Line2D([0], [0], lw=1.5, color='C0', label=f'Simulations, $\\tau$={row.tau_best:.0f} days')]

   # hand written simple least squares fit
   N_points=flux_cont_good.shape[0]
   # delta=N_points*np.sum(flux_cont_good**2)-np.sum(flux_cont_good)**2
   # slope=(N_points*np.sum(flux_cont_good*flux_Fe_good)-np.sum(flux_cont_good)*np.sum(flux_Fe_good))/delta
   # intercept=(np.sum(flux_cont_good**2)*np.sum(flux_Fe_good)-np.sum(flux_cont_good)*np.sum(flux_cont_good*flux_Fe_good))/delta
   # print('normal mean square:',slope,intercept)
   # ax.plot(flux_cont_good,intercept+slope*flux_cont_good, c='C3',label=f'normal mean square')

   # hand written weighted least squares fit
   # w=1/(lc.flux_Fe_err[notnullpoints])**2
   # delta=np.sum(w)*np.sum(w*flux_cont_good**2)-np.sum(w*flux_cont_good)**2
   # slope=(np.sum(w)*np.sum(w*flux_cont_good*flux_Fe_good)-np.sum(w*flux_cont_good)*np.sum(w*flux_Fe_good))/delta
   # intercept=(np.sum(w*flux_cont_good**2)*np.sum(w*flux_Fe_good)-np.sum(w*flux_cont_good)*np.sum(w*flux_cont_good*flux_Fe_good))/delta
   # print('weighted mean square:',slope,intercept)
   # red_chi_sq=np.sum((flux_Fe_good-intercept-slope*flux_cont_good)**2*w)/(N_points-2)
   # ax.plot(flux_cont_good,intercept+slope*flux_cont_good, c='C4',label=f'weighted mean square,red$\\chi^2$={red_chi_sq}')

   # numpy polyfit weighted fit
   #w=1/lc.flux_Fe_err[notnullpoints]
   #everything =np.polyfit(flux_cont_good, flux_Fe_good, deg=1, full=True, w=w)#
   #coeffs=everything[0]
   #print(everything[1])
   residuals=chisqr/(N_points-2)
   #print('np.polyfit',coeffs)
   #print(residuals)
   #ax.plot(flux_cont_good,coeffs[1]+coeffs[0]*flux_cont_good, c='C2',label=f'np.polyfit, red$\\chi^2$={residuals}')

   ax.text(0.965, 0.03,f"Pearson's $\\rho$={pearson_coeff[0]:.2f}\nSpearman's rank $\\rho$={spearmanrank[0]:.2f}\n $\~\\chi^2$={residuals[0]:.2f}", transform=ax.transAxes, **text_pars)
   leg=ax.legend(handles=legend_elements, title=gal,loc='upper left')
   leg._legend_box.align= "left"
   #ax.legend()
   ax.set_xlabel('Continuum flux (arbitrary units)')
   ax.set_ylabel('Fe K$\\alpha$ flux (arbitrary units)')

   ax2.hist(slopes,15, color='C0')
   ax2.axvline(slope,label='Data slope', c='C1')
   ax2.set_xlabel('Simulated slopes distribution')
   ax2.set_ylabel('Frequency')

   plt.savefig(path+f'Flux_flux_{gal}_besttau.pdf', format = 'pdf', bbox_inches = 'tight',transparent=True) ;

#gals_df.to_csv(base_path+'output/mass_size_rel.csv')
#%%
# plot slopes correl
texts = []
fig,ax = plt.subplots(figsize=[9, 6])
ax.plot([gals_df.data_slope]*2,[gals_df.sim_slope_14,gals_df.sim_slope_86])
ax.plot([gals_df.data_slope-gals_df.data_slope_err,gals_df.data_slope+gals_df.data_slope_err],[gals_df.sim_slope_50]*2)
ax.scatter(gals_df.data_slope, gals_df.sim_slope_50)
ax.plot([-1,1],[-1,1.])
ax.set_xlabel('$F_{Fe\,K\\alpha} - F_{2-10keV}$ observed slope')
ax.set_ylabel('$F_{Fe\,K\\alpha} - F_{2-10keV}$ simulated slope')
for key, row in gals_df.iterrows():
   if np.isnan(row.tau_best):
      texts.append(plt.text(-0.5, -0.5, ''))
      continue
   texts.append(plt.text(row.data_slope, row.sim_slope_50, key, size=8))
adjust_text(texts)
plt.savefig(base_path+'Flux_flux_all_besttau.pdf', format = 'pdf', bbox_inches = 'tight',transparent=True) 