#%%
"""
Macro inspired by ../Code/calibrate_fluxes.py but improved and translated to Py3
Reads optical light curves extracted by Patricia and calibrates them using fluxes of stars in the fields.

Assumes light curve files are named 'LC_raw_'+band+'.qdp' and outputs a calibrated light curve named 'LC_cal_{band}.txt' in the same input path.

Converts the magnitudes of the stars to fluxes using zero point magnitudes from Gemini (see link below).
"""
import numpy as np
import pandas as pd
#%%
### select gal from dictionary below
gal="MRK509"
band="I"
#%%
names_dic={'ESO244':"ESO244-G17", 'ESO354':"ESO354-G04",'ESO434':"ESO434-G40", 'HE0502':"HE0502-2948", 'IRAS01089':"IRAS01089-4743", 
          'IRAS01267':"IRAS01267-2157", 'IRAS04505':"IRAS 04505-2958", 'IRAS16355':"IRAS 16355-2049", 'IRASL06229':"IRASL06229-6434", 
           'LB1727':"LB1727", 'MRK509':"MRK 509", 'PKS0521':"PKS0521-36", 'TONS180':"TON S 180"}
gal_name=names_dic.get(gal)
# define a common path to the light curve files:
p="../Data/Optical_light_curves_tables/"+gal+'/'
lc1=p+'LC_raw_'+band+'.qdp'
#%%
# Magnitude-flux conversion
# Johnson filters are Vega magnitudes
# https://www.gemini.edu/sciops/instruments/midir-resources/imaging-calibrations/fluxmagnitude-conversion
Zero_points={'B':4266.7, 'V':3836.3, 'I':2247.4, 'R':2841} # [Jy]
#%%
all_stars={}

#from catalogs TONS180
B_stars=[1,2,4,5,6]
I_stars=[2,3,4,5]
TONS180={'B':[17.77,16.32,18.38,17.22,16.47,17.94],
        'V':[16.67,14.49,np.nan,16.75,15.86,17.0],
        'I':[14.95,13.85,17.02,15.93,15.66,15.78],
        'extended':[False,False,False,False,False,False]}
#from NED 2011ApJS..193...28P "The LCO/WFCCD Galaxy Survey of 20 Fields Surrounding UV-bright Quasars"
TONS180_LCO={'Bmag':[18.3,16.39,20.2,17.69,16.83,18.33],'Bmag_err':[0.1,0.09,0.11,0.09,0.08,0.09],
         'Bflux':[2.04e-4,1.18e-3,3.54e-5,3.58e-4,7.9e-4,1.98e-4],'Bflux_err':[1.88e-5,9.82e-5,3.59e-6,2.96e-5,5.82e-5,1.64e-5],
        'extended':[False,False,False,False,False,False]}
#ned_names=['WISEA J005728.27-222347.1','WISEA J005716.29-222330.6','WISEA J005718.45-222133.2','WISEA J005721.33-222058.7','WISEA J005715.62-222040.6','WISEA J005709.60-222104.2']
all_stars['TONS180']=pd.DataFrame(data=TONS180,index=np.arange(len(TONS180['B']))+1)

# HE0502-2948
# Nomad + Tess input catalog
HE0502 ={'B':[15.98,18.38,17.91,17.75],
        'R':[14.67,16.81,16.53,15.29],
        'extended':[False,False,False,False]}
#ned_names=['WISEA J050425.46-294420.1','WISEA J050426.90-294223.4','WISEA J050420.61-294529.7','WISEA J050408.81-294408.5']
all_stars['HE0502']=pd.DataFrame(data=HE0502,index=np.arange(len(HE0502['B']))+1)

# IRAS01267-2157
# Nomad + USNO-B1.0 + Tess input catalog
IRAS01267 ={'B':[16.29,18.35,16.57,16.13],
        'I':[15.0,14.79,15.46,14.77],
        'extended':[False,False,False,False]}
#ned_names=['WISEA J012913.24-214150.0','WISEA J012915.11-214111.3','WISEA J012915.96-214052.3','WISEA J012902.24-213942.8']
all_stars['IRAS01267']=pd.DataFrame(data=IRAS01267,index=np.arange(len(IRAS01267['B']))+1)

# IRAS 04505-2958
# Nomad + Tess input catalog
IRAS04505 ={'B':[15.02,18.29,16.62,15.41],
            'V':[14.46,17.44,15.98,14.97],
            'extended':[False,False,False,False]}
#ned_names=['WISEA J045240.17-295313.4','WISEA J045226.70-295241.8','WISEA J045222.60-295339.3','WISEA J045225.89-295149.8']
all_stars['IRAS04505']=pd.DataFrame(data=IRAS04505,index=np.arange(len(IRAS04505['B']))+1)

# MRK 509
# Nomad + USNO-B1.0 + Tess input catalog
MRK509 ={'B':[12.895,14.82,17.31,16.42],
         'I':[12.55,13.83,15.76,15.87],
         'extended':[False,False,False,False]}
#ned_names=['WISEA J204411.21-104152.4','WISEA J204415.06-104441.8','WISEA J204412.78-104315.3','WISEA J204406.50-104310.8']
all_stars['MRK509']=pd.DataFrame(data=MRK509,index=np.arange(len(MRK509['B']))+1)

# 
# Nomad + USNO-B1.0 + Tess input catalog
#IRAS01267 ={'B':[,,,],
#        'I':[,,,],
#        'extended':[False,False,False,False]}
#ned_names=['WISEA ','WISEA ','WISEA ','WISEA ']
#all_stars['IRAS01267']=pd.DataFrame(data=IRAS01267,index=np.arange(len(IRAS01267['B']))+1)

stars=all_stars[gal]
#%%
# mag to fluxes
stars[band+'_flux']=Zero_points[band]*10**(-stars[band]/2.5)
print(stars[[band,band+'_flux']])
#%%
# leggo cat e calibro
stars_total=stars[band+'_flux'].sum()
print(stars_total)

mjd1, flux1, err1 = np.loadtxt(lc1, unpack = True, usecols = [0, 1, 2])
flux1 = flux1*stars_total
err1= err1*stars_total

#save to output file
cal_LC = pd.DataFrame(data={'flux':flux1,'err':err1},index=mjd1)
cal_LC.index.name='MJD'

cal_LC.to_csv(p+f'LC_cal_{band}.txt',sep='\t')
# read with:
# cal_LC = pd.read_csv(p+f'LC_cal_{band}.txt',sep='\t',header=0,index_col=0)

mean_flux=np.median(flux1)
print(mean_flux)
# %%


# %%
