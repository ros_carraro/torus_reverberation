#%%
"""
Funzione che simula curve di luce a partire da un power spectrum con forma di power law su cui si introduce del rumore.
(float) t_min, T_max, dt - starting time, end time and time interval
power law parameters: A normalization, alpha exponent (float)
seeds (int, 2el array) - seeds for the simulation of the Re and Im noise
flux_ave - average value for normalization of simulated curve
flux_err - error in % of flux_ave units.
"""
#!/usr/bin/python
#-*- coding: utf-8 -*-

import numpy as np
import scipy
from matplotlib import pyplot as plt
from matplotlib import ticker
import pyfftw


def curve_simul(tau,ifft,ifft_array,T_max=5001.,t_min=1.,dt=1.,freq_nu=3,alpha_h=2.,alpha_l=1,A=1e-3,seeds=[123,124]):

########################################
### Parameter definition
########################################
# Fundamental vars
#T_max=1001. # (days) time covered by the simulated curve
#t_min= 1 # (days) initial time - can't be 0 or freq becomes infinite
#dt= 1. # (days) simulation time
#alpha= 2. # power law index
#A = 1e-3 # power law normalization
#seeds=[123,124]

# Derived vars
   #times= np.arange(t_min,T_max,dt)
   N= int((T_max-t_min)/dt)
   idx=int(N/2)
   times= np.arange(t_min,2*idx*dt+t_min,dt)
   indices = np.arange(1,idx+1)
   freq=indices/(T_max-t_min)
   #print('times last',times[-1])
   #print('tmax',T_max)
   #print('tmin',t_min)
   #print('max',freq[-1],np.max(freq))
   #print('min',freq[0],np.min(freq))

########################################
### Power spectrum calculation
########################################
   np.random.seed(seeds[0])
   Re=np.random.normal(size=idx)
   np.random.seed(seeds[1])
   Im=np.random.normal(size=idx)

   #def find_nearest(array, value):
   # array = np.asarray(array)
   ## idx = (np.abs(array - value)).argmin()
   # return idx
   #idx=find_nearest(freq, 0.01)

   PS=(A*freq**-alpha_l)/(1+(freq/freq_nu)**(alpha_h-alpha_l))#*N/(2.0*dt)
   #PS=np.zeros(len(freq))
   #PS[idx]=N

   # light curve
   Sim_PS_pos=(Re+1j*Im)*(PS/2)**0.5
   Sim_PS_neg=(Re-1j*Im)*(PS/2)**0.5
   Sim_PS_neg=Sim_PS_neg[::-1]

   #smoothed light curve
   sinc=np.sinc(freq*tau)
   Sim_PS_pos_sm=Sim_PS_pos*sinc
   Sim_PS_neg_sm=Sim_PS_neg*sinc[::-1]
   PS_smooth=PS*sinc**2

   fig, ax = plt.subplots()
   # plt.plot(freq,PS)
   # plt.plot(freq,Sim_PS_pos.real)
   ax.plot(freq,freq*((Sim_PS_pos.real)**2+(Sim_PS_pos.imag)**2), linewidth=0.2,label='Power spectrum realization')
   ax.plot(freq,freq*((Sim_PS_pos_sm.real)**2+(Sim_PS_pos_sm.imag)**2), linewidth=0.2,label='Smoothed power spectrum realization')
   ax.plot(freq,freq*PS,label='Power spectrum model')
   ax.plot(freq,freq*PS_smooth, linewidth=0.5,label='Smoothed power spectrum model')
   ax.set_yscale('log')
   ax.set_xscale('log')
   ax.minorticks_on()
   leg=ax.legend(title=f'$\\tau=${tau}',loc='lower left')
   leg._legend_box.align= "left"
   ax.set_xlabel('frequency (1/days)')
   ax.set_ylabel('power per frequency')
   ax.yaxis.set_major_locator(ticker.LogLocator(base=10, numticks=15))
   #ax.yaxis.grid(True, which='major')
   ax.set_ylim(bottom=1e-10,top=10)
   ax.tick_params(axis='both',which='both',direction='in',top=True,left=True,right=True)
   plt.savefig(f'power_spectrum_tau{tau}.pdf', 
               format = 'pdf', bbox_inches = 'tight',transparent=True)
   #plt.show;
   plt.close()

########################################
### Inverse FFT
########################################
   Sim_PS=np.append([0+0j],Sim_PS_pos)
   Sim_PS=np.append(Sim_PS,Sim_PS_neg)
   #print('length sim_ps',len(Sim_PS))
   ifft_array[:] = Sim_PS
   sim_curve = ifft().copy()
   #sim_curve=np.fft.ifft(Sim_PS)

   # smoothed
   Sim_PS_sm=np.append([0+0j],Sim_PS_pos_sm)
   Sim_PS_sm=np.append(Sim_PS_sm,Sim_PS_neg_sm)
   ifft_array[:] = Sim_PS_sm
   sim_curve_sm = ifft()
   #sim_curve_sm=np.fft.ifft(Sim_PS_sm)

########################################
### Plot simulated curve
########################################
   times=np.append(times,times[-1]+dt)
   # plt.figure()
   # plt.minorticks_on()
   # plt.plot(times,sim_curve.real)

   # plt.show;

########################################
### Add noise
########################################
# simulare errore poissoniano
# sommo valore medio flusso errore alla curva simulata
# e in ogni punto + gauss sim * tamano barra de error
# flux_ave=0.3
# flux_err=0.05
#   sim_curve_noise = (sim_curve + flux_ave)*(1 + np.random.normal(size=N+1)*flux_err)


########################################
### Plot simulated curve w/noise
########################################
   # plt.figure()
   # plt.minorticks_on()
   # plt.plot(times,sim_curve_noise.real)

   # plt.show;

   return (times,sim_curve.real,sim_curve_sm.real)

#%%
