#%%
"""
Reads VOTable with observations list for (dunno obj name)
downloaded from SMARTS archive and compares dates for 
IR and optical observations.
"""

from astropy.io.votable import parse
import numpy as np
from astropy.time import Time
from matplotlib import pyplot as plt

output_file_folder="../Optical_CCF/"
list_file="../Data/rows_as_votable_1543418255_3340.vot"
votable = parse(list_file)
table = votable.get_first_table()
data=table.array

#%%
print data.dtype
obs_date=data[['date_obs']]
obs_type=data[['dtacqnam']]
obs_date=obs_date.data
obs_type=obs_type.data
new_obs_date = []
new_obs_type= []
for i in range(len(obs_date)):
   new_obs_date.append(obs_date[i][0])
   new_obs_type.append(obs_type[i][0])

new_obs_date = np.array(new_obs_date)
new_obs_type = np.array(new_obs_type)

t = Time(new_obs_date, format='iso', scale='utc')
t_mjd=t.mjd

print(t_mjd[:10])

#%%
def custom_filter(search_arr,search_string):
   res = []
   for elem in search_arr:
      res.append(search_string in elem)
   return res

#%%
idx = np.where(custom_filter(new_obs_type,'ccd'))[0]
opt_dates=t_mjd[idx]

idx = np.where(custom_filter(new_obs_type,'ir'))[0]
ir_dates=t_mjd[idx]

np.array_equal(opt_dates,ir_dates)

#%%

fig = plt.figure()
ax1 = fig.add_subplot(111)

ax1.plot(opt_dates,np.zeros(len(opt_dates)),'o',ms=.5)
ax1.plot(ir_dates,np.zeros(len(ir_dates))+0.1,'o',ms=.5)
plt.ylim(-0.05,0.2)
plt.locator_params(axis='x', nbins=3)

plt.savefig(output_file_folder+'dates_comp.pdf', format = 'pdf', orientation = 'landscape', bbox_inches = 'tight') 
plt.close(fig)
fig
