#%%
"""
reads B light curves and multiplies the relative fluxes for the sums of the calibration stars fluxes obtained from online catalogs
"""
import numpy as np
#%%
# def nomi oggetti e path
object_name=["LB1727","TONS180","ESO354-G004"]

# define a common path to the light curve files:
p="../Data/Light_curves_tables/Optical/"
lc1=[p+"bin_LB1727_B.qdp", p+"bin_TONS180_B.qdp",p+"bin_ESO354_B.qdp"]

#lc1_n=[p+"TONS180_calib.qdp"]

# stars mags from catalog downloaded with Topcat:
# VO->VizierCatalogueService->Optical->Nomad(Zacharias+05)
# and then sent to Aladin
stars_mags=[[16.39,15.2,18.04,17.82],
            [17.77,16.32,17.22,16.47],
            [15.21,14.33,18.24,17.72]] # B band
# fluxes obtained with iObserve            
stars_fluxes=[[1.16,3.46,0.253,0.310],
               [0.325,1.23,0.539,1.08],
               [3.06,6.87,0.188,0.303]] # mJy
stars_fluxes[0]
mean_eso=0.104
#%%
# ciclo for sugli oggetti
for i in range(len(object_name)):
   stars_total=sum(stars_fluxes[i])
   print stars_total

   mjd1, flux1, err1 =  np.loadtxt(lc1[i], unpack = True, usecols = [0, 1, 2])
   flux1 = flux1*stars_total
   err1= err1*stars_total

   # data1 = np.array([mjd1, flux1, err1])
   # data1 = data1.T

   # header='Calibration of the light curve. Total flux of calibration stars is {0} mJy'.format(stars_total)

   # with open(lc1_n[i], 'w+') as datafile_id:
   #    np.savetxt(datafile_id, data1, delimiter=' ', header=header)

   mean_flux=np.median(flux1)
   print mean_flux

#%%
# for ['NGC 3783','MR 2251-178','MCG-6-30-15'] we take mean flux from the papers
#in 1e-15*erg/s/cm^2/A
plot_fluxes= np.array([25.,8.,3.3])
# conversion from http://www.stsci.edu/~strolger/docs/UNITS.txt
# [Y Jy] = 3.33564095E+04 * [X1 erg/cm^2/s/A] * [X2 A]^2
jansky_fluxes=3.33564e4*3598.5**2*plot_fluxes*1e-15*1e3 # mJyobject_name=object_name+['NGC 3783','MR 2251-178','MCG-6-30-15']
# galaxy magnitude obtained with mean_flux and with for Johnson B http://ssc.spitzer.caltech.edu/warmmission/propkit/pet/magtojy/

#%%
mag=np.array([15.3,14.7,19,14,15.2,16.2])
# average distance modulus from NED search, redshifts
distance_modulus=np.array([38.3,37.17,32.5,33,37.25,32.5])
M=mag-distance_modulus
print M



#%%
