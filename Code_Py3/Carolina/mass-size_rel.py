import glob
#import re
#from typing import NoReturn
import pandas as pd
import matplotlib.pyplot as plt
#from statsmodels.formula.api import ols
import numpy as np
from adjustText import adjust_text
import statsmodels.api as sm
from scipy import interpolate
from scipy.integrate import quad
from math import ceil

cols= 4*["#1f77b4","#fda000","#e55171","#44aa99","#332288","#e6a1eb","#0b672a","#f6d740","#bb2aa7","#6ab1d1"]

# read and convert to dataframe list of galaxies + taus
path='/Users/Ros/Google_Drive/Valparaiso/Light_curves/Code_Py3/Carolina/'

paths=glob.glob(path+'output/X_tau/X_tau_*.csv')
paths = sorted(paths)
gals=[p[(p.rfind('_')+1):p.rfind('.csv')] for p in paths]

gals_df=pd.DataFrame(data=paths,index=gals,columns=['path'])
gals_df["tau_inf"] = np.nan
gals_df["tau_best"] = np.nan
gals_df["tau_sup"] = np.nan

def find_X(X,root=0):
   # find where X(tau_i)*X(tau_i+1)<0
   X.sort_index(inplace=True)
   X_val=X.values-root
   abs_sign_diff = np.abs(np.diff(np.sign(X_val)))
   change_idx = np.flatnonzero(abs_sign_diff == 2)
   change_idx = np.append(change_idx, change_idx + 1)
   if len(change_idx)>2:
      print(f"Warning! More than 1 change of sign for root={root}")
   #print(X.iloc[change_idx])
   change_idx=change_idx[0:2]
   X_chsign = X.iloc[change_idx]-root
   #X_root=X[X_chsign.abs().idxmin()]
   tau_root=X_chsign.abs().idxmin()
   return tau_root

for gal,row in gals_df.iterrows():
   X = pd.read_csv(row.path,squeeze=True,index_col=0)
   #print(X)

   # look for X=1 
   X.sort_index(inplace=True)
   if (X.iloc[0]<1) and (X.iloc[-1]>1):
      ret=find_X(X,root=1)
      gals_df.loc[gal, 'tau_sup']=ret

   # look for X=0
   if (X.iloc[0]<0) and (X.iloc[-1]>0):
      ret=find_X(X,root=0)
      gals_df.loc[gal, 'tau_best']=ret

   # look for X=-1
   X.sort_index(inplace=True)
   if (X.iloc[0]<-1) and (X.iloc[-1]>-1):
      ret=find_X(X,root=-1)
      gals_df.loc[gal, 'tau_inf']=ret

def uniform(x,min,max):
   # generate points with uniform distribution in a range, with normalization 1/2
   return np.full_like(x,1/(max-min)/2)

def gauss(x, mean, sigma):
   return np.exp(-(x - mean) ** 2. / (2.*sigma ** 2.))/(sigma*(2*np.pi)**.5)

def resample(var,var_low=None,var_upp=None,upp_lim=1e4,low_lim=1e-3,size=400):
   n_data=len(var)
   samples=np.zeros((size,n_data))
   binning=1

   # make plots of prob distributions
   n_cols=3
   n_rows= int(ceil(n_data / n_cols))
   fig,axs = plt.subplots(n_rows,n_cols,figsize=[5*n_cols, 5*n_rows])
   axs=axs.flatten()

   for i in range(n_data):
      # draw normal random points when confidence range,
      # draw uniform distrib when upper/lower limit
      if ((np.isnan(var_low[i])) and (np.isnan(var_upp[i]))) or (np.isnan(var[i])):
         print(f'Skipping {gals_df.index[i]}')
         continue

      # generate prob distrib
      if np.isnan(var_low[i]):
         # if upper limit
         x1 = np.arange(low_lim, var[i], binning)
         prob1=uniform(x1,low_lim, var[i])
      else:
         # if 1sigma range is defined
         x1 = np.arange(var_low[i], var[i], binning)
         norm_fac=quad(gauss, np.min(x1), np.max(x1), args=(var[i], var[i]-var_low[i]))
         prob1=gauss(x1, var[i], var[i]-var_low[i])/norm_fac[0]/2

      if np.isnan(var_upp[i]):
         # if lower limit - upp and best
         x2 = np.arange(var[i], upp_lim, binning)
         prob2=uniform(x2, var[i], upp_lim)
      else:
         # if 1sigma range is defined
         x2 = np.arange(var[i], upp_lim, binning)
         norm_fac=quad(gauss, np.min(x2), np.max(x2), args=(var[i], var_upp[i]-var[i]))
         prob2=gauss(x2, var[i], var_upp[i]-var[i])/norm_fac[0]/2

      x = np.append(x1,x2)
      prob = np.append(prob1,prob2)
      cumulative=np.cumsum(prob*binning) 
      if (cumulative[-1] >1.01) or (cumulative[-1]<0.99):
         print(f'ATTENTION!!! Normalization went wrong, cumulative goes up to {cumulative[-1]} instead of 1')

      a = np.random.random(size)
      cum_law = interpolate.interp1d(cumulative, x, bounds_error=False)
      samples[:,i] = cum_law(a)  
      
      axs[i].hist(samples[:,i],alpha=0.8,density=True)
      axs[i].plot(x,prob)
      axs[i].text(x=0.9,y=1e-4,s=f'{gals_df.index[i]}')
      axs[i].set_yscale('log')
   plt.savefig(path+'output/tau_prob_distributions.pdf', format = 'pdf', bbox_inches = 'tight',transparent=True) ;
   return samples

"""
gals_df['tau'] = gals_df['tau'].astype(float)
gals_df['logTau']=np.log10(gals_df['tau'])
"""

# add Mbh column to dataframe
ps_pars=pd.read_csv(path+'LightCurves/info_BHmass_nuB_days.csv',index_col=0,header=0)
gals_df=gals_df.drop(['path'],axis=1).dropna(how='all')
gals_df=pd.merge(gals_df,ps_pars['logMbh(Msun)'], left_index=True, right_index=True)
gals_df.rename(columns={"logMbh(Msun)": "logMbh"},inplace=True)
mass_range=np.array([gals_df.logMbh.min(),gals_df.logMbh.max()])
#gals_df.drop(['path'],axis=1).to_csv(path+'/output/mass_size_rel.csv')
gals_df.to_csv(path+'/output/mass_size_rel.csv')

# resampling data according to confidence range/
gals_df.drop(['1H0707-495'],axis=0,inplace=True)
tau_sampling=resample(gals_df.tau_best,gals_df.tau_inf,gals_df.tau_sup)
#print(tau_sampling)
# we will work in log-sigma:
logtau_sampling = np.log10(tau_sampling)
#print(logtau_sampling)

gals_df['lolims']=(np.isnan(gals_df.tau_best)) & (~np.isnan(gals_df.tau_inf))
gals_df['uplims']=(np.isnan(gals_df.tau_best)) & (~np.isnan(gals_df.tau_sup))
texts=[]
plt.figure()
for key, row in gals_df.iterrows():
   if row.lolims:
      texts.append(plt.text(row.logMbh, np.log10(row.tau_inf), key, size=10))
   elif row.uplims:
      if key=='CircinusGalaxy':
         print(row.logMbh, row.tau_sup)
      texts.append(plt.text(row.logMbh, np.log10(row.tau_sup), key, size=10))
   else:
      texts.append(plt.text(row.logMbh, np.log10(row.tau_best), key, size=10))
adjust_text(texts, force_text=0.1)
for x, y in zip(gals_df.logMbh,logtau_sampling.T):
   # for each galaxy, plot a little cloud with its own colors
   x=np.full_like(y,x)
   plt.scatter(x, y, s=2, marker='x', alpha=0.4)
plt.scatter(gals_df.logMbh,np.log10(gals_df.tau_best),edgecolors='Black')
xlabel = 'Black hole mass [log, $M_\odot$]'
ylabel = '\\tau [log, days]'
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.ylim(bottom=8e-1)
plt.savefig(path+'output/tau_mass_sampling.pdf', format = 'pdf', bbox_inches = 'tight',transparent=True) ;

parameters = ['slope', 'offset', 'scatter']

def prior_transform(cube):
    # the argument, cube, consists of values from 0 to 1
    # we have to convert them to physical scales

    params = cube.copy()
    # let slope go from -3 to +3
    lo = -3
    hi = +3
    params[0] = cube[0] * (hi - lo) + lo
    # let offset go from 10 to 1000 km/s -- use log
    lo = np.log10(10)
    hi = np.log10(1000)
    params[1] = cube[1] * (hi - lo) + lo
    # let scatter go from 0.001 to 10
    lo = np.log10(0.001)
    hi = np.log10(10)
    params[2] = 10**(cube[2] * (hi - lo) + lo)
    return params

import scipy.stats

def log_likelihood(params):
    # unpack the current parameters:
    slope, offset, scatter = params

    # compute for each x point, where it should lie in y
    y_expected = (logtau_sampling[:,0] - 10) * slope + offset
    # compute the probability of each sample
    probs_samples = scipy.stats.norm(y_expected, scatter).pdf(logtau_sampling[:,1])
    # average over each galaxy, because we assume one of the points is the correct one (logical OR)
    probs_objects = probs_samples.mean(axis=1)
    assert len(probs_objects) == gals_df.shape[0]
    # multiply over the galaxies, because we assume our model holds true for all objects (logical AND)
    # for numerical stability, we work in log and avoid zeros
    loglike = np.log(probs_objects + 1e-100).sum()
    return loglike

import ultranest

sampler = ultranest.ReactiveNestedSampler(parameters, log_likelihood, prior_transform)
result = sampler.run(min_num_live_points=50, min_ess=100)
from ultranest.plot import cornerplot
cornerplot(sampler.results)

gals_df['lolims']=(np.isnan(gals_df.tau_best)) & (~np.isnan(gals_df.tau_inf))
gals_df['uplims']=(np.isnan(gals_df.tau_best)) & (~np.isnan(gals_df.tau_sup))
fig,ax = plt.subplots(figsize=[9, 6])
ax.scatter(gals_df.logMbh,gals_df.tau_best,edgecolor='Black')
#ax.scatter(gals_df.logMbh,gals_df.logTau,edgecolor='Black')
#ax.plot(mass_range,intercept+slope*mass_range,zorder=0)
ax.set_yscale('log')
texts = []
for key, row in gals_df.iterrows():
   if row.lolims:
      texts.append(plt.text(row.logMbh, row.tau_inf, key, size=10))
   elif row.uplims:
      if key=='CircinusGalaxy':
         print(row.logMbh, row.tau_sup)
      texts.append(plt.text(row.logMbh, row.tau_sup, key, size=10))
   else:
      texts.append(plt.text(row.logMbh, row.tau_best, key, size=10))
adjust_text(texts, force_text=0.1)
gals_df['sup_err']=gals_df.tau_sup-gals_df.tau_best
gals_df.loc[np.isnan(gals_df.sup_err),'sup_err']=15000-gals_df.loc[np.isnan(gals_df.sup_err),'tau_best']
gals_df['inf_err']=gals_df.tau_best-gals_df.tau_inf
gals_df.loc[np.isnan(gals_df.inf_err),'inf_err']=gals_df.loc[np.isnan(gals_df.inf_err),'tau_best']-0.001
errors=gals_df[['inf_err','sup_err']].values.T
plt.figure()
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.errorbar(gals_df.logMbh,gals_df.tau_best, yerr=errors,
             marker='o', ls=' ', color='orange')

from ultranest.plot import PredictionBand
x = np.linspace(9, 12.5, 400)
band = PredictionBand(x)
band_lo = PredictionBand(x)
band_hi = PredictionBand(x)

for params in sampler.results['samples'][:40]:
    slope, offset, scatter = params
    y = (x - 10) * slope + offset
    band.add(10**y)

    # indicate intrinsic scatter
    band_hi.add(10**(y + scatter))
    band_lo.add(10**(y - scatter))

band.shade(color='k', alpha=0.1)
band.line(color='k')
band_lo.line(color='r', ls='--')
band_hi.line(color='r', ls='--')

plt.yscale('log');


"""
# fit
model_fit = ols(formula="logTau ~ logMbh", data=gals_df).fit()
print(model_fit.summary())
a0 = model_fit.params['Intercept']
a1 = model_fit.params['logMbh']
e0 = model_fit.bse['Intercept']
e1 = model_fit.bse['logMbh']
intercept = a0
slope = a1
uncertainty_in_intercept = e0
uncertainty_in_slope = e1
"""

# plot "correlation"
gals_df['lolims']=(np.isnan(gals_df.tau_best)) & (~np.isnan(gals_df.tau_inf))
gals_df['uplims']=(np.isnan(gals_df.tau_best)) & (~np.isnan(gals_df.tau_sup))
fig,ax = plt.subplots(figsize=[9, 6])
ax.scatter(gals_df.logMbh,gals_df.tau_best,edgecolor='Black')
#ax.scatter(gals_df.logMbh,gals_df.logTau,edgecolor='Black')
#ax.plot(mass_range,intercept+slope*mass_range,zorder=0)
ax.set_yscale('log')
texts = []
for key, row in gals_df.iterrows():
   if row.lolims:
      texts.append(plt.text(row.logMbh, row.tau_inf, key, size=10))
   elif row.uplims:
      if key=='CircinusGalaxy':
         print(row.logMbh, row.tau_sup)
      texts.append(plt.text(row.logMbh, row.tau_sup, key, size=10))
   else:
      texts.append(plt.text(row.logMbh, row.tau_best, key, size=10))
adjust_text(texts, force_text=0.1)
gals_df['sup_err']=gals_df.tau_sup-gals_df.tau_best
gals_df.loc[np.isnan(gals_df.sup_err),'sup_err']=15000-gals_df.loc[np.isnan(gals_df.sup_err),'tau_best']
gals_df['inf_err']=gals_df.tau_best-gals_df.tau_inf
gals_df.loc[np.isnan(gals_df.inf_err),'inf_err']=gals_df.loc[np.isnan(gals_df.inf_err),'tau_best']-0.001
print(gals_df)
errors=gals_df[['inf_err','sup_err']].values.T
ax.errorbar(gals_df.logMbh,gals_df.tau_best, yerr=errors,
            #lolims=gals_df.lolims, uplims=gals_df.uplims,
            marker='o', markersize=8,linestyle='',markeredgecolor='Black')
ax.scatter(gals_df.loc[gals_df.lolims,'logMbh'],gals_df.loc[gals_df.lolims,'tau_inf'],marker='^')
ax.scatter(gals_df.loc[gals_df.uplims,'logMbh'],gals_df.loc[gals_df.uplims,'tau_sup'],marker='v')
ax.set_ylim(0.8,15000)
ax.set_xlabel('$\log(M_{BH}) [M_\odot]$')
ax.set_ylabel('$\log(\\tau)$ [days]')
#ax.set_xscale('log')
plt.savefig(path+'tau_mass_corr.pdf', format = 'pdf', bbox_inches = 'tight',transparent=True) ;