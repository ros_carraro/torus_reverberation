#%%
"""
Estimate stars flux from magnitudes in order to interpolate their
Y flux and calibrate the galaxy's LC.
"""
import matplotlib.pyplot as plt
from scipy import interpolate
import pandas as pd
import numpy as np
from astropy.modeling import models, fitting
from astropy import units as u
#%%
# from https://www.gemini.edu/sciops/instruments/midir-resources/imaging-calibrations/fluxmagnitude-conversion
Zero_points={'B':4266.7, 'V':3836.3, 'R':2841, 'I':2247.4,'J':1594,'H':1024,'K':666} # [Jy]
# gal mags - pointless
#bands=['B','V','R','I','J','H','K']
#lambd = [445,551,658,806,1220,1630,2190]
# gal mags - pointless
#mags=[15.25,14.3,14.15,13.88,14.331,13.562,12.462]

#bands=['B','V','R','J','H','K']
#lambd = [445,551,658,1220,1630,2190]
#mags=[15.25,14.3,14.15,14.331,13.562,12.462]

#bands=['B','R','I','J','H','K']
lambd = [445,658,806,1220,1630,2190]

unknown=['Y']
lambda_Y=1020
# %%
# define stars mags
# WISEA J045233.96-295257.9
#star_mags=pd.Series(data=[16.79,16.08,15.13,15.13],index=['B','V','R','I'])
#star_fluxes=pd.Series(data=[Zero_points[ind]*10**(-mag/2.5) for ind,mag in star_mags.iteritems()],index=['B','V','R','I'])
star_mags=pd.Series(data=[16.79,16.08,15.13],index=['B','V','I'])
star_fluxes=pd.Series(data=[Zero_points[ind]*10**(-mag/2.5) for ind,mag in star_mags.iteritems()],index=['B','V','I'])
#star_mags=pd.Series(data=[16.79,16.08,15.13],index=['B','V','R'])
#star_fluxes=pd.Series(data=[Zero_points[ind]*10**(-mag/2.5) for ind,mag in star_mags.iteritems()],index=['B','V','R'])
ir_fluxes=pd.Series(data={'J':2.22e-3,'H':2.11e-3,'K':1.46e-3})
star_fluxes=star_fluxes.append(ir_fluxes)
print(star_fluxes)

# %%
bb_init = models.BlackBody(temperature=5000*u.K,scale=110)
fit_bb = fitting.LevMarLSQFitter()
bb = fit_bb(bb_init, lambd*u.nm, star_fluxes*u.Jy,maxiter=1000)
print(bb.temperature.value)
# %%
x=np.arange(450,2190,3)
f = interpolate.interp1d(lambd, star_fluxes,kind='quadratic')
mag_Y = f(lambda_Y)
print('Spline Y flux=',mag_Y)
print('BB Y flux=',bb(lambda_Y* u.nm))
plt.plot(lambd,star_fluxes)
plt.plot(x,f(x), color='orange')
plt.scatter(lambda_Y,mag_Y, color='orange')
plt.plot(x, bb(x * u.nm), color='green')
plt.scatter(lambda_Y,bb(lambda_Y* u.nm), color='green');

# %%
