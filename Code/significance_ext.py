#%%
"""
Macro che calcola significanza delle correlazioni trovate. 
Simulo curve con caratteristiche come quelle dei dati (B band). Riduco il sampling a quello delle osservazioni. Studio la correlazione tra 
   1 una curva simulata e quella dei dati (I band)
   NO 2 due curve simulate con lo stesso seed ma diversi parametri della power law
al variare del tau, lag.
Le statistiche delle CCF delle curve simulate mi dicono il livello di significanza della CCF tra le curve dei dati (B and I bands)
"""
#!/usr/bin/python
#-*- coding: utf-8 -*-

import numpy as np
from matplotlib import pyplot as plt
import curve_simul as c_sim

import sys
import argparse
import numpy as np
import scipy
import PYCCF as myccf
from scipy import stats 

import pandas as pd

#%%
# Parameters for the simulation of many curves 
n_simul=500
# mi servono 2 seed per ogni curva simulata
seeds=np.arange(n_simul*2).reshape(n_simul,2)

#%%
# Read light curve to find useful parameters for later:
object_name="LB1727"
err=0.05
lc1 = "../Data/Optical/LB1727_B.qdp"
lc2 = "../Data/Optical/LB1727_I.qdp"
mjd1, flux1, err1  =  np.loadtxt(lc1, unpack = True, usecols = [0, 1, 2])
err1=err*flux1
mjd2, flux2, err2  =  np.loadtxt(lc2, unpack = True, usecols = [0, 1, 2])
err2=err*flux2

average=np.mean(flux1)
obs_dur=int(max(mjd1)-min(mjd1))
#%%
#########################################
##Set Interpolation settings, user-specified
#########################################
# I need the cross correlation between the two curves, no MC stuff

lag_range = [-(obs_dur-obs_dur*0.2), obs_dur-obs_dur*0.2]  #Time lag range to consider in the CCF (days). Must be small enough that there is some overlap between light curves at that shift (i.e., if the light curves span 80 days, these values must be less than 80 days)

interp = 2. #Interpolation time step (days). Must be less than the average cadence of the observations, but too small will introduce noise.

#%%
for i in range(n_simul):
   #curva di luce sim i - molto più lunga, 10 Delta T dati.
   times, sim_curve, sim_curve_noise = c_sim.curve_simul(seeds=seeds[i,:],flux_ave=average,flux_err=err)
   N=np.size(times)

   # #%%
   # plt.figure()
   # plt.minorticks_on()
   # plt.plot(times,sim_curve)
   # plt.plot(times,sim_curve_noise)
   # plt.show;

   # #%%
   #sampling - take central part of the sim_curve and sample it
   sim_start=times[N/2]-obs_dur/2
   data_shift=int(mjd1[0])-sim_start
   mjd1_shifted=mjd1.astype(int)-data_shift

   #times_sampled=np.array([times[i] for i in range(len(times)) if times[i] in mjd1_shifted])
   times_sampled = np.sort(np.array(list(set(times).intersection(mjd1_shifted))))
   idx=times_sampled.astype(int)-1
   sim_curve_noise_sampled=sim_curve_noise[idx]

   # #%%
   # # plot curva sampled e data
   # plt.figure()
   # plt.minorticks_on()
   # plt.plot(mjd1,flux1)
   # plt.plot(times_sampled+data_shift,sim_curve_noise_sampled)
   # plt.show;

   # #%%
   # pezzo per tutti i tau possibili immaginabili
   #tau=np.arange(obs_dur-obs_dur*0.2,2)

   # #%%
   ##########################################
   #Calculate lag with python CCF program
   ##########################################
   tlag_peak, status_peak, tlag_centroid, status_centroid, ccf_pack, max_rval, status_rval, pval = myccf.peakcent(times_sampled+data_shift,sim_curve_noise_sampled, mjd2, flux2, lag_range[0], lag_range[1], interp)

   lag = ccf_pack[1]
   r = ccf_pack[0]

   # #%%
   #metto via info correlazione - matrice CCF[i,tau]
   if i == 0:
      sim_CCF=pd.DataFrame(columns=lag)
      sim_CCF.loc[i]=r
   else:
      sim_CCF.loc[i]=r


print sim_CCF.info()
#%%
# Cross correlation between the two observed light curves
tlag_peak, status_peak, tlag_centroid, status_centroid, ccf_pack, max_rval, status_rval, pval = myccf.peakcent(mjd1, flux1, mjd2, flux2, lag_range[0], lag_range[1], interp)

r = ccf_pack[0]

#%%
# Estimate quantiles and add CCF curve to dataframe for plot
quantiles=sim_CCF.quantile([0.01,0.05,0.95,0.99])
quantiles=quantiles.T
quantiles.loc[:,"CCF, B and I"]=pd.Series(r, index=quantiles.index)

#%%
# Plot all curves and save
quantiles.index.name = 'Time lag of I band behind B band [days]'
quantiles.plot(title='CCF as a function of lag between B and I band of {0}'.format(object_name))
plt.ylabel('CCF and significance threshold')
plt.legend(loc='lower right')
plt.savefig('../Optical_CCF/{0}_CCF.pdf'.format(object_name), format = 'pdf', orientation = 'landscape')

