#%%
#!/usr/bin/python
#-*- coding: utf-8 -*-
"""
macro that makes plot like the one in the poster of torus (bottom) 
but just with the line from Koshida+14 paper
"""

import numpy as np
import scipy
from matplotlib import pyplot as plt

#%%
a=-2.052
mag=np.arange(-23,-15)
tau=10**(a-0.2*mag)

with plt.style.context(['seaborn-white','seaborn-bright','seaborn-ticks']):
   plt.yscale('log')
   plt.plot(mag,tau);

#%%


#%%
