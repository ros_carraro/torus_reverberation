#%%
# Change directory to VSCode workspace root so that relative path loads work correctly. Turn this addition off with the DataScience.changeDirOnImportExport setting
import os
try:
	os.chdir(os.path.join(os.getcwd(), '..'))
	print(os.getcwd())
except:
	pass


#%%
"""
script that calls the used to be conv_mag.py but now is a function to extract fluxes from images.
Then makes light curves with them


https://photutils.readthedocs.io/en/stable/psf_matching.html
"""

import glob
from astropy.io import fits
import numpy as np
from astropy.table import Table
import sys
sys.path.append('/Users/Ros/Google_Drive/Valparaiso/Light_curves/SMARTS_pipeline/')
from conv_mag import image_fluxes, convolution, psf_extr, plot_PSF

import matplotlib.pyplot as plt
#imports to study psf
get_ipython().run_line_magic('matplotlib', 'inline')
# %matplotlib notebook
from photutils import source_properties,detect_sources
from photutils import CircularAperture,detect_threshold
from photutils import EllipticalAperture
from astropy.visualization import simple_norm
from astropy.visualization.mpl_normalize import ImageNormalize
from scipy import stats
from astropy import units as u
import pandas as pd


#%%
### select gal from path_name variable below
gal="LB1727"
band="H"
exp_sources=2 # total number of expected sources in the field (galaxy+stars)
stn_threshold=2


#%%
# define general paths and raw fits to read
source_path=gal+'/'+band+'/'

# Build table with info about all galaxies in order to automate
path_name=["IRAS04505","LB1727","TONS180","IRAS16355","ESO354"]
object_name=["IRAS04505-2958","LB1727","TON S 180","IRAS16355-2049","ESO354-G04"]
star_number=[None,0,0,1,1]
# star info
# LB1727
# http://vizier.u-strasbg.fr/viz-bin/VizieR-5?-info=XML&-out.add=.&-source=I/297/out&NOMAD1===0328-0051985&-c=66.4951342-57.1825925&-c.eq=J2000.000&-c.rs=0.004
# TON S 180
# http://vizier.u-strasbg.fr/viz-bin/VizieR-5?-info=XML&-out.add=.&-source=I/297/out&NOMAD1===0676-0015885&-c=14.3178825-22.3918525&-c.eq=J2000.000&-c.rs=0.004
# IRAS16355
# http://vizier.u-strasbg.fr/viz-bin/VizieR-5?-info=XML&-out.add=.&-source=I/297/out&NOMAD1===0690-0372171&-c=249.6328044-20.9183736&-c.eq=J2000.000&-c.rs=0.004
# ESO 354
# http://vizier.u-strasbg.fr/viz-bin/VizieR-5?-info=XML&-out.add=.&-source=I/297/out&NOMAD1===0538-0015419&-c=27.951615-36.1773475&-c.eq=J2000.000&-c.rs=0.004

stars=pd.DataFrame(index=object_name)
stars_mag=pd.DataFrame(index=object_name)

stars.loc[:,"Y"]=np.array([np.nan,np.nan,np.nan,np.nan,np.nan])
stars_mag.loc[:,"J"]=np.array([np.nan,13.442,13.221,12.083,12.514]) # 2MASS"
stars_mag.loc[:,"H"]=np.array([np.nan,13.167,12.570,11.132,12.212])
stars_mag.loc[:,"K"]=np.array([np.nan,13.175,12.444,10.865,12.127])

stars.loc[:,"J"]=np.array([np.nan,6.69,8.20,23.40,15.73]) # mJy
stars.loc[:,"H"]=np.array([np.nan,5.62,9.74,36.61,13.54])
stars.loc[:,"K"]=np.array([np.nan,3.59,7.03,30.11,9.42])

stars=stars.drop(index="IRAS04505-2958")
a=stars.index.str.contains(gal[:3],regex=False)
b=stars.index.str.contains(gal[-3:],regex=False)
idx=np.logical_and(a,b)
star_flux=stars.loc[stars.index[idx].values,band].values

gen_path='/Users/Ros/Google_Drive/Valparaiso/Light_curves/Data/SMARTS_Data/'
path=gen_path+source_path
output_file_folder='/Users/Ros/Google_Drive/Valparaiso/Light_curves/Data/My_light_curves_tables/'+source_path

fits_list=glob.glob(path+'dif*.fits')
fits_list.sort()
fits_list=np.array(fits_list)
#raw_fits=glob.glob(path+'binir120205.0014.fits')
#print(raw_fits[0])
len(fits_list)


#%%
## ### Call function to evaluate PSFs
# !!!!!!
# Call it just once, then the results are saved in the file
psf_extr(fits_list,stn_threshold,output_file_folder,exp_sources=exp_sources)


#%%
# Read PSF info
PSF_raw=pd.read_csv(output_file_folder+'PSF_distrib.txt',sep='\t',header=[0,1],index_col=0)
#PSF_raw=Table.read(output_file_folder+'PSF_distrib.txt',format='ascii')
#npzfile=np.load(output_file_folder+'PSF_distrib.npz')
#npzfile.files #to check file content
# PSF_x=npzfile['arr_0']
# PSF_y=npzfile['arr_1']
# theta=npzfile['arr_2']


#%%
# row lines to be removed manually because of not detecting the right sources:
# lb ... k band
# bad_list=['dif_B_binir110403.0012','dif_B_binir110423.0006','dif_B_binir111227.0089','dif_B_binir120208.0036']
# PSF_raw=PSF_raw.drop(bad_list)


#%%
# optional, if you need to rerun the plot
#perc_vals=[0.50,0.80,0.90,0.95,0.99]
#plot_table=PSF_raw.loc[:,[('star_0', 'dist'), ('psf_0','x'), ('psf_0','y'), ('psf_0','theta')]]
#plot_PSF(plot_table, output_file_folder, perc_vals)


#%%
# Setting things up for detections
# Select reference image for aperture and convolution:
threshold=0.95
bigger_x=PSF_raw.loc[:,('psf_0','x')].quantile(threshold,interpolation='nearest')
bigger_y=PSF_raw.loc[:,('psf_0','y')].quantile(threshold,interpolation='nearest')
#bigger_x=PSF_raw['PSF_x'][PSF_raw['PSF_x'] == np.percentile(PSF_raw['PSF_x'],95,interpolation='nearest')]
#bigger_y=PSF_raw['PSF_y'][PSF_raw['PSF_y'] == np.percentile(PSF_raw['PSF_y'],95,interpolation='nearest')]
      
reference_psf=bigger_x

# Remove bad images from list
low_x=(PSF_raw.loc[:,('psf_0','x')] <= bigger_x)
low_y=(PSF_raw.loc[:,('psf_0','y')] <= bigger_y)
PSF_table=PSF_raw[low_x & low_y]

# Light curves tables initialization
n=2 #n of tables in output from image_fluxes
lc_tables=[None]*n
columns=['JD','gal_sum', 'gal_sum_err', 'star_sum','star_sum_err','ratio','ratio_err']
arr=np.zeros((len(PSF_table),len(columns)))
for i in range(n):
   lc_tables[i]=Table(arr,names=columns, meta={'keywords': {'key1': 'val1'}})


#%%
# Read fits file and detect sources
r = reference_psf*0.5    # approximate isophotal extent
annulus=reference_psf*5
dannulus=4
bad_rows=[]
good_table=0

#fits_image_filename=fits_list[0]
#i=0
for i,fits_image_filename in enumerate(PSF_table.loc[:,('file','path')]):
   with fits.open(fits_image_filename) as hdul:
      filename=fits_image_filename[-27:-5]
      print(fits_image_filename)
      #hdul = fits.open(fits_image_filename)
      #hdul.info()
      data = hdul[0].data.astype(float) 
      hdr=hdul[0].header
      time=hdr['JD']-2450000.5

      # tables, fig=image_fluxes(data,r,annulus,dannulus)

      # # save useful info
      # fig.savefig(output_file_folder+'phot_{0}.pdf'.format(fits_image_filename[-27:-5]), format = 'pdf', bbox_inches = 'tight',transparent=True) 

      # # tell the code which one the star is:
      # # if fits_image_filename == fits_list[0]:
      # #    s=input('Enter star(s) source number(s):')
      # table=tables[good_table]
      # if len(table) > 2:
      #    table.remove_row(1)
      # s=1
      # s=np.array(s)
      # coords = Table(table[s]['xcenter','ycenter'], names=('x','y'))

      # for j,t in enumerate(tables):
      #    #if len(s) > 1:
      #       #prendi le righe delle stelle e somma i valori
      #    #else:
      #    lc_tables[j]['JD'][i] = time  
      #    for obj in t:
      #       if obj['id'] == s:
      #          lc_tables[j]['star_sum'][i] = obj['sum']
      #          lc_tables[j]['star_sum_err'][i] = obj['sum_err']
      #       else:
      #          lc_tables[j]['gal_sum'][i] = obj['sum']
      #          lc_tables[j]['gal_sum_err'][i] = obj['sum_err']
      #    # estimate ratio and error
      #    a=lc_tables[j]['gal_sum'][i]
      #    da=lc_tables[j]['gal_sum_err'][i]
      #    b=lc_tables[j]['star_sum'][i]
      #    db=lc_tables[j]['star_sum_err'][i]
      #    lc_tables[j]['ratio'][i]=a/b
      #    ratio_err=da**2/b**2+(a**2/b**4)*db**2
      #    lc_tables[j]['ratio_err'][i]=ratio_err
      
      data_row=PSF_table.loc[filename]

      conv_data, fig=convolution(data,data_row,bigger_x,bigger_y)
      fig.savefig(output_file_folder+'kernel_{0}.pdf'.format(filename), format = 'pdf', bbox_inches = 'tight',transparent=True) 


      tables,fig=image_fluxes(data,conv_data,data_row,r,annulus,dannulus)

      # save useful info
      fig.savefig(output_file_folder+'phot_conv_{0}.pdf'.format(filename), format = 'pdf', bbox_inches = 'tight',transparent=True) 

      table=tables[good_table]
      
      # if len(table) <= 1:
      #    bad_rows.append(i)
      #    #PSF_table.remove_row(i)
      #    #[t.remove_row(i) for t in lc_tables]
      #    print(F'Only one source in image {fits_image_filename[-27:-5]} - skipping it')
      #    continue
      # elif len(table) > 2:
      #    table.remove_row(1)

      for j,t in enumerate(tables):
         #if size(s) > 1:
            #prendi le linee delle stelle e somma i valori
         #else:
         lc_tables[j]['JD'][i] = time 
         for obj in t:
            if obj['id'].startswith('star'):
               lc_tables[j]['star_sum'][i] = obj['sum']
               lc_tables[j]['star_sum_err'][i] = obj['sum_err']
            else:
               lc_tables[j]['gal_sum'][i] = obj['sum']
               lc_tables[j]['gal_sum_err'][i] = obj['sum_err']
         # estimate ratio and error
         a=lc_tables[j]['gal_sum'][i]
         da=lc_tables[j]['gal_sum_err'][i]
         b=lc_tables[j]['star_sum'][i]
         db=lc_tables[j]['star_sum_err'][i]
         lc_tables[j]['ratio'][i]=a/b
         ratio_err=da**2/b**2+(a**2/b**4)*db**2
         lc_tables[j]['ratio_err'][i]=ratio_err
            
# need to close raw file if NOT using "with fits.open(fits_image_filename) as hdul:"
#hdul.close()


#%%
# grouping of tables and output writing
grouped_LC=[None]*len(lc_tables)
for i,t in enumerate(lc_tables):
   # remove bad images that don't have gal+star 
   #t.remove_rows(np.array(bad_rows,dtype='int'))
   # Group info by day
   t['JD']=np.trunc(t['JD'])
   obs_by_date = t.group_by('JD')
   # obs_by_date=obs_by_date.groups.filter(all_positive)
   obs_mean = obs_by_date['ratio'].groups.aggregate(np.mean) 
   obs_diff = obs_by_date['ratio'].groups.aggregate(np.subtract)
   obs_diff /= np.mean(t['ratio'])

   # Curves calibration
   #obs_mean=obs_mean*star_flux
   #obs_diff=obs_diff*star_flux

   # Join info in another table and save to file
   grouped_LC[i]=Table([obs_by_date.groups.keys['JD'].astype(int),obs_mean,abs(obs_diff)],names=['JD','mean_ratio', 'ratio_err'])
   # save annulus table
   if i == good_table:
      grouped_LC[i].write(output_file_folder+'LC.txt',format='ascii',overwrite=True)


#%%
# Plot all lightcurves and errors
# labels bak 'Segmentation','Segm+back',
labels1=['CircAper+Annulus','CircAper+back']
labels2=[]
with plt.style.context(['seaborn-white','seaborn-bright','seaborn-ticks']):
   fig, (ax1,ax2) = plt.subplots(2, 1, figsize=(10, 10))
   #ax1.set_ylim([0.3,1.3])
   #x2.set_ylim([-1.,1.])
   for t in grouped_LC:
      t.info
      ax1.errorbar(t['JD'][t['ratio_err']<10],t['mean_ratio'][t['ratio_err']<10], fmt='o', ms=3)
      ax2.errorbar(t['JD'][t['ratio_err']<10],np.log10(t['ratio_err'][t['ratio_err']<10]), fmt='o', ms=3)
      a=t['ratio_err']
      labels2.append(F'stddev: {np.std(a)}, median: {np.median(a):.3f}')
      ##[t['ratio_err']:.2f<10]
   ax1.minorticks_on()
   ax2.minorticks_on()
   ax1.legend(labels1)
   ax2.legend(labels2)
   plt.savefig(output_file_folder+'LC.pdf', format = 'pdf', bbox_inches = 'tight',transparent=True) 


#%%
t=grouped_LC[good_table]
labels1=['Segmentation','Segm+back','CircAper+Annulus','CircAper+back']
labels2=[]

with plt.style.context(['seaborn-white','seaborn-bright','seaborn-ticks']):
   fig, (ax1,ax2) = plt.subplots(2, 1, figsize=(10, 10))
   #ax1.set_ylim([0.48,0.7])
   ax1.errorbar(t['JD'][t['ratio_err']<10],t['mean_ratio'][t['ratio_err']<10], fmt='o', ms=3)
   ax2.errorbar(t['JD'][t['ratio_err']<10],np.log10(t['ratio_err'][t['ratio_err']<10]), fmt='o', ms=3)
   a=t['ratio_err']
   labels2.append(F'stddev: {np.std(a):.2f}, median: {np.median(a):.3f}')
   ax1.minorticks_on()
   ax2.minorticks_on()
   ax1.legend(labels1[good_table])
   ax2.legend(labels2)
   plt.savefig(output_file_folder+'LC_annulus_toph.pdf', format = 'pdf', bbox_inches = 'tight',transparent=True)


#%%
nt=1
t=grouped_LC[nt]
labels1=['Segmentation','Segm+back','CircAper+Annulus','CircAper+back']
labels2=[]

with plt.style.context(['seaborn-white','seaborn-bright','seaborn-ticks']):
   fig, (ax1,ax2) = plt.subplots(2, 1, figsize=(10, 10))
   #ax1.set_ylim([0.45,0.8])
   #ax2.set_ylim([-0.075,0.075])
   ax1.errorbar(t['JD'][t['ratio_err']<10],t['mean_ratio'][t['ratio_err']<10], fmt='o', ms=3)
   ax2.errorbar(t['JD'][t['ratio_err']<10],np.log10(t['ratio_err'][t['ratio_err']<10]), fmt='o', ms=3)
   a=t['ratio_err']
   labels2.append(F'stddev: {np.std(a):.2f}, median: {np.median(a):.3f}')
   ax1.minorticks_on()
   ax2.minorticks_on()
   ax1.legend(labels1[nt])
   ax2.legend(labels2)
   plt.savefig(output_file_folder+'LC_back_toph.pdf', format = 'pdf', bbox_inches = 'tight',transparent=True)

# plot curva - no day mean
# with plt.style.context(['seaborn-white','seaborn-bright']):
#    fig, (ax1,ax2) = plt.subplots(2, 1, figsize=(10, 10))
#    ax1.set_xlim([5700,6220])
#    ax1.set_ylim([0,2])
#    for t in lc_tables:
#       #ax1.errorbar(obs_by_date.groups.keys,obs_mean, fmt='o', ms=3)
#       ax1.errorbar(t['JD'],t['ratio'],yerr=t['ratio_err'], fmt='o', ms=3)
#    ax1.legend(labels)
#    plt.savefig(output_file_folder+'LC_{0}.pdf'.format(fits_image_filename[-27:-5]), format = 'pdf', bbox_inches = 'tight',transparent=True) 
# fare figura di delta Flux in f di t per ciascun metodo -> conserva tutti i delta


#%%
def all_positive(table,key_colnames):
   for colname in key_colnames:
      if np.any(table[colname] <= 0):
         return False
   return True


#%%
np.std(t['ratio_err'])


#%%
lc_tables[0].columns


#%%
from jupyter_core.paths import jupyter_config_dir
jupyter_dir = jupyter_config_dir()
print(jupyter_dir)


