#%%
"""
Plot of disk and torus black bodies spectra
"""
import matplotlib.pyplot as plt
import numpy as np
#%%
path='/Users/Ros/Google_Drive/Valparaiso/Light_curves/Code/BB_plot/'
file=path+"SED_poster_Rosamaria.qdp"

x, bb1, bb2 =  np.loadtxt(file, unpack = True, usecols = [0, 1, 2])

bb1=bb1/x*10
bb2=bb2/x*10
bb_tot=bb1+bb2

with plt.style.context(['seaborn-white','seaborn-bright','seaborn-ticks']):
   fig = plt.figure()
   ax = fig.add_subplot(111)
   ax.set_xscale("log", nonposx='clip')
   ax.set_yscale("log", nonposy='clip')
   plt.ylim(1.1e-17,6e-11)
   plt.xlim(1.5e2,2e5)
   ax.minorticks_on()
   ax.set_yticklabels([]) # turn off y axis numbers

   plt.plot(x,bb1,c='r')
   plt.plot(x,bb2)
   plt.plot(x,bb_tot,linewidth=4.0,c='black')

   plt.savefig(path+'BB_8.pdf', format = 'pdf', orientation = 'landscape', bbox_inches = 'tight',transparent=True) 



#%%
