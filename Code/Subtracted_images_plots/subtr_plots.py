#%%
#!/usr/bin/python
#-*- coding: utf-8 -*-
"""
Macro that I don't remember writing. 
It does the difference between some given two galaxy images. 
It was originally made to understand why some specific images made the code (Light_curves and conv_mag) crash.
From the outputs of this code I figured out they had negatives counts all over the image, even in the source position.
I solved by background subtracting the images.
"""
import matplotlib.pyplot as plt 
from astropy.io import fits
from astropy.visualization import simple_norm
#%%
path='/Users/Ros/Google_Drive/Valparaiso/Light_curves/Data/SMARTS_Data/'
output='/Users/Ros/Google_Drive/Valparaiso/Light_curves/Code/Subtracted_images_plots/'
path_loc=path+'ESO244/H/'
file1=path_loc+'binir120116.0016.fits'
file2=path_loc+'binir120116.0017.fits'
gal='ESO244-G17'
#%%
with fits.open(file1) as hdu1, fits.open(file2) as hdu2:
   hdu1[0].data -= hdu2[0].data

   with plt.style.context(['seaborn-white','seaborn-bright']):
      norm = simple_norm(hdu1[0].data, 'sqrt', percent=99.9)
      plt.imshow(hdu1[0].data, cmap='Greys_r', origin='lower', norm=norm)
      plt.xlim(0, hdu1[0].data.shape[1]-1)
      plt.ylim(0, hdu1[0].data.shape[0]-1)
      plt.title(gal)
      plt.savefig(output+'dif_'+file1[-21:-10]+'.pdf', format = 'pdf', bbox_inches = 'tight',transparent=True)

   hdu1.writeto(output+'dif_'+file1[-21:-10]+'.fits')

#%%
path_loc=path+'ESO434/H/'
file1=path_loc+'binir120120.0185.fits'
file2=path_loc+'binir120120.0186.fits'
gal='ESO434-G40'
#%%
with fits.open(file1) as hdu1, fits.open(file2) as hdu2:
   hdu1[0].data -= hdu2[0].data

   with plt.style.context(['seaborn-white','seaborn-bright']):
      norm = simple_norm(hdu1[0].data, 'sqrt', percent=99.9)
      plt.imshow(hdu1[0].data, cmap='Greys_r', origin='lower', norm=norm)
      plt.xlim(0, hdu1[0].data.shape[1]-1)
      plt.ylim(0, hdu1[0].data.shape[0]-1)
      plt.title(gal)
      plt.savefig(output+'dif_'+file1[-21:-10]+'.pdf', format = 'pdf', bbox_inches = 'tight',transparent=True)

   hdu1.writeto(output+'dif_'+file1[-21:-10]+'.fits')

#%%
path_loc=path+'HE0502/J/'
#file1=path_loc+'binir120123.0107.fits'
#file2=path_loc+'binir120123.0108.fits'
#file1=path_loc+'binir120818.0336.fits'
#file2=path_loc+'binir120818.0337.fits'
file1=path_loc+'binir111106.0290.fits'
file2=path_loc+'binir111106.0291.fits'
gal='HE0502-2948'
#%%
with fits.open(file1) as hdu1, fits.open(file2) as hdu2:
   hdu1[0].data -= hdu2[0].data

   with plt.style.context(['seaborn-white','seaborn-bright']):
      norm = simple_norm(hdu1[0].data, 'sqrt', percent=99.9)
      plt.imshow(hdu1[0].data, cmap='Greys_r', origin='lower', norm=norm)
      plt.xlim(0, hdu1[0].data.shape[1]-1)
      plt.ylim(0, hdu1[0].data.shape[0]-1)
      plt.title(gal)
      plt.savefig(output+'dif_'+file1[-21:-10]+'.pdf', format = 'pdf', bbox_inches = 'tight',transparent=True)

   hdu1.writeto(output+'dif_'+file1[-21:-10]+'.fits',overwrite=True)

#%%
path_loc=path+'IRAS01089/H/'
file1=path_loc+'binir120131.0011.fits'
file2=path_loc+'binir120131.0012.fits'
gal='IRAS01089-4743'
#%%
with fits.open(file1) as hdu1, fits.open(file2) as hdu2:
   hdu1[0].data -= hdu2[0].data

   with plt.style.context(['seaborn-white','seaborn-bright']):
      norm = simple_norm(hdu1[0].data, 'sqrt', percent=99.9)
      plt.imshow(hdu1[0].data, cmap='Greys_r', origin='lower', norm=norm)
      plt.xlim(0, hdu1[0].data.shape[1]-1)
      plt.ylim(0, hdu1[0].data.shape[0]-1)
      plt.title(gal)
      plt.savefig(output+'dif_'+file1[-21:-10]+'.pdf', format = 'pdf', bbox_inches = 'tight',transparent=True)

   hdu1.writeto(output+'dif_'+file1[-21:-10]+'.fits')

#%%
path_loc=path+'IRAS01267/H/'
file1=path_loc+'binir120214.0019.fits'
file2=path_loc+'binir120214.0020.fits'
gal='IRAS01267-2157'
#%%
with fits.open(file1) as hdu1, fits.open(file2) as hdu2:
   hdu1[0].data -= hdu2[0].data

   with plt.style.context(['seaborn-white','seaborn-bright']):
      norm = simple_norm(hdu1[0].data, 'sqrt', percent=99.9)
      plt.imshow(hdu1[0].data, cmap='Greys_r', origin='lower', norm=norm)
      plt.xlim(0, hdu1[0].data.shape[1]-1)
      plt.ylim(0, hdu1[0].data.shape[0]-1)
      plt.title(gal)
      plt.savefig(output+'dif_'+file1[-21:-10]+'.pdf', format = 'pdf', bbox_inches = 'tight',transparent=True)

   hdu1.writeto(output+'dif_'+file1[-21:-10]+'.fits')


#%%
path_loc=path+'IRASL06229/H/'
file1=path_loc+'binir120213.0248.fits'
file2=path_loc+'binir120213.0249.fits'
gal='IRASL06229-6434'
#%%
with fits.open(file1) as hdu1, fits.open(file2) as hdu2:
   hdu1[0].data -= hdu2[0].data

   with plt.style.context(['seaborn-white','seaborn-bright']):
      norm = simple_norm(hdu1[0].data, 'sqrt', percent=99.9)
      plt.imshow(hdu1[0].data, cmap='Greys_r', origin='lower', norm=norm)
      plt.xlim(0, hdu1[0].data.shape[1]-1)
      plt.ylim(0, hdu1[0].data.shape[0]-1)
      plt.title(gal)
      plt.savefig(output+'dif_'+file1[-21:-10]+'.pdf', format = 'pdf', bbox_inches = 'tight',transparent=True)

   hdu1.writeto(output+'dif_'+file1[-21:-10]+'.fits')


#%%
path_loc=path+'PKS0521/H/'
file1=path_loc+'binir120111.0087.fits'
file2=path_loc+'binir120111.0088.fits'
gal='PKS0521-36'
#%%
with fits.open(file1) as hdu1, fits.open(file2) as hdu2:
   hdu1[0].data -= hdu2[0].data

   with plt.style.context(['seaborn-white','seaborn-bright']):
      norm = simple_norm(hdu1[0].data, 'sqrt', percent=99.9)
      plt.imshow(hdu1[0].data, cmap='Greys_r', origin='lower', norm=norm)
      plt.xlim(0, hdu1[0].data.shape[1]-1)
      plt.ylim(0, hdu1[0].data.shape[0]-1)
      plt.title(gal)
      plt.savefig(output+'dif_'+file1[-21:-10]+'.pdf', format = 'pdf', bbox_inches = 'tight',transparent=True)

   hdu1.writeto(output+'dif_'+file1[-21:-10]+'.fits')


#%%
