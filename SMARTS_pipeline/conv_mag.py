#!/usr/bin/python
#-*- coding: utf-8 -*-

"""
Useful functions are: create_difference_images, find_target, source_find, psf_extr, conv_flux (makes sense only to call them in this order)
The rest are "auxiliary" functions for psf_extr.

create_difference_images - groups the observation files in couples from the same day and creates the two subracted image files

image_fluxes - extracts fluxes from sources in the convolved data row in the area defined by radius, annulus and dannulus (for background)

find_target - in case of more than two expected sources () looks for a reference (target) image to use to compare to. Uses as auxiliary: yes_or_no

source_find - Scans and inds all sources positions and returns a table with all the information. Returns also a list of images with no galaxy detection or no convergence of astroalign. Uses as auxiliary: check_plot, yes_or_no

psf_extr - Scans and studies the PSF distribution. Also finds all sources positions and returns a table with all the information. Uses as auxiliary: plot_PSF, check_plot, measure_PSF

check_plot - Plots the data and the (optional) positions of the sources in the field.

plot_PSF - Plots the PSF distributions extracted from psf_extr.

measure_PSF - Measures the PSF on the reference star of the image.

convolution - Convolves data to a given width (default x,y=4,6).

convol_bak - A backup of the first method I tried to use to convolve the image. Convolves to a reference source PSF.

yes_or_no - Function that asks a yes/no question. Takes a string as input. Returns a boolean value as the answer.
"""
import itertools as it
# imports for find_sources 
from IPython.display import Markdown
from skimage import transform

# imports for image_fluxes
from photutils import detect_threshold, detect_sources
from photutils import Background2D, ModeEstimatorBackground
from photutils import source_properties
from photutils.utils import calc_total_error
from photutils import aperture_photometry
from photutils import CircularAnnulus,CircularAperture
from photutils import EllipticalAnnulus,EllipticalAperture
from astropy.table import Table, vstack, hstack, join
import numpy as np
import matplotlib.pyplot as plt
from astropy.visualization import PowerStretch,MinMaxInterval
from math import pi

#imports to study psf
from os import path
from astropy.io import fits
from astropy.visualization import simple_norm
from astropy.visualization.mpl_normalize import ImageNormalize
from scipy import stats
from astropy import units as u
from astropy.table import Column, QTable
import pandas as pd
import astroalign as aa
aa.MIN_MATCHES_FRACTION = 0.01

# imports for convolve
#from photutils import Background2D, ModeEstimatorBackground
from astropy.modeling.models import Gaussian2D
from astropy.nddata import NDData
from photutils.psf import extract_stars
#import matplotlib.pyplot as plt
from photutils import EPSFBuilder
from astropy.modeling import models, fitting
#from photutils import IntegratedGaussianPRF
from astropy.stats import gaussian_sigma_to_fwhm
from astropy.convolution import Gaussian2DKernel, convolve
from photutils import create_matching_kernel, TopHatWindow

# secondo convolve
#from photutils import create_matching_kernel

def create_difference_images_bak(fits_list,out_path):
   """
   Input:
      fits_list is a list of paths to the raw observation images, containing the date in the name
      out_path is the path to save the output files, but has to coincide with the path of the input files for the function to find the dates in the file names.
   Output:
      'dif_A_binirDATE.fits','dif_B_binirDATE.fits' fits files that are the difference of the two images of each day of observations
   """
   fits_list.sort()

   # extract the date from the names to use as index and create a series with the files list
   dates_list=np.array([file[len(out_path)+5:len(out_path)+11] for file in fits_list],dtype='int')
   #dates_index=pd.to_datetime(dates_list,yearfirst=True,format='%y%m%d')
   fits_df=pd.Series(fits_list,index=dates_list) #index=dates_list if datetime index

   # look for duplicates in the index - every day is supposed to be there twice
   new_fits_df=pd.DataFrame(columns=['first','second'])
   def check_groups(fits_el):
      #assert fits_el.shape[0] == 2,'{0} has {1} elements'.format(fits_el.index[0].strftime('%y%m%d'),fits_el.shape[0])
      assert fits_el.shape[0] == 2,'{0} has {1} elements'.format(fits_el.index[0],fits_el.shape[0])
      index_name=fits_el.index[0]
      new_fits_el=fits_el.reset_index(drop=True)#.rename(index_name)
      return new_fits_el.to_list(),index_name
   
   rows_list,index_list= map(list,zip(*[check_groups(fits_df.groupby(fits_df.index).get_group(idx)) for idx in fits_df.index.unique()]))
   new_fits_df=pd.DataFrame(rows_list,index=index_list,columns=['first','second'])
   
   # second way to do it - using .index.duplicated() - but worse check on number of obs per day
   #new_fits_df.append(rows_list,ignore_index=True)
   #new_fits_df=pd.concat([pd.DataFrame(i,columns=['first','second']) for i in rows_list])
   #duplicates=fits_df.index.duplicated()
   #assert duplicates.sum() == np.logical_not(duplicates).sum(),'Not all days have 2 observations.'
   #new_fits_df=pd.concat([fits_df.loc[np.logical_not(duplicates)].rename('first'), fits_df.loc[duplicates].rename('second')],axis=1)

   # for each day read the files, make the differences, and save to a new fits
   for _,row in new_fits_df.iterrows():
      with fits.open(row['first']) as hdu0, fits.open(row['second']) as hdu1:
         data0 = hdu0[0].data.astype(float)
         header0 = hdu0[0].header
         data1 = hdu1[0].data.astype(float)

         dif_A=data0-data1
         dif_B=data1-data0

         fits.PrimaryHDU(dif_A,header=header0).writeto(out_path+'dif_A_'+row['first'][len(out_path):-10]+'.fits')
         fits.PrimaryHDU(dif_B,header=header0).writeto(out_path+'dif_B_'+row['second'][len(out_path):-10]+'.fits')

def create_difference_images(fits_list,out_path):
   """
   Input:
      fits_list is a list of paths to the raw observation images, containing the date in the name
      out_path is the path to save the output files, but has to coincide with the path of the input files for the function to find the dates in the file names.
   Output:
      'dif_X_binirDATE.fits' fits files that are the difference of two images of each day of observations. X goes in alphabetic order
   """
   # version with support to many images per dayfits_list.sort()
   fits_list.sort()
   # extract the date from the names to use as index and create a series with the files list
   dates_list=np.array([file[len(out_path)+5:len(out_path)+11] for file in fits_list],dtype='int')
   fits_df=pd.Series(fits_list,index=dates_list) #index=dates_list if datetime index

   def pairs(group):
      pair=list(it.combinations(range(len(group)), 2))
      letters=[chr(x) for x in range(65, 65+2*len(pair))]
      for i,p in enumerate(pair):
         p0, p1=p[0], p[1]
         with fits.open(group.iloc[p0]) as hdu0, fits.open(group.iloc[p1]) as hdu1:
            data0 = hdu0[0].data.astype(float)
            header0 = hdu0[0].header
            data1 = hdu1[0].data.astype(float)

            dif_A=data0-data1
            dif_B=data1-data0

            fits.PrimaryHDU(dif_A,header=header0).writeto(out_path+'dif_'+letters[2*i]+'_binir'+str(group.index[0])+'.fits')
            fits.PrimaryHDU(dif_B,header=header0).writeto(out_path+'dif_'+letters[2*i+1]+'_binir'+str(group.index[0])+'.fits')
        
   # extract files of each date by grouping in order to do all the subractions
   grouped_list=fits_df.groupby(fits_df.index)
   [pairs(grouped_list.get_group(idx)) for idx in fits_df.index.unique()]

def stars_flux_df(gal):
   ##################################
   # dictionary with stars info
   # stars fluxes from 2MASS data on NED, 4" aperture
   # extended sources info from TESS input catalog v8.0 (Stassun+19)
   
   all_stars={}

   eso244={'J':[1.5e-3,5.13e-3,4.0e-3,2.6e-3],'J_err':[9.42e-5,9.93e-5,1.33e-4,8.61e-5],
         'H':[1.62e-3,5.53e-3,4.61e-3,2.89e-3],'H_err':[1.7e-4,1.89e-4,2.21e-4,2.1e-4],
         'K':[1.43e-3,4.39e-3,4.17e-3,2.23e-3],'K_err':[1.54e-4,2.19e-4,2.31e-4,1.34e-4],
         'extended':[False,False,True,False]}
   #ned_names=['WISEA J012009.82-440642.8','2MASS J01201000-4407571','LCRS B011759.9-442340','WISEA J012012.18-440839.2']
   all_stars['ESO244']=pd.DataFrame(data=eso244,index=np.arange(1,len(eso244['J'])+1))

   ESO354={'J':[1.57e-2],'J_err':[1.73e-4], 'H':[1.32e-2],'H_err':[5.22e-4], 'K':[9.1e-3],'K_err':[3.44e-4], 'extended':[False]}
   #ned_names=['WISEA J015148.39-361038.4']
   all_stars['ESO354']=pd.DataFrame(data=ESO354,index=[1])

   ESO434={'J':[2.15e-3,1.83e-2,6.05e-3],'J_err':[7.12e-5,2.52e-4,2.62e-4],
         'H':[1.97e-3,1.63e-2,5.1e-3],'H_err':[2.04e-4,6.3e-4,3.06e-4],
         'K':[1.51e-3,1.20e-2,3.5e-3],'K_err':[1.98e-4,2.1e-4,1.84e-4],
         'extended':[False,False,False]}
   #ned_names=['WISEA J094737.16-305558.7','WISEA J094738.36-305545.6','2MASS J09473861-3057050']
   all_stars['ESO434']=pd.DataFrame(data=ESO434,index=np.arange(1,len(ESO434['J'])+1))

   #for k band
   #HE0502={'J':[1.580e-2],'J_err':[2.77e-4], 'H':[1.41e-2],'H_err':[4.55e-4],
   #        'K':[9.6e-3],'K_err':[2.39e-4], 'extended':[False]}
   #ned_names=['WISEA J050417.13-294532.8']
   #all_stars['HE0502']=pd.DataFrame(data=HE0502,index=[1])
   HE0502={'J':[7.94e-4,1.580e-2,6.72e-4],'J_err':[9.07e-5,2.77e-4,1.31e-4], 
         'H':[7.18e-4,1.41e-2,8.19e-4],'H_err':[2.05e-4,4.55e-4,2.11e-5],
         'K':[7.91e-4,9.6e-3,7.56e-4],'K_err':[1.19e-4,2.39e-4,1.99e-4], 'extended':[False,False,False]}
   #ned_names=['WISEA J050414.43-294436.6','WISEA J050417.13-294532.8','WISEA J050420.61-294529.7']
   all_stars['HE0502']=pd.DataFrame(data=HE0502,index=np.arange(1,len(HE0502['J'])+1))

   IRAS01089={'J':[2.52e-3,3.96e-3],'J_err':[1.05e-4,1.24e-4],
               'H':[2.73e-3,4.52e-3],'H_err':[1.03e-4,3.16e-4],
               'K':[2.0e-3,3.76e-3],'K_err':[6.99e-5,1.25e-4],
               'extended':[False,False]}
   #ned_names=['WISEA J011112.64-472650.7','WISEA J011113.82-472809.1']
   all_stars['IRAS01089']=pd.DataFrame(data=IRAS01089,index=np.arange(1,len(IRAS01089['J'])+1))

   IRAS01267={'J':[3.34e-3,2.67e-3,5.26e-3],'J_err':[8.3e-5,6.4e-5,8.73e-5], #,1.82e-3 ,1.29e-4 #,5.54e-3,1.43e-4,
            'H':[4.47e-3,2.66e-3,5.96e-3],'H_err':[1.4e-4,1.96e-4,2.52e-4], #,1.63e-3 ,6.75e-5 #8.17e-3,1.66e-4
            'K':[4.76e-3,2.16e-3,4.88e-3],'K_err':[2.68e-4,2.15e-4,1.44e-4], #,1.16e-3 ,1.72e-4 #1.33e-2,1.47e-4
            'extended':[True,False,False]} #,False
   #ned_names=[' WISEA J012910.41-214157.0','WISEA J012913.24-214150.0','WISEA J012915.11-214111.3',]#'WISEA J012915.96-214052.3'#'WISEA J012910.95-214156.9'
   all_stars['IRAS01267']=pd.DataFrame(data=IRAS01267,index=np.arange(len(IRAS01267['J']))+1)

   y_flux=3631*10**(-15.5814/2.5) # 3631 is zero-point for AB mags. Ymag=15.5814 from DES(Abbott+2018)
   IRAS04505={'J':[2.22e-3],'J_err':[9.43e-5], 'H':[2.11e-3],'H_err':[1.96e-4], 
            'Y':[y_flux],'Y_err':[9.9999999e-9], 'extended':[False]} # Y data not available in NED, VistaHemisphereSurvey, eso archive, UKIDSS, DarkEnergySurvey
   #ned_names=['WISEA J045233.96-295257.9'] more possible stars in the field: ['2MASS 04523376-2952269','2MASS 04523381-2953202','2MASS 04522670-2952419','2MASS 04522673-2953170']
   all_stars['IRAS04505']=pd.DataFrame(data=IRAS04505,index=[1])

   IRAS16355={'J':[5.84e-3,2.74e-2,2.32e-2,1.6e-3,2.39e-3,6.81e-3],'J_err':[1.13e-4,3.29e-4,4.06e-4,1.16e-4,6.16e-5,1.32e-4],
               'H':[6.69e-3,3.44e-2,3.62e-2,2.05e-3,2.18e-3,8.35e-3],'H_err':[2.9e-4,0.00165,0.00123,8.33e-5,1.47e-4,2.39e-4],
               'K':[4.72e-3,2.68e-2,2.92e-2,1.4e-3,1.64e-3,6.33e-3],'K_err':[3.18e-4,6.18e-4,5.93e-4,1.43e-4,2.7e-4,2.91e-4],
               'extended':[False,False,False,False,False,False]}
   #ned_names=['WISEA J163826.32-205619.6','WISEA J163830.48-205353.8','WISEA J163831.87-205506.0','WISEA J163834.96-205411.5','WISEA J163835.07-205437.7','WISEA J163835.63-205617.1']
   all_stars['IRAS16355']=pd.DataFrame(data=IRAS16355,index=np.arange(1,len(IRAS16355['J'])+1))

   IRASL06229={'J':[2.21e-2,1.06e-3,2.61e-3,1.24e-3,7.99e-3],'J_err':[2.04e-4,1.67e-4,1.15e-4,1.2e-4,1.18e-4],
               'H':[1.8e-2,9.29e-4,2.83e-3,1.45e-3,8.78e-3],'H_err':[4.64e-4,2.46e-4,1.77e-4,1.92e-4,3.48e-4],
               'K':[1.23e-2,5.7e-4,1.21e-3,9.8e-4,7.3e-3],'K_err':[1.25e-4,7.98e-5,1.26e-4,3.7e-5,1.88e-4],
               'extended':[False,False,False,False,False]}
   #ned_names=['WISEA J062258.29-643532.0','WISEA J062304.81-643638.5','WISEA J062304.96-643547.7','WISEA J062309.17-643600.0','WISEA J062319.69-643641.8']
   all_stars['IRASL06229']=pd.DataFrame(data=IRASL06229,index=np.arange(len(IRASL06229['J']))+1)

   LB1727={'J':[6.65e-3],'J_err':[2.14e-4], 'H':[5.48e-3],'H_err':[2.58e-4], 'K':[3.59e-3],'K_err':[3.05e-4], 'extended':[False]}
   #ned_names=['WISEA J042558.83-571057.2']
   all_stars['LB1727']=pd.DataFrame(data=LB1727,index=[1])

   MRK509={'J':[1.61e-3,7.19e-3],'J_err':[9.82e-5,1.19e-4], 
         'H':[1.89e-3,5.94e-3],'H_err':[1.74e-4,1.09e-4],
         'K':[1.4e-3,4.13e-3],'K_err':[2.39e-4,1.71e-4], 'extended':[False,False]}
   #ned_names=['WISEA J204412.78-104315.3','WISEA J204415.06-104441.8']
   all_stars['MRK509']=pd.DataFrame(data=MRK509,index=np.arange(len(MRK509['J']))+1)

   PKS0521={'J':[2.56e-3,2.55e-2,9.94e-3],'J_err':[1.11e-4,4.47e-4,1.1e-4],
         'H':[3.02e-3,2.52e-2,8.5e-3],'H_err':[1.95e-4,6.74e-4,1.33e-4],
         'K':[2.26e-3,1.84e-2,6.22e-3],'K_err':[1.94e-4,4.24e-4,2.29e-4],
         'extended':[False,False,False]}
   #ned_names=['WISEA J052250.30-362605.1','WISEA J052252.84-362704.7','WISEA J052256.03-362618.9']
   all_stars['PKS0521']=pd.DataFrame(data=PKS0521,index=np.arange(len(PKS0521['J']))+1)

   TONS180={'J':[8.07e-3],'J_err':[1.19e-4], 'H':[9.47e-3],'H_err':[1.48e-4], 'K':[6.94e-3],'K_err':[1.98e-4], 'extended':[False]}
   #ned_names=['WISEA J005716.29-222330.6']
   all_stars['TONS180']=pd.DataFrame(data=TONS180,index=[1])

   #ESO354={'J':[],'J_err':[],
   #        'H':[],'H_err':[],
   #        'K':[],'K_err':[],
   #        'extended':[]}
   #ned_names=['']
   #all_stars['']=pd.DataFrame(data=,index=)

   stars = all_stars[gal]

   return stars

def image_fluxes(data,conv_data,data_row,r=10,annulus=18,dannulus=4):
   """
   Input:
      data is fits.data as np.array - not really necessary anymore
      conv_data is the convolved data obtained with convolve
      data_row is a Pandas series with the sources information
      r radius of aperture for flux extraction
      annulus is the inner radius for background subtraction
      dannulus is the width of the annulus for background subtraction
   Output:
      phot_table1 table with fluxes from aperture and annulus background subtraction (method #3)
      phot_table2 table with fluxes from fixed elliptical aperture subtracting background (method #4)
      fig - figure of convolved data and background from convolved data.

   This function originally included 4 methods to estimate fluxes, but #3 is the one that showed better results. #4 works almost as well but gives bigger errors, #1,2 give different light curves.
   """
   rin=annulus
   rout=annulus+dannulus

   # Background - cooler way than just a S/N threshold
   #threshold = detect_threshold(data, snr=3.)
   # bkg_estimator = ModeEstimatorBackground() 
   # bkg = Background2D(data, (32, 32), filter_size=(3, 3),
   #       bkg_estimator=bkg_estimator) #box size bigger than source size, but entire number should cover the field, so 32 o 64
   # threshold = bkg.background + (5. * bkg.background_rms)

   #segm = detect_sources(data, threshold, npixels=50)

   # Background - for flux extraction purposes
   bkg_estimator = ModeEstimatorBackground() 
   bkg = Background2D(conv_data, (16, 16), filter_size=(5, 5),
         bkg_estimator=bkg_estimator) #box size bigger than source size, but entire number should cover the field, so 32 o 64
   #threshold = bkg.background + (5. * bkg.background_rms)


   # # 1 - fluxes from segmentation image
   # cat = source_properties(conv_data, segm)
   # tbl1 = cat.to_table()
   # tbl1['xcentroid'].info.format = '.2f'  # optional format
   # tbl1['ycentroid'].info.format = '.2f'
   # tbl1['cxx'].info.format = '.2f'
   # tbl1['cxy'].info.format = '.2f'
   # tbl1['cyy'].info.format = '.2f'
   # tbl1['id']=tbl1['id']-1 # I want IDs that start from 0
   # #print(tbl1['id','source_sum','source_sum_err'])
   # #print(cat.id)
   # tbl1.rename_column('source_sum', 'sum')
   # tbl1.rename_column('source_sum_err','sum_err')

   # # 2 - fluxes from segmentation image considering background
   effective_gain=2.3 #epadu in TON_H.py 
   error=calc_total_error(conv_data, bkg.background_rms, effective_gain)
   # data_nobak=data - bkg.background
   # cat = source_properties(data_nobak, segm, error=error,background=bkg.background)
   # tbl2 = cat.to_table()
   # tbl2['xcentroid'].info.format = '.2f'  # optional format
   # tbl2['ycentroid'].info.format = '.2f'
   # tbl2['cxx'].info.format = '.2f'
   # tbl2['cxy'].info.format = '.2f'
   # tbl2['cyy'].info.format = '.2f'
   # tbl2['id']=tbl2['id']-1 # I want IDs that start from 0
   # #print(tbl2['id','source_sum','source_sum_err'])
   # tbl2.rename_column('source_sum', 'sum')
   # tbl2.rename_column('source_sum_err','sum_err')

   # retrieve all 'star_N' whose 'x' is not NaN.
   star_xcols = [(idx[0].startswith('star_') or idx[0].startswith('gal')) and idx[1].startswith('x') for idx in data_row.index]
   defined_el = data_row.notnull()
   filter = np.logical_and(star_xcols,defined_el)

   source_list=[idx[0] for idx in data_row[filter].index]

   # 3 - fluxes from aperture and annulus background subtraction
   apertures = []
   annuli_areas=[None]*len(source_list)
   apertures_areas=[None]*len(source_list)
   positions=[None]*len(source_list)
   for i,obj in enumerate(source_list):
      # for each source define aperture and annulus where to extract photometry
      position = [data_row[obj,'x'], data_row[obj,'y']]
      #apertures needed here
      aperture=CircularAperture(position, r=r)
      annulus_apertures = CircularAnnulus(position, r_in=rin, r_out=rout)
      apers = [aperture, annulus_apertures]
      # apertures needed for plotting 
      # apertures[2*i]=aperture
      # apertures[2*i+1]=annulus_apertures
      apertures.append(aperture)
      apertures.append(annulus_apertures)
      # create table with all fluxes
      if obj == 'gal':
         phot_table1=aperture_photometry(conv_data,apers,error=error)
         phot_table1.rename_column('id', 'id1')
         phot_table1['id1'][0] = i 
         id = Column([obj], name='id')
         phot_table1.add_column(id, index=0)
      else:
         cat2=aperture_photometry(conv_data,apers,error=error)
         cat2.rename_column('id', 'id1')
         cat2['id1'][0] = i 
         id = Column([obj], name='id')
         cat2.add_column(id, index=0)
         # adds row to fluxes table
         phot_table1=vstack([phot_table1,cat2])
      annuli_areas[i]=annulus_apertures.area
      apertures_areas[i]=aperture.area
      positions[i]=position
   # estimate bkg counts per unit area from annulus
   bkg_mean = phot_table1['aperture_sum_1'] / annuli_areas
   # bkg counts in source area
   bkg_sum = bkg_mean * apertures_areas
   # subtract background from source
   final_sum = phot_table1['aperture_sum_0'] - bkg_sum
   # write value in table
   phot_table1['residual_aperture_sum'] = final_sum
   phot_table1['residual_aperture_sum'].info.format = '%.8g'  # for consistent table output
   # estimate error on background subtracted flux
   bkg_sum_err=phot_table1['aperture_sum_err_1']/ annuli_areas*apertures_areas
   final_sum_err=(phot_table1['aperture_sum_err_0'])**2+bkg_sum_err**2
   phot_table1['residual_aperture_sum_err']=final_sum_err
   # rename columns for simplicity's sake
   phot_table1.rename_column('residual_aperture_sum', 'sum')
   phot_table1.rename_column('residual_aperture_sum_err','sum_err')
   #print(phot_table1['aperture_sum_0','residual_aperture_sum']) 
   # non so perché non funge. Secondo questo link dovrebbe: https://photutils.readthedocs.io/en/stable/aperture.html#performing-aperture-photometry
   # for i, obj in enumerate(cat):
   #    position = (obj.xcentroid.value, obj.ycentroid.value)
   #    apertures.append(CircularAperture(position, r))
   # phot=aperture_photometry(data,apertures, error=error)


   # 4 - fluxes from fixed elliptical aperture subtracting background
   for i,obj in enumerate(source_list):
      position = [data_row[obj,'x'], data_row[obj,'y']]
      aperture=CircularAperture(position, r=r)
      apertures.append(aperture)
      if obj == 'gal':
         phot_table2=aperture_photometry(conv_data - bkg.background, apertures[i],error=error)
         phot_table2.rename_column('id', 'id1')
         phot_table2['id1'][0] = i 
         id = Column([obj], name='id')
         phot_table2.add_column(id, index=0)
      else:
         cat2=aperture_photometry(conv_data - bkg.background, apertures[i],error=error)
         cat2.rename_column('id', 'id1')
         cat2['id1'][0] = i  
         id = Column([obj], name='id')
         cat2.add_column(id, index=0)
         phot_table2=vstack([phot_table2,cat2])
   #print(phot_table2['aperture_sum']) 
   phot_table2.rename_column('aperture_sum', 'sum')
   phot_table2.rename_column('aperture_sum_err','sum_err')


   # # 5 - fluxes from elliptical apertures obtained from segm
   # y, x = np.mgrid[:data.shape[1], :data.shape[0]]
   # e_apertures=[]
   # e_annuli_areas=[]
   # e_apertures_areas=[]
   # for i,obj in enumerate(cat):
   #    # Fit the data using a Gaussian
   #    g_init = models.Gaussian2D(amplitude=100., x_mean=obj.xcentroid.value, y_mean=obj.ycentroid.value, x_stddev=8, y_stddev=8)
   #    fit_g = fitting.LevMarLSQFitter()
   #    g = fit_g(g_init, x, y, data)
   #    #print(g.x_mean.value,g.x_fwhm,g.y_fwhm,g.theta)
   #    position = (obj.xcentroid.value, obj.ycentroid.value)
   #    e_aperture=EllipticalAperture(position, 0.5*g.x_fwhm, 0.5*g.y_fwhm, theta=g.theta.value)
   #    e_annulus=EllipticalAnnulus(position, 1.5*g.x_fwhm, 1.5*g.x_fwhm+4, 2*g.y_fwhm+4, theta=g.theta.value)
   #    apers = [e_aperture, e_annulus]
   #    e_apertures.append(e_aperture)
   #    e_apertures.append(e_annulus)
   #    if i == 0:
   #       phot_table3=aperture_photometry(data,apers,error=error)
   #       phot_table3['id'][0] = i  
   #    else:
   #       cat2=aperture_photometry(data,apers,error=error)
   #       cat2['id'][0] = i  
   #       phot_table3=vstack([phot_table3,cat2])
   #    e_annuli_areas.append(e_annulus.area)
   #    e_apertures_areas.append(e_aperture.area)
   # bkg_mean = phot_table3['aperture_sum_1'] / e_annuli_areas
   # bkg_sum = bkg_mean * e_apertures_areas
   # final_sum = phot_table3['aperture_sum_0'] - bkg_sum
   # phot_table3['residual_aperture_sum'] = final_sum
   # phot_table3['residual_aperture_sum'].info.format = '%.8g' 


   # # 6 - fluxes from fixed aperture subtracting background
   # for i, obj in enumerate(cat):
   #    if i == 0:
   #       phot_table4=aperture_photometry(data - bkg.background, e_apertures[i],error=error)
   #       phot_table4['id'][0] = i  
   #    else:
   #       cat2=aperture_photometry(data - bkg.background, e_apertures[i],error=error)
   #       cat2['id'][0] = i  
   #       phot_table4=vstack([phot_table4,cat2])


   # Plot data, apertures, background
   #norm = ImageNormalize(stretch=PowerStretch(3))
   name_size=15 #fontsize
   with plt.style.context(['seaborn-white','seaborn-bright']):
      fig, (ax1, ax3) = plt.subplots(2, 1, figsize=(10, 16.25))
      im1=ax1.imshow(conv_data, origin='lower', cmap='Greys_r',vmin=-50,vmax=250)
      for i,obj in enumerate(source_list):
         ax1.annotate(obj, (data_row[obj,'x']+20,data_row[obj,'y']+20),size=name_size,color='white')
      ax1.set_title('Convolved data')
      fig.colorbar(im1, ax=ax1)
      # ax2.imshow(segm, origin='lower',
      #          cmap=segm.cmap(random_state=12345))
      # ax2.set_title('Segmentation Image from unconvolved data')
      im3=ax3.imshow(bkg.background)
      ax3.invert_yaxis()
      fig.colorbar(im3, ax=ax3)
      ax3.set_title('Background from convolved data')
      for aperture in apertures:
         aperture.plot(color='white', lw=0.3, axes=ax1)
         #aperture.plot(color='white', lw=1.5, axes=ax2)
         aperture.plot(color='white', lw=1.5, axes=ax3)
      # for aperture in e_apertures:
      #    aperture.plot(color='green', lw=0.3, axes=ax1)
      #    aperture.plot(color='green', lw=1.5, axes=ax2)
      #    aperture.plot(color='green', lw=1.5, axes=ax3)
      #plt.savefig(output_file_folder+'centroid_{0}_{1}{2}.pdf'.format(object_name,bands[0],bands[1]), format = 'pdf', orientation = 'landscape', bbox_inches = 'tight',transparent=True) 
      plt.close(fig)


   return [phot_table1,phot_table2], fig
   #return [tbl1,tbl2,phot_table1,phot_table2], fig
   #return [tbl1,tbl2,phot_table1,phot_table2,phot_table3,phot_table4], fig

def find_target(fits_list,output_file_folder='./',stn_value=3):
   """
   Studies images until it finds a good one

   Input:
      - fits_list - list of fits file paths
      - stn_value - signal to noise threshold value for detections (Default 3).

   Output:
      - table with target sources
      - saves table to .npy file
      - flag saying weather it succeded
   """

   flag=0
   for fits_image_filename in fits_list:
      with fits.open(fits_image_filename) as hdul:
         print(fits_image_filename)
         data = hdul[0].data.astype(float)

         bkg_estimator = ModeEstimatorBackground() 
         bkg = Background2D(data, (32, 32), filter_size=(3, 3),
               bkg_estimator=bkg_estimator) #box size bigger than source size, but entire number should cover the field, so 32 o 64
         data_nobak=data-bkg.background
         threshold = bkg.background + (stn_value * bkg.background_rms)

         segm = detect_sources(data, threshold, npixels=40)
         
         if segm is None:
            check_plot(data)
            print('No detected sources. Skipping.')
            continue
         cat = source_properties(data_nobak, segm)

         table = cat.to_table(columns = ['id', 'xcentroid', 'ycentroid'])
         table['id']=np.arange(len(table)) 
         
         # check for stars, spurious sources, or lack of sources:
         check_plot(data,table)
         yn=yes_or_no(F"{len(table)} sources found. Do you want to use this image as a target? You'll be able to remove possible spurious sources later.")
         if yn == 0:
            # NO, I don't want to use this image as a target.
            continue
         else:
            # yn == 1 => YES, I want to use it
            while True:
               print(F'Enter spurious source numbers to be removed')
               r=input(F'Comma separated list, leave blank not to remove any:')
               if not r:
                  # if left blank, take all of them
                  print('You chose them all.')
                  r=list(table['id'])
                  break
               try:
                  # convert r from string to list
                  r = list(map(int, r.split(","))) 
                  print(F'You are removing {len(r)} sources.')
               except ValueError:
                  print('Please enter a valid list of source numbers, e.g.: 0,1,2')
                  continue
               if len(r) < len(table):
                  if len(r) > 1:
                     table.remove_rows(r)
                     print(F'Now there are {len(table)} sources in your target image.')
                     break
                  else:
                     table.remove_row(int(r[0]))
                     print(F'Now there are {len(table)} sources in your target image.')
                     break
               else:
                  # len(r) >= len(table):
                  print('Entered too many sources')
                  continue

         # skip image if not enough sources
         if len(table) < 2:
            check_plot(data,table)
            print(F'Data file {fits_image_filename[fits_image_filename.rfind("/")+1:-5]}: too few ({len(table)}) sources. Skipping.')
            continue

         while True:
            r=input('Please enter the GALAXY source number (as shown in image):')
            if not r:
               # ask for galaxy number again
               continue
            if len(r) > 1:
               print('Please enter the galaxy number only.')
               continue
            try:
               # convert r from string to list
               r = int(r)
               if r >= len(table):
                  print('Number out of sources range.')
                  continue
               #print(F'The galaxy is source number {r}.')
               table[r]['id']=-1
               break
            except ValueError:
               print('Please enter a valid integer number, e.g.: 3')
               continue

         # rearrange table order
         table.sort('id')
         table['id']=np.arange(len(table)) 

         # If image is good proceed to show it again and to renaming:
         print('New sources configuration:')
         check_plot(data,table)
         flag=1

      if flag==1:
         #print(table)
         table.write(output_file_folder+'target.txt',format='ascii',overwrite=True)
         break

   return table, flag

def source_find(fits_list,stn_value=3,output_file_folder='./',exp_sources=2,target=None, auto=False, tolerance=3):
   """
   # Scan and study PSF distribution

   Input:
      - fits_list - list of fits file paths
      - stn_value - signal to noise threshold value for detections (Default 3).
      - output_file_folder - file where to save files (Default "here").
      - exp_sources - Number of expected sources in the field. Needs to be >=2 (galaxy + ref star). If >2 target table is also necessary. (Default 2)
      - target - Astropy table with sources ID and coordinates of target image. Used to estimate exp_sources. If present rewrites exp_sources value.
      - auto - Boolean. If true skips the source removal phase. Can only be done if more than 2 expected sources and there is a target table. Default False.
      - tolerance - (pixels) tolerance in sources distances. This is used for source recognition in case #2 (exp_sources>2, found_sources=2).

   Output:
      - PSF_table - Saves the Pandas dataframe to output_file_folder+'Sources_positions.txt' with each fits file sources positions and the file path. Has also the (empty) PSF columns for psf_extr to fill them.
      - skipped_img - List of fits files whose elaboration wasn't successful because of non convergence of astroalign.findtransform or because of non detection of the galaxy.
   """
   if target:
      exp_sources=len(target)

   s=None
   skipped_img=[]

   if not target and exp_sources > 2:
      print("Please run find_target function and give target table in input.")
      return fits_list

   if exp_sources == 2:
      gal = None

   # Create empty table to place useful information for the study.
   # info necessary for multiindexing
   # galaxy only has x,y properties
   # if there already is a table saved it is opened so the user can continue filling it
   if path.exists(output_file_folder+'Sources_positions.txt'):
      PSF_table = pd.read_csv(output_file_folder+'Sources_positions.txt',sep='\t',header=[0,1],index_col=0)
   else:
      lev1=['gal']
      lev2=['x', 'y', 'dist', 'theta', 'path']
      codes1=[0,0] # two galaxy columns
      codes2=[0,1] # the first one is 'x', the second one is 'y'
      #stars
      for i in range(1, exp_sources):
         lev1 += ['star_'+str(i),'psf_'+str(i)]
         codes1+=[2*(i-1)+1,2*(i-1)+1,2*(i-1)+1]+[2*i,2*i,2*i]
         # the star has 'x', 'y' and 'dist'ance from the galaxy properties
         # the psf has 'x', 'y' and 'theta' properties
         codes2+=[0,1,2]+[0,1,3]
      # average psf
      lev1 += ['mean_psf']

      codes1 += [2*(exp_sources-1)+1,2*(exp_sources-1)+1,2*(exp_sources-1)+1]
      codes2 +=[0,1,3]
      #file_path
      lev1 += ['file']
      codes1 +=[2*(exp_sources-1)+2]
      codes2 +=[4]
      # columns definition
      levels=[lev1,lev2]
      codes=[codes1,codes2]
      columns=pd.MultiIndex(levels=levels,codes=codes,names=['object', 'property'])

      file_name=[u[u.rfind('/')+1:-5] for u in fits_list]
      # fill the table with a NaN values
      arr=np.full([len(fits_list),len(columns)], np.nan)
      PSF_table=pd.DataFrame(arr,columns=columns,index=file_name)
      PSF_table.loc[:,('file','path')]=fits_list
      PSF_table=PSF_table.sort_index(level=1)
      
   star_cols=[col[0].startswith('star_') or col[0].startswith('gal') for col in PSF_table.columns]

   if target:
      # define stars distance info
      dist=np.zeros(exp_sources)
      gals= np.empty(exp_sources,dtype=int)
      gals[:] = -1
      for i in range(1,exp_sources):
         dist[i]=((target['xcentroid'][0]-target['xcentroid'][i])**2 + (target['ycentroid'][0]-target['ycentroid'][i])**2)**(0.5)

   for fits_image_filename in fits_list:
      with fits.open(fits_image_filename) as hdul:
         print(fits_image_filename)
         filename=fits_image_filename[fits_image_filename.rfind('/')+1:-5]
         data = hdul[0].data.astype(float)

         bkg_estimator = ModeEstimatorBackground() 
         bkg = Background2D(data, (32, 32), filter_size=(3, 3),
               bkg_estimator=bkg_estimator) #box size bigger than source size, but entire number should cover the field, so 32 o 64
         data_nobak=data-bkg.background
         threshold = bkg.background + (stn_value * bkg.background_rms)

         #threshold = detect_threshold(data, snr=1.5)
         segm = detect_sources(data, threshold, npixels=40)

         #plot segmentation image
         #check_plot(segm)
         
         if segm is None:
            check_plot(data)
            print('No detected sources. Skipping.')
            PSF_table=PSF_table.drop(filename)
            continue
         cat = source_properties(data_nobak, segm)

         table = cat.to_table()
         table['id']=np.arange(len(table)) 

         if len(table) == 1:
            check_plot(data,table)
            print('Not enough detected sources (just 1). Skipping.')
            PSF_table=PSF_table.drop(filename)
            continue

         # check for stars, spurious sources, or lack of sources:
         check_plot(data,table)

         if auto and exp_sources == 2:
            auto=False

         if auto is False:
            while True:
               print(F'{len(table)} sources found, enter spurious source numbers to be removed')
               r=input(F'Comma separated list, leave blank not to remove any:')
               if not r:
                  # if left blank, take all of them
                  print('You kept them all.')

                  # before breaking check that there is a valid amount of sources in the table
                  if exp_sources==2 and len(table) > exp_sources:
                     print('Too many sources, please remove more sources and leave the galaxy and star only.')
                     continue
                  else:
                     r=list(table['id'])
                     break
               try:
                  # convert r from string to list
                  r = list(map(int, r.split(","))) 
                  print(F'You selected to remove {len(r)} sources.')
               except ValueError:
                  print('Please enter a valid list of source numbers, e.g.: 0,1,2')
                  continue
               if len(r) <= len(table) and max(r) < len(table):
                  if len(r) > 1:
                     table.remove_rows(r)
                     print(F'Now there are {len(table)} sources in your science image.')
                  else:
                     table.remove_row(int(r[0]))
                     print(F'Now there are {len(table)} sources in your science image.')

                  # before breaking check that there is a valid amount of sources in the table
                  if exp_sources==2 and len(table) > exp_sources:
                     print('Too many sources, please remove more sources and leave the galaxy and star only.')
                     continue
                  else:
                     break
               else:
                  # len(r) >= len(table) or elements out of range:
                  print('Entered either too many sources or a number out of range.')
                  continue
               if exp_sources==2 and len(table) > exp_sources:
                  print('Too many sources, please remove more sources and leave the galaxy and star only.')
                  continue

         # skip image if not enough sources
         if len(table) < 2:
            check_plot(data,table)
            print(F'Data file {fits_image_filename[fits_image_filename.rfind("/")+1:-5]}: too few ({len(table)}) sources. Skipping.')
            #PSF_table=PSF_table.drop(filename) 
            skipped_img.append(fits_image_filename)
            continue

         # Only 2 sources detected and 2 expected
         if len(table) == 2 and exp_sources == 2:
            print('case #1')
            # need to figure out which one the galaxy is and place it in the right place in the table
            # we assume the galaxy will always have the same position
            if gal == None:
               while True:
                  gal=input('Please enter the GALAXY source number (as shown in image):')
                  if not gal:
                     # ask for galaxy number again
                     continue
                  if len(gal) > 1:
                     print('Please enter the galaxy number only.')
                     continue
                  try:
                     # convert r from string to list
                     gal = int(gal)
                     if gal >= len(table):
                        print('Number out of sources range.')
                        continue
                     print(F'The galaxy is source number {gal}.')
                     break
                  except ValueError:
                     print('Please enter a valid integer number, e.g.: 1')
                     continue
            # rearrange table order if galaxy is not n=0
            if gal > 0:
               table[gal]['id']=-1
               table.sort('id')
               table['id']=np.arange(len(table)) 
            # if you removed a source in "if auto is False:" you need to make sure source numbers are 0 and 1
            if table['id'].max() > 1:
               table['id']=np.arange(len(table)) 

         # If >=3 sources detected and >=3 expected 
         if len(table) > 2 and exp_sources > 2:
            print('case #2')
            #check_plot(data,target,table)
            # compare table and target with astroalign.
            # check parameter aa.MIN_MATCHES_FRACTION at the beginning in case of not so good transformations
            # build tables to be compared
            target_table=np.array([target['xcentroid'],target['ycentroid']]).T
            # need an ID column to maintain sorting of sources
            target['id']=np.arange(len(target_table)) 
            source_table=np.array([table['xcentroid'],table['ycentroid']]).T
            try:
               p, (source_list, target_list) = aa.find_transform(source_table,target_table)
            except:# MaxIterError as err:
               #print('Handling run-time error:', err)
               print('aa.FindTransform could not converge. Skipping image.')
               skipped_img.append(fits_image_filename)
               continue

            # stack astroalign output
            matched_table= hstack([Table(source_list,names=('x_s','y_s')),Table(target_list,names=('xcentroid','ycentroid'))])
            # join it with the target keeping all rows from the target even if there is no match in the source image (left join)
            sorted_complete=join(target/u.pix,matched_table,keys=['xcentroid','ycentroid'],join_type='left')
            # sort it to the original order
            sorted_complete.sort('id')
            # select the useful columns, give them the right units and rename them
            table=sorted_complete['id','x_s','y_s']
            table['id'].unit = None
            table['x_s'].unit = u.pix
            table['y_s'].unit = u.pix
            table.rename_column('x_s','xcentroid')
            table.rename_column('y_s','ycentroid')
         
         # Only 2 sources detected but more expected
         if len(table) == 2 and exp_sources > 2:
            print('case #3')

            # compare with target distances
            success=False
            if auto:
               data_dist=((table['xcentroid'][0]-table['xcentroid'][1])**2 + (table['ycentroid'][0]-table['ycentroid'][1])**2)**(0.5)
               for n,d in enumerate(dist):
                  if (data_dist/ u.pix > d - tolerance) and (data_dist/ u.pix < d + tolerance):
                     check_plot(data,target,table)
                     print(f'Probable detection of star_{n}.')
                     display (Markdown('<span style="color: #000000; background-color:#ffffff;">Target sources in color </span><span style="color: #000080; background-color:#ffffff;">**BLUE**.</span>'))          
                     keep_going = yes_or_no('Is the detection correct?',default="yes")
                     if keep_going == 0:
                        print(F'Skipping data file {fits_image_filename[fits_image_filename.rfind("/")+1:-5]}.')
                        #PSF_table=PSF_table.drop(filename) 
                        skipped_img.append(fits_image_filename)
                        continue
                     else:
                        success=True

                     if gals[n] < 0:
                        while True:
                           display (Markdown('Please enter the <span style="color: #b22222">**GALAXY**</span> source number as shown in image in color <span style="color: #b22222">**RED**</span>'))
                           gal=input(':')
                           #gal=input('Please enter the GALAXY source number (as shown in image):')
                           if not gal:
                              # ask for galaxy number again
                              continue
                           if len(gal) > 1:
                              print('Please enter the galaxy number only.')
                              continue
                           try:
                              # convert r from string to list
                              gal = int(gal)
                              if gal >= len(table):
                                 print('Number out of sources range.')
                                 continue
                              print(F'The galaxy is source number {gal}.')
                              break
                           except ValueError:
                              print('Please enter a valid integer number, e.g.: 1')
                              continue
                        gals[n]=gal
                        ## find transformation matrix
                     else:
                        gal=gals[n]
                     # rearrange table order if galaxy is not n=0
                     if gal > 0:
                        table[0]['id']=n
                        table[gal]['id']=0
                        table.sort('id')
                     else:
                        table[1]['id']=n

            if not success:
               if auto:
                  keep_going = yes_or_no('Are both sources valid?',default="yes")
                  if keep_going == 0:
                     print(F'Skipping data file {fits_image_filename[fits_image_filename.rfind("/")+1:-5]}.')
                     #PSF_table=PSF_table.drop(filename) 
                     skipped_img.append(fits_image_filename)
                     continue
               else:
                  check_plot(data,target,table)

               while True:
                  display (Markdown('Please enter the <span style="color: #b22222">**GALAXY**</span> source number as shown in image in color <span style="color: #b22222">**RED**</span>'))
                  gal=input(':')
                  if not gal:
                     # ask for galaxy number again
                     continue
                  if len(gal) > 2:
                     print('Please enter the galaxy number only.')
                     continue
                  try:
                     # convert r from string to list
                     gal = int(gal)
                     if gal not in table['id']:
                        print('Number out of sources range.')
                        continue
                     #print(F'The galaxy is source number {gal}.')
                     break
                  except ValueError:
                     print('Please enter a valid integer number, e.g.: 1')
                     continue

               while True:
                  display (Markdown('<span style="color: #000000; background-color:#ffffff;">Please enter the corresponding</span><span style="color: #000080; background-color:#ffffff;"> **STAR** </span><span style="color: #000000; background-color:#ffffff;">source number as shown in image in color </span><span style="color: #000080; background-color:#ffffff;">**BLUE**:</span>'))          
                  Tstar=input(':')
                  if not Tstar:
                     # ask for star number again
                     continue
                  if len(Tstar) > 1:
                     print('Please enter the star number only.')
                     continue
                  try:
                     # convert r from string to list
                     Tstar = int(Tstar)
                     if Tstar not in target['id']:
                        print('Number out of sources range.')
                        continue
                     #print(F'The star is source number {Tstar}.')
                     break
                  except ValueError:
                     print('Please enter a valid integer number, e.g.: 1')
                     continue
            
               # need auxiliary table as a reference when rewriting
               # (because otherwise it doesn't work when e.g. Tstar=3,gal=3, both id become =0)
               table_tmp=Table(table,copy=True)
               # rename the star
               table['id'][table_tmp['id']!=gal]=Tstar
               if gal > 0:
                  # rename the galaxy
                  table['id'][table_tmp['id']==gal]=0
                  # rearrange table order
                  table.sort('id')
            else:
               # If success, transform target into table
               table=_transform_target(table,target)

         # If image is good proceed show it again:
         print('New sources configuration:')
         check_plot(data,table)

         non_s=0
         # define which stars are the present in order to fill the table
         mask=np.array([])
         if np.ma.is_masked(table['xcentroid']):
            mask = table['xcentroid'].mask.nonzero()[0]
            cond=np.setdiff1d(np.arange(1,len(table)),mask)
         else:
            cond=np.arange(1,len(table))
         stars=table['id'][cond]

         # erase previous content of the row
         PSF_table.loc[filename,star_cols]=np.nan

         # Save galaxy position
         if not np.ma.is_masked(table['xcentroid'][non_s]):
            if isinstance(table['xcentroid'][non_s],float):
               PSF_table.loc[filename,('gal','x')] = table['xcentroid'][non_s] 
               PSF_table.loc[filename,('gal','y')] = table['ycentroid'][non_s]
            else:
               PSF_table.loc[filename,('gal','x')] = table['xcentroid'][non_s] / u.pix
               PSF_table.loc[filename,('gal','y')] = table['ycentroid'][non_s] / u.pix
         else:
            print('Galaxy not detected, skipping')
            skipped_img.append(fits_image_filename)
            continue
         
         # save stars' information
         for s in stars:
            coords = Table(table[table['id']==s]['xcentroid','ycentroid'], names=('x','y'))
            if not isinstance(coords['x'],float):
               coords['x'] = coords['x'] / u.pix
               coords['y'] = coords['y'] / u.pix

            PSF_table.loc[filename,('star_'+str(s),'x')]=coords['x']
            PSF_table.loc[filename,('star_'+str(s),'y')]=coords['y']

            PSF_table.loc[filename,('star_'+str(s),'dist')]=((coords['x']-PSF_table.loc[filename,('gal','x')])**2+(coords['y']-PSF_table.loc[filename,('gal','y')])**2)**0.5
            
         #print(PSF_table.loc[filename])

   PSF_table.to_csv(output_file_folder+'Sources_positions.txt',sep='\t')
   return skipped_img

def _transform_target(table,target):
   """
   Translate the target table to the position of current data. For internal use.
   """
   #transform tables to numpy arrays
   target_data=np.lib.recfunctions.structured_to_unstructured(np.array(target[table['id']]['xcentroid','ycentroid']))
   table_data=np.lib.recfunctions.structured_to_unstructured(np.array(table['xcentroid','ycentroid']))
   
   # determine transformation between tables
   tform = transform.estimate_transform('similarity', target_data, table_data)
   
   # transform target and save to a new table
   complete_target_data = np.lib.recfunctions.structured_to_unstructured(np.array(target['xcentroid','ycentroid']))
   new_table = Table(tform(complete_target_data), names=('xcentroid','ycentroid'))
   new_table['id']=np.arange(len(target))
   return new_table

def psf_extr(PSF_table,extended,output_file_folder='./'):
   """
   # Study PSF distribution of sources with coordinates on PSF_table.

   Input:
      - extended - Pandas series saying which sources are extended and which not, we need only stars for PSF extraction
      - output_file_folder - file where to save files (which files?) (Default "here").

   Output:
      - Saves plot output_file_folder+'peaks_fits_image_filename.pdf' with sources and PSF shape
      - PSF_table - Saves the (now more complete) Pandas dataframe to output_file_folder+'PSF_distrib.txt' with each fits file sources positions, stars PSF parameters, a mean PSF and the file path.
   """

   #redefine list of files based on what is in the table
   fits_list=PSF_table.loc[:,('file','path')].array

   for fits_image_filename in fits_list:
      print(fits_image_filename)
      with fits.open(fits_image_filename) as hdul:
         filename=fits_image_filename[fits_image_filename.rfind('/')+1:-5]
         data = hdul[0].data.astype(float)
         bkg_estimator = ModeEstimatorBackground() 
         bkg = Background2D(data, (32, 32), filter_size=(3, 3),
               bkg_estimator=bkg_estimator) #box size bigger than source size, but entire number should cover the field, so 32 o 64
         data_nobak=data-bkg.background
         # retrieve all 'star_X' whose 'x' is not NaN.
         # especially make an int array of Xs. Maybe not int and remove the str=s
         star_xcols=[col[0].startswith('star_') and col[1].startswith('x') for col in PSF_table.columns]
         defined_el = PSF_table.loc[filename, :].notnull()
         # star 'x' columns that are also not null
         col_filter = np.logical_and(star_xcols,defined_el)
         # retrieve the part of the name of the column after the '_' character, and select non extended sources.
         stars=[int(col[0].partition('_')[2]) for col in PSF_table.loc[filename, col_filter].index if not extended[int(col[0].partition('_')[2])]]

         for s in stars:
            # recreate coords table for the star
            #coords = Table.from_pandas(PSF_table.loc[filename,('star_'+str(s),('x','y'))])
            coords = PSF_table.loc[filename,('star_'+str(s),('x','y'))].to_numpy()
            coords = Table(coords, names=('x','y'))
            
            # actual measurement of the PSF
            PSF_table.loc[filename,('psf_'+str(s),'x')], PSF_table.loc[filename,('psf_'+str(s),'y')], PSF_table.loc[filename,('psf_'+str(s),'theta')] = measure_PSF(data_nobak,coords)

            # Impose x be minor axes
            if PSF_table.loc[filename,('psf_'+str(s),'x')] > PSF_table.loc[filename,('psf_'+str(s),'y')]:
               # swap values
               PSF_table.loc[filename,('psf_'+str(s),'y')],PSF_table.loc[filename,('psf_'+str(s),'x')] = PSF_table.loc[filename,('psf_'+str(s),'x')], PSF_table.loc[filename,('psf_'+str(s),'y')]
               if PSF_table.loc[filename,('psf_'+str(s),'theta')] > 0:
                  PSF_table.loc[filename,('psf_'+str(s),'theta')]=PSF_table.loc[filename,('psf_'+str(s),'theta')]-pi/2
               else:
                  PSF_table.loc[filename,('psf_'+str(s),'theta')]=PSF_table.loc[filename,('psf_'+str(s),'theta')]+pi/2

         # need to average psf values
         psf_cols=['psf_'+str(s) for s in stars]
         PSF_table.loc[filename,('mean_psf','x')]=PSF_table.loc[filename,(psf_cols,'x')].mean()
         PSF_table.loc[filename,('mean_psf','y')]=PSF_table.loc[filename,(psf_cols,'y')].mean()
         PSF_table.loc[filename,('mean_psf','theta')]=PSF_table.loc[filename,(psf_cols,'theta')].mean()
         
         star_cols=['star_'+str(s) for s in stars]
         x=PSF_table.loc[filename,(star_cols,('x'))].array
         y=PSF_table.loc[filename,(star_cols,('y'))].array
         positions=np.array([x,y]).T

         # Plot
         apertures = EllipticalAperture(positions, PSF_table.loc[filename,('mean_psf','x')], PSF_table.loc[filename,('mean_psf','y')], theta=PSF_table.loc[filename,('mean_psf','theta')])
         #apertures = CircularAperture(positions, r=5.)
         norm = simple_norm(data, 'sqrt', percent=99.9)
         plt.imshow(data, cmap='Greys_r', origin='lower', norm=norm)
         apertures.plot(color='#0547f9', lw=0.3)
         plt.xlim(0, data.shape[1]-1)
         plt.ylim(0, data.shape[0]-1)
         for i,s in enumerate(stars):
            plt.annotate(str(s), (positions[i,0]+20,positions[i,1]+20),size=13,color='black')
         plt.savefig(output_file_folder+'peaks_{0}.pdf'.format(fits_image_filename[fits_image_filename.rfind('/')+1:-5]), format = 'pdf', bbox_inches = 'tight',transparent=True)
         plt.close()

   PSF_table.to_csv(output_file_folder+'PSF_distrib.txt',sep='\t')

   plot_table=PSF_table.loc[:,'mean_psf']

   #plot_PSF(plot_table, output_file_folder)

def check_plot(data,table=None,table2=None):
   """
   Input:
      - data - image data as np.array
      - table - (optional) Astropy table with sources positions. Column names must be id, xcentroid, ycentroid, as returned by the source_properties function.

   Output:
      - Shows (does not return or save to file) plot with data and sources from table.
   """
   if table:
      positions=np.array([table['xcentroid'],table['ycentroid']])
      positions=positions.transpose()
      apertures = CircularAperture(positions, r=5.)
   if table2:
      positions=np.array([table2['xcentroid'],table2['ycentroid']])
      positions=positions.transpose()
      apertures2 = CircularAperture(positions, r=4.)

   norm = simple_norm(data, 'sqrt', percent=99.9)
   plt.imshow(data, cmap='Greys_r', origin='lower', norm=norm)
   plt.xlim(0, data.shape[1]-1)
   plt.ylim(0, data.shape[0]-1)

   if table:
      col='#000080'
      apertures.plot(color=col, lw=0.6)
      for obj in table:
         if isinstance(obj['xcentroid'],float):
            plt.annotate(str(obj['id']), (obj['xcentroid']+20,obj['ycentroid']+20),size=13,color=col)
         elif isinstance(obj['xcentroid'],np.ma.core.MaskedConstant):
            continue
         else:
            plt.annotate(str(obj['id']), (obj['xcentroid'].value+20,obj['ycentroid'].value+20),size=13,color=col)
   if table2:
      col='#b22222'
      apertures2.plot(color=col, lw=0.6)
      for obj in table2:
         if isinstance(obj['xcentroid'],float):
            plt.annotate(str(obj['id']), (obj['xcentroid']+20,obj['ycentroid']+20),size=13,color=col)
         elif isinstance(obj['xcentroid'],np.ma.core.MaskedConstant):
            continue
         else:
            plt.annotate(str(obj['id']), (obj['xcentroid'].value+20,obj['ycentroid'].value-20),size=13,color=col)

   plt.show()
   plt.close()

def plot_PSF(PSF_table, output_file_folder='./', vals=[0.50,0.80,0.90,0.95,0.99]):
   """
   Input:
      - PSF_table - Pandas table
      - output_file_folder - Path to save image files (default "here")
      - vals - Values to estimate percentiles for the plot (fractions of 1) (default 50,80,90,95,99).

   Output:
      - Figure with PSF distribution (output_file_folder+'PSF_distrib.pdf')
      - Figure with PSF 2D distribution (output_file_folder+'PSF_2d_distrib.pdf)
   """
   # Do statistics
   quantiles=PSF_table.quantile(vals,interpolation='nearest')
   print(quantiles)
   print(PSF_table.describe())

   with plt.style.context(['seaborn-white','seaborn-bright','seaborn-ticks']):
      axes=PSF_table.hist(bins='auto',figsize=(10, 10))
      fig = axes[0,0].get_figure()
      for plot in axes.flatten():
         plot.minorticks_on()
      _, max1_ = axes[0,1].get_ylim()
      _, max2_ = axes[1,0].get_ylim()
      for i,val in enumerate(vals):
         perc_x=quantiles.loc[val,('mean_psf','x')]
         perc_y=quantiles.loc[val,('mean_psf','y')]
         axes[0,1].axvline(perc_x, color='k', linestyle='dashed', linewidth=1)
         axes[1,0].axvline(perc_y, color='k', linestyle='dashed', linewidth=1)
         axes[0,1].text(perc_x + perc_y/40, max1_ - (i+1)*max1_/10, 
         F'{val} perc: {perc_x:.2f}')
         axes[1,0].text(perc_y + perc_y/40, max2_ - (i+1)*max2_/10, 
         F'{val} perc: {perc_y:.2f}')

      fig.savefig(output_file_folder+'PSF_distrib.pdf', format = 'pdf', bbox_inches = 'tight',transparent=True)
      plt.close(fig)

   styles_vec=['-','--','-.',':']
   n_appends=np.int(np.trunc(len(vals)/len(styles_vec)))
   n_adds=len(vals)%len(styles_vec)
   linestyle=n_appends*styles_vec+styles_vec[:n_adds]

   with plt.style.context(['seaborn-white','seaborn-bright','seaborn-ticks']):
      plt.hist2d(PSF_table['mean_psf','x'],PSF_table['mean_psf','y'], bins=100,range=[[1,quantiles.loc[0.95,('mean_psf','x')]+2],[1,quantiles.loc[0.95,('mean_psf','y')]+2]])  # arguments are passed to np.histogram
      for i,(val,ls) in enumerate(zip(vals,linestyle)):
         perc_x=quantiles.loc[val,('mean_psf','x')]
         perc_y=quantiles.loc[val,('mean_psf','y')]
         plt.axvline(perc_x, color='k', linestyle=ls, linewidth=1)
         plt.axhline(perc_y, color='k', linestyle=ls, linewidth=1)
      plt.savefig(output_file_folder+'PSF_2d_distrib.pdf', format = 'pdf', bbox_inches = 'tight',transparent=True)
      plt.close(fig)
      
def measure_PSF(data_nobak,coords):
   """
   Input:
      - data_nobak - Background subtracted data as np.array
      - Coords - Star coordinates, in order to extract its PSF. As Pandas series with just 'x' and 'y' indices.

   Output:
      - g.x_stddev.value - PSF minor axis width (STD dev)
      - g.y_stddev.value - PSF major axis width (STD dev)
      - g.theta.value - PSF minor axis width 
   """
   # data has to be background subtracted
   # Fit the star using an asymmetric 2D Gaussian
   cutout_size=30
   y, x = np.mgrid[:data_nobak.shape[1], :data_nobak.shape[0]]
   bounds={"theta":[-pi/2,pi/2], 
            "x_stddev":[0,cutout_size], 
            "y_stddev":[0,cutout_size]}
   g_init = models.Gaussian2D(amplitude=100., x_mean=coords['x'], y_mean=coords['y'], x_stddev=7, y_stddev=7,bounds=bounds)
   fit_g = fitting.LevMarLSQFitter()
   g = fit_g(g_init, x, y, data_nobak,maxiter=300)
   return g.x_stddev.value,g.y_stddev.value,g.theta.value

   # Model stars
   # nddata = NDData(data=data_nobak)
   # stars = extract_stars(nddata, coords, size=cutout_size)
   # #plt.imshow(stars[0])
   # epsf_builder = EPSFBuilder(oversampling=1, maxiters=10,
   #                            progress_bar=False)
   # epsf, fitted_stars = epsf_builder(stars)

   # norm = simple_norm(epsf.data_nobak, 'log', percent=99.)
   # plt.imshow(epsf.data_nobak, norm=norm, origin='lower', cmap='viridis')
   # plt.colorbar()

   # y, x = np.mgrid[:epsf.data.shape[1], :epsf.data.shape[0]]
   # Fit the star using a simmetric Gaussian
   # gaussian_prf = IntegratedGaussianPRF(sigma=13,x_0=epsf.x_0.value, y_0=epsf.y_0.value)
   # gaussian_prf.sigma.fixed = False
   # fit_g = fitting.LevMarLSQFitter()
   # g = fit_g(gaussian_prf, x, y, epsf.data,maxiter=300)
   # #print(fit_g.fit_info['message'])
   # print(g.sigma.value)
   # return g.sigma.value

   # Fit the star using an asymmetric 2D Gaussian
   # bounds={"theta":[-pi/2,pi/2], 
   #          "x_stddev":[1,cutout_size], 
   #          "y_stddev":[1,cutout_size]}
   # g_init = models.Gaussian2D(amplitude=100., x_mean=epsf.x_0.value, y_mean=epsf.y_0.value, x_stddev=7, y_stddev=7,bounds=bounds)
   # fit_g = fitting.LevMarLSQFitter()
   # g = fit_g(g_init, x, y, epsf.data,maxiter=300)
   # print(g.x_stddev.value,g.y_stddev.value,g.theta.value)
   # return g.x_stddev.value,g.y_stddev.value,g.theta.value

def conv_flux(PSF_raw,perc_threshold,reference_psf_factor,n_tables,good_table,stars,band,output_file_folder='./'):
   # Select reference image for aperture and convolution:
   bigger_x=PSF_raw.loc[:,('mean_psf','x')].quantile(perc_threshold,interpolation='nearest')
   bigger_y=PSF_raw.loc[:,('mean_psf','y')].quantile(perc_threshold,interpolation='nearest')
     
   # y is minor axis
   reference_psf=bigger_x

   # Remove bad images from list
   low_x=(PSF_raw.loc[:,('mean_psf','x')] <= bigger_x)
   low_y=(PSF_raw.loc[:,('mean_psf','y')] <= bigger_y)
   PSF_table=PSF_raw[low_x & low_y]

   # source detection definitions
   r = reference_psf*0.5    # approximate isophotal extent
   annulus=reference_psf*reference_psf_factor
   dannulus=4

   # Light curves tables initialization
   lc_tables=[None]*n_tables
   columns=['date','JD','gal_sum', 'gal_sum_err', 'gal_flux', 'gal_flux_err']
   for i in range(1, len(stars)+1):
            columns += ['star'+str(i)+'_sum','star'+str(i)+'_sum_err',
                        'ratio'+str(i),'ratio'+str(i)+'_err']
   arr=np.zeros((len(PSF_table),len(columns)))
   str_arr=np.empty(len(PSF_table),dtype="<U30")
   for i in range(n_tables):
      lc_tables[i]=Table(arr,names=columns, meta={'keywords': {'key1': 'val1'}})
      lc_tables[i].add_column(Column(str_arr,dtype="<U30"),name='file_name',index=0)

   # Read fits file and measure sources
   for i,fits_image_filename in enumerate(PSF_table.loc[:,('file','path')]):
      with fits.open(fits_image_filename) as hdul:
            filename=fits_image_filename[fits_image_filename.rfind("/")+1:-5]
            print(fits_image_filename)
            data = hdul[0].data.astype(float) 
            hdr=hdul[0].header
            time=hdr['JD']-2450000.5
            date=fits_image_filename[fits_image_filename.rfind("/")+12:-5]

            data_row=PSF_table.loc[filename]

            conv_data, fig=convolution(data,data_row,bigger_x,bigger_y)
            fig.savefig(output_file_folder+'kernel_{0}.pdf'.format(filename), format = 'pdf', bbox_inches = 'tight',transparent=True)

            tables,fig=image_fluxes(data,conv_data,data_row,r,annulus,dannulus)
            fig.savefig(output_file_folder+'phot_conv_{0}.pdf'.format(filename), format = 'pdf', bbox_inches = 'tight',transparent=True)

            for j,t in enumerate(tables):
               lc_tables[j]['JD'][i] = time 
               lc_tables[j]['date'][i] = date 
               lc_tables[j]['file_name'][i] = filename 
               
               stars_field=[]
               for obj in t:
                  if obj['id'].startswith('star'):
                     num=obj['id'][-1:]
                     stars_field.append(int(num))
                     lc_tables[j]['star'+str(num)+'_sum'][i] = obj['sum']
                     lc_tables[j]['star'+str(num)+'_sum_err'][i] = obj['sum_err']

                     lc_tables[j]['ratio'+str(num)][i] = t[0]['sum']/obj['sum']
                     lc_tables[j]['ratio'+str(num)+'_err'][i] = error_propagation(t[0]['sum'],t[0]['sum_err'],
                                                                                    obj['sum'],obj['sum_err'])
                  else:
                     lc_tables[j]['gal_sum'][i] = obj['sum']
                     lc_tables[j]['gal_sum_err'][i] = obj['sum_err']

               lc_tables[j]['gal_flux'][i] = t[0]['sum'] / t[1:]['sum'].sum() * stars[band][stars_field].sum()
               lc_tables[j]['gal_flux_err'][i] = error_propagation(t[0]['sum'],t[0]['sum_err'],t[1:]['sum'],t[1:]['sum_err'],
                                                                  stars[band][stars_field], stars[band+'_err'][stars_field])

   return PSF_table, lc_tables

def error_propagation(gal_ct, gal_cterr, stars_ct, stars_cterr, stars_flux=None, stars_fluxerr=None):
   # formula for error propagation of galaxy flux
   stars_cterr2=stars_cterr**2
    
   if stars_flux is None or stars_fluxerr is None:
      return np.sqrt(gal_cterr**2 + gal_ct**2*stars_cterr2.sum()/(stars_ct.sum())**2) / stars_ct.sum()
   else:
      stars_fluxerr2=stars_fluxerr**2
      return np.sqrt((gal_cterr**2 + gal_ct**2*stars_cterr2.sum()/(stars_ct.sum())**2)/stars_ct.sum()**2 * stars_flux.sum()**2 +
                     (gal_ct/stars_ct.sum())**2 * stars_fluxerr2.sum() )

def most_frequent_star(PSF_table):
   # returns the star number of the star that has the most detections in the field
   star_xcols=[col[0].startswith('star_') and col[1].startswith('x') for col in PSF_table.columns]
   describe_xcols = PSF_table.describe().loc['count',star_xcols[:-1]]
   max_count = describe_xcols.max()
   good_star_df = describe_xcols == max_count
   good_star=int(describe_xcols.index.get_level_values(0)[good_star_df].values[0][-1])
   return good_star

def convolution(data,data_row,final_x=4,final_y=6):
   """
   Input:
      - data: Image data as np.array
      - data_row: Pandas series with info about the image. The needed info is about the PSF (x,y,theta)
      - final_x: Target PSF minor axis (default=4)
      - final_y: Target PSF major axis (default=6)

   Output:
      - conv_data: Convolved image data as np.array
      - fig: Plot of the convolution kernel, useful for visual inspection and verify quality of transformation.
   """

   # convolves to a given sigma with a circular Gaussian kernel
   # smoothing through convolution with gaussian
   if (data_row['mean_psf','x'] > final_x) | (data_row['mean_psf','y'] > final_y):
      print(F"Something's wrong: PSF_x = {data_row['mean_psf','x']} (max {final_x}), PSF_y = {data_row['mean_psf','y']} (max {final_y})")
      fig= plt.figure()
      return data,fig
   else:
      n_size=6
      kernel_size=np.rint(n_size*final_x)
      if kernel_size % 2 == 0:
         kernel_size+=1 # making sure kernel size is odd
      center=n_size/2*final_x
      y, x = np.mgrid[:kernel_size, :kernel_size]
      gm_data=Gaussian2D(100,center,center,data_row['mean_psf','x'],data_row['mean_psf','y'],data_row['mean_psf','theta'])
      gm_ref=Gaussian2D(100,center,center,final_x,final_y,data_row['mean_psf','theta'])
      g_data = gm_data(x, y)
      g_ref = gm_ref(x, y)
      g_data /= g_data.sum()
      g_ref /= g_ref.sum()

      window = TopHatWindow(0.4)
      kernel = create_matching_kernel(g_data, g_ref,window=window)
      conv_data = convolve(data, kernel)
   
      # plot kernel
      with plt.style.context(['seaborn-white','seaborn-bright','seaborn-ticks']):
         fig = plt.figure()
         plt.imshow(kernel, interpolation='none', origin='lower')
         plt.title('Kernel')
         plt.colorbar()
         plt.close()
         plt.text(2, kernel.shape[1]-4, F"data PSF_x = {data_row['mean_psf','x']} (ref {final_x}), data PSF_y = {data_row['mean_psf','y']} (ref {final_y})", color='w')
         # stampa su figura F"data PSF_x = {data_row['mean_psf','x']} (ref {final_x}), data PSF_y = {data_row['mean_psf','y']} (ref {final_y})"
      
      return conv_data,fig

def convol_bak(data,data_row,ref_row):
   """
   Input:
      - data: Image data as np.array
      - data_row: Pandas series with info about the image. The needed info is about the PSF (x,y,theta)
      - ref_row: Pandas series with info about the reference image. The needed info is still the PSF

   Output:
      - conv_data: Convolved image data as np.array
      - fig: Plot of the convolution kernel, useful for visual inspection and verify quality of transformation.
   """
   # use the gaussian with the limiting sigma found from psf_extr to match the psf of current data

   if data_row['PSF_x'] >= ref_row['PSF_x']:
      print("Convolved image exists")
      fig= plt.figure()
      return data,fig
   else:
      n_size=6
      kernel_size=n_size*ref_row['PSF_x']+1
      center=n_size/2*ref_row['PSF_x']
      y, x = np.mgrid[:kernel_size, :kernel_size]
      gm_data=Gaussian2D(100,center,center,data_row['PSF_x'],data_row['PSF_y'],data_row['theta'])
      gm_ref=Gaussian2D(100,center,center,ref_row['PSF_x'],ref_row['PSF_y'],ref_row['theta'])
      g_data = gm_data(x, y)
      g_ref = gm_ref(x, y)
      g_data /= g_data.sum()
      g_ref /= g_ref.sum()

      window = TopHatWindow(0.3)
      kernel = create_matching_kernel(g_data, g_ref,window=window)
      conv_data = convolve(data, kernel)

      # plot kernel
      with plt.style.context(['seaborn-white','seaborn-bright','seaborn-ticks']):
         fig = plt.figure()
         plt.imshow(kernel, interpolation='none', origin='lower')
         plt.title('Kernel')
         plt.colorbar()
         plt.close()
   
      return conv_data,fig

def yes_or_no(question, default="yes"):
   """Ask a yes/no question via input() and return their answer.

   Input:
      - question - is a string that is presented to the user.
      - default - is the presumed answer if the user just hits <Enter>.
         It must be "yes" (the default), "no" or None (meaning
         an answer is required of the user).

   The "answer" return value is True for "yes" or False for "no".
   """
   if default is None:
      prompt = " (y/n): "
   elif default == "yes":
      prompt = " ([y]/n): "
      def_out = 1
   elif default == "no":
      prompt = " (y/[n]): "
      def_out = 0
   else:
      raise ValueError(F"invalid default answer: '{default}'")

   reply = str(input(question + prompt)).lower().strip()
   if len(reply) == 0:
      if default:
         return def_out
      else:
         return yes_or_no("Please Enter")
   if reply[0] == 'y':
      return 1
   elif reply[0] == 'n':
      return 0
   else:
      # if answer is not valid
      return yes_or_no("Please Enter")
