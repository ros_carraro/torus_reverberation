#%%
#!/usr/bin/python
#-*- coding: utf-8 -*-
"""
Makes plots of the light curves in all available bands
for each object
"""

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from astropy.table import Table
sys.path.append('/Users/Ros/Google_Drive/Valparaiso/Light_curves/Code/')
from utilities import is_outlier

"""
missing light curves:
IRAS04505/K
LB1727/H
TONS180/K
IRAS16355/J
IRAS16355/H
IRAS16355/K
"""
#%%
path_name=["IRAS04505","LB1727","TONS180","IRAS16355","ESO354"]
object_name=["IRAS04505-2958","LB1727","TON S 180","IRAS16355-2049","ESO354-G04"]

output_file_folder="/Users/Ros/Google_Drive/Valparaiso/Light_curves/Optical_CCF/"

galaxies=pd.DataFrame(index=path_name)

# define a common path to the light curve files:
p="/Users/Ros/Google_Drive/Valparaiso/Light_curves/Data/Light_curves_tables/Optical/"
lc1=[p+"Iras045B_norm.qdp", p+"bin_LB1727_B.qdp", p+"bin_TONS180_B.qdp", p+"bin_IRAS16_B.qdp", p+"ESO354_lc_conv_ratiosB_source_all.qdp"]
lc2=[None, p+"bin_LB1727_I.qdp", p+"bin_TONS180_B.qdp", p+"bin_IRAS16_I.qdp", p+"ESO354_lc_conv_ratiosI_source_all.qdp"]
lc6=[p+"Iras045V_norm.qdp",None, None, None, None]

galaxies.loc[:,"B"]=lc1
galaxies.loc[:,"I"]=lc2
galaxies.loc[:,"V"]=lc6
#%%
# stars is just a place holder to know which galaxies/bands have light curves
stars=pd.DataFrame(index=path_name)
stars.loc[:,"Y"]=np.array([666,np.nan,np.nan,np.nan,np.nan])
stars.loc[:,"J"]=np.array([np.nan,6.69,8.20,666,15.73]) # mJy
stars.loc[:,"H"]=np.array([666,   666, 9.74,666,13.54])
stars.loc[:,"K"]=np.array([np.nan,3.59,666, 666,9.42])
# IRAS04505, K - only one source!

IR_bands=['Y','J','H','K']
for band in IR_bands:
   galaxies[band]=None
   for gal in path_name:
      if ~np.isnan(stars.loc[gal,band]):
         source_path=gal+'/'+band+'/'
         galaxies.loc[gal,band]='/Users/Ros/Google_Drive/Valparaiso/Light_curves/Data/My_light_curves_tables/'+source_path+'LC.txt'
      else:
         continue

#%%
with plt.style.context(['seaborn-white','seaborn-bright']):
   for obj in path_name:
      #print(obj)
      labels=[]
      dm = 4

      fig = plt.figure()
      ax1 = fig.add_subplot(111)
      #ax1.set_title('{0}'.format(object_name[i]), size=20)#,fontweight="bold"

      for k, column in enumerate(galaxies):
         #print(galaxies.loc[obj,column])

         if galaxies.loc[obj,column] is not None:
            labels.append(column)

            LC=Table.read(galaxies.loc[obj,column],format='ascii',names=('mjd', 'flux', 'err'))
            bad=is_outlier(LC['flux'], thresh=3)
            LC=LC[np.logical_not(bad)]

            norm_flux=np.array((LC['flux']-np.mean(LC['flux']))/np.std(LC['flux']))
  
            plt.errorbar(np.array(LC['mjd']), k*dm+norm_flux, yerr=LC['err'], fmt='o', ms=3)

            #plt.errorbar(mjd1,flux1,yerr=err1,fmt='none',elinewidth=1,ecolor='0.7')


      ax1.legend(labels)
            
      axes_size=12
      plt.xlabel('mjd-2450000.5', fontsize=axes_size)
      plt.ylabel('Flux (arbitrary units)', fontsize=axes_size)

      plt.savefig(output_file_folder+'light_curves_{0}.pdf'.format(obj), format = 'pdf', orientation = 'landscape', bbox_inches = 'tight',transparent=True) 
      plt.close(fig)
      fig


#%%
