#%%
"""
Funzione che simula curve di luce a partire da un power spectrum con forma di power law su cui si introduce del rumore.
(float) t_min, T_max, dt - starting time, end time and time interval
power law parameters: A normalization, alpha exponent (float)
seeds (int, 2el array) - seeds for the simulation of the Re and Im noise
flux_ave - average value for normalization of simulated curve
flux_err - error in % of flux_ave units.
"""
#!/usr/bin/python
#-*- coding: utf-8 -*-

import numpy as np
import scipy
from matplotlib import pyplot as plt


def curve_simul(T_max=5001.,t_min=1.,dt=1.,alpha=2.,A=1e-3,seeds=[123,124],flux_ave=0.3,flux_err=0.05):

########################################
### Parameter definition
########################################
# Fundamental vars
#T_max=1001. # (days) time covered by the simulated curve
#t_min= 1 # (days) initial time - can't be 0 or freq becomes infinite
#dt= 1. # (days) simulation time
#alpha= 2. # power law index
#A = 1e-3 # power law normalization
#seeds=[123,124]

# Derived vars
   times= np.arange(t_min,T_max,dt)
   N= int((T_max-t_min)/dt)
   freq=times[:N/2]/T_max

########################################
### Power spectrum calculation
########################################
   np.random.seed(seeds[0])
   Re=np.random.normal(size=N/2)
   np.random.seed(seeds[1])
   Im=np.random.normal(size=N/2)

   PS=A*freq**-alpha

   Sim_PS_pos=(Re+1j*Im)*(PS/2)**0.5
   Sim_PS_neg=np.flip((Re-1j*Im)*(PS/2)**0.5, 0)

   # plt.figure()
   # plt.minorticks_on()
   # plt.plot(freq,PS)
   # plt.plot(freq,Sim_PS_pos.real)
   # plt.show;

########################################
### Inverse FFT
########################################
   Sim_PS=np.append([0+0j],Sim_PS_pos)
   Sim_PS=np.append(Sim_PS,Sim_PS_neg)
   sim_curve=np.fft.ifft(Sim_PS)

########################################
### Plot simulated curve
########################################
   times=np.append(times,times[-1]+dt)
   # plt.figure()
   # plt.minorticks_on()
   # plt.plot(times,sim_curve.real)

   # plt.show;

########################################
### Add noise
########################################
# simulare errore poissoniano
# sommo valore medio flusso errore alla curva simulata
# e in ogni punto + gauss sim * tamano barra de error
# flux_ave=0.3
# flux_err=0.05
   sim_curve_noise = (sim_curve + flux_ave)*(1 + np.random.normal(size=N+1)*flux_err)


########################################
### Plot simulated curve w/noise
########################################
   # plt.figure()
   # plt.minorticks_on()
   # plt.plot(times,sim_curve_noise.real)

   # plt.show;

   return (times,sim_curve.real,sim_curve_noise.real)

#%%
