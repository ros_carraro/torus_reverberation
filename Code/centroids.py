
#%%
#!/usr/bin/python
#-*- coding: utf-8 -*-

import sys
import argparse
import numpy as np
from matplotlib import pyplot as plt
import scipy
import PYCCF as myccf
from scipy import stats 
from utilities import is_outlier

""" Function that uses PYCCF.
It requires in input the two light curves. 
It will output three data files containing the
CCF, CCCD, and CCPD, and one plot showing the light curves, CCF,
CCCD, and CCPD, all into the current directory.
"""


#%%
def centroids(object_name,lc1,lc2,err1=None,err2=None,output_file_folder='./',nsim=500,bands=['B','H']):
   ########################################
   ###Read in two light curves
   ########################################
   # if object_name == "ESO354-G04":
   #    err2=0.001

   if err1 is not None:
      mjd1, flux1  =  np.loadtxt(lc1, unpack = True, usecols = [0, 1])
      err1 = err1*flux1
   else:
      mjd1, flux1, err1  =  np.loadtxt(lc1, unpack = True, usecols = [0, 1, 2])

   if err2 is not None:
      if lc2.endswith('qdp'):
         mjd2, flux2, err2 =  np.loadtxt(lc2, unpack = True, usecols = [0, 1])
      else:
         mjd2, flux2, err2  =  np.loadtxt(lc2, unpack = True, usecols = [2, 3])
         sort_idx = mjd2.argsort()
         mjd2=mjd2[sort_idx]-2450000
         flux2=1/flux2[sort_idx]
         err2=err2[sort_idx]

   else:
      if lc2.endswith('qdp'):
         mjd2, flux2, err2 =  np.loadtxt(lc2, unpack = True, usecols = [0, 1, 2])
      else:
         mjd2, flux2, err2  =  np.loadtxt(lc2, unpack = True, usecols = [2, 3, 4])
         sort_idx = mjd2.argsort()
         mjd2=mjd2[sort_idx]-2450000
         err2=err2[sort_idx]
         if object_name != "IRAS16355-2049":
            flux2=1/flux2[sort_idx]

   #%%
   #########################################
   ## Remove outliers
   #########################################
   bad1=is_outlier(flux1, thresh=2)
   mjd1=mjd1[np.logical_not(bad1)]
   flux1=flux1[np.logical_not(bad1)]
   err1=err1[np.logical_not(bad1)]

   bad2=is_outlier(flux2, thresh=2)
   mjd2=mjd2[np.logical_not(bad2)]
   flux2=flux2[np.logical_not(bad2)]
   err2=err2[np.logical_not(bad2)]

   #%%
   #########################################
   ##Estimate best interpolation time step from observations
   #########################################
   start_day=int(max(min(mjd2),min(mjd1)))
   end_day=int(min(max(mjd2),max(mjd1)))
   span=end_day-start_day

   # diff_1=np.diff(mjd1)
   # diff_2=np.diff(mjd2)

   # bins = 10**(0.15*np.arange(-25,18))
   # plt.figure()
   # plt.xscale('log')
   # plt.minorticks_on()
   # plt.hist(diff_2,bins=bins)

   # plt.hist(diff_1,bins=bins, fill=False)
   # plt.show

   #mjd1[diff_1 < 0.1]+mjd1[np.roll((diff_1 < 0.1),1)]
   #np.size(mjd1[diff_1 < 0.1])


   #%%
   #########################################
   ##Set Interpolation settings, user-specified
   #########################################
   fraction=0.4
   lag_range = [-(span*fraction), span*fraction]  #Time lag range to consider in the CCF (days). Must be small enough that there is some overlap between light curves at that shift (i.e., if the light curves span 80 days, these values must be less than 80 days)
   interp = 2. #Interpolation time step (days). Must be less than the average cadence of the observations, but too small will introduce noise.
   #
   # 
   
   sim = 500  #Number of Monte Carlo iterations for calculation of uncertainties
   mcmode = 0  #Do both FR/RSS sampling (1 = RSS only, 2 = FR only) 
   sigmode = 0.2  #Choose the threshold for considering a measurement "significant". sigmode = 0.2 will consider all CCFs with r_max <= 0.2 as "failed". See code for different sigmodes.


   #%%
   ##########################################
   #Calculate lag with python CCF program
   ##########################################
   tlag_peak, status_peak, tlag_centroid, status_centroid, ccf_pack, max_rval, status_rval, pval = myccf.peakcent(mjd1, flux1, mjd2, flux2, lag_range[0], lag_range[1], interp)
   tlags_peak, tlags_centroid, nsuccess_peak, nfail_peak, nsuccess_centroid, nfail_centroid, max_rvals, nfail_rvals, pvals = myccf.xcor_mc(mjd1, flux1, abs(err1), mjd2, flux2, abs(err2), lag_range[0], lag_range[1], interp, nsim = nsim, mcmode=mcmode, sigmode = sigmode)

   lag = ccf_pack[1]
   r = ccf_pack[0]

   perclim = 84.1344746    

   ###Calculate the best peak and centroid and their uncertainties using the median of the
   ##distributions. 
   centau = stats.scoreatpercentile(tlags_centroid, 50)
   centau_uperr = (stats.scoreatpercentile(tlags_centroid, perclim))-centau
   centau_loerr = centau-(stats.scoreatpercentile(tlags_centroid, (100.-perclim)))
   print 'Centroid, error: %10.3f  (+%10.3f -%10.3f)'%(centau, centau_loerr, centau_uperr)

   peaktau = stats.scoreatpercentile(tlags_peak, 50)
   peaktau_uperr = (stats.scoreatpercentile(tlags_peak, perclim))-centau
   peaktau_loerr = centau-(stats.scoreatpercentile(tlags_peak, (100.-perclim)))
   print 'Peak, errors: %10.3f  (+%10.3f -%10.3f)'%(peaktau, peaktau_uperr, peaktau_loerr)


   #%%
   ##########################################
   #Write results out to a file in case we want them later.
   ##########################################
   # output_file_folder = './'
   # centfile = open(output_file_folder+'sample_centtab_yap.dat', 'w')
   # peakfile = open(output_file_folder+'sample_peaktab_yap.dat', 'w')
   # ccf_file = open(output_file_folder+'sample_ccf_yap.dat', 'w')
   # for m in xrange(0, np.size(tlags_centroid)):
   #    centfile.write('%5.5f    \n'%(tlags_centroid[m]))
   # centfile.close()
   # for m in xrange(0, np.size(tlags_peak)):
   #    peakfile.write('%5.5f    \n'%(tlags_peak[m]))
   # peakfile.close()
   # for m in xrange(0, np.size(lag)):
   #    ccf_file.write('%5.5f    %5.5f  \n'%(lag[m], r[m]))
   # ccf_file.close() 


   #%%
   ##########################################
   #Plot the Light curves, CCF, CCCD, and CCPD
   ##########################################
   color='k'# w for display, k for file saving purposes
   fontsize=15#originally 15 or 16

   fig = plt.figure(figsize=(13, 12))
   fig.subplots_adjust(hspace=0.2, wspace = 0.1)
   fig.suptitle('{0}'.format(object_name), fontsize=fontsize)



   #Plot lightcurves
   ax1 = fig.add_subplot(3, 1, 1)
   ax1.errorbar(mjd1, flux1, yerr = err1, marker = '.', linestyle = ':', color = color, label = 'LC 1 (Continuum)')
   ax1_2 = fig.add_subplot(3, 1, 2, sharex = ax1)
   ax1_2.errorbar(mjd2, flux2, yerr = err2, marker = '.', linestyle = ':', color = color, label = 'LC 2 (Emission Line)')

   ax1.text(0.025, 0.825, lc1, fontsize = fontsize, transform = ax1.transAxes)
   ax1_2.text(0.025, 0.825, lc2, fontsize = fontsize, transform = ax1_2.transAxes)
   ax1.set_ylabel('LC 1 Flux')
   ax1_2.set_ylabel('LC 2 Flux')
   ax1_2.set_xlabel('MJD')
   ax1.minorticks_on()
   ax1_2.minorticks_on()

   #Plot CCF Information
   xmin, xmax = lag_range[0],lag_range[1]
   ax2 = fig.add_subplot(3, 3, 7)
   ax2.set_ylabel('CCF r')
   ax2.text(0.2, 0.85, 'CCF ', horizontalalignment = 'center', verticalalignment = 'center', transform = ax2.transAxes, fontsize = fontsize)
   ax2.set_xlim(xmin, xmax)
   ax2.minorticks_on()
   ax2.set_ylim(-1.0, 1.0)
   ax2.plot(lag, r, color = color)

   #xmin, xmax = -10, 60
   #ax3 = fig.add_subplot(3, 3, 8)
   ax3 = fig.add_subplot(3, 3, 8, sharex = ax2)
   ax3.set_xlim(xmin, xmax)
   ax3.minorticks_on()
   ax3.axes.get_yaxis().set_ticks([])
   ax3.set_xlabel('Centroid Lag: %5.1f (+%5.1f -%5.1f) days'%(centau, centau_uperr, centau_loerr), fontsize = fontsize) 
   ax3.text(0.2, 0.85, 'CCCD ', horizontalalignment = 'center', verticalalignment = 'center', transform = ax3.transAxes, fontsize = fontsize)
   n, bins, etc = ax3.hist(tlags_centroid, bins = 50, color = 'b')

   #ax4 = fig.add_subplot(3, 3, 9)
   ax4 = fig.add_subplot(3, 3, 9, sharex = ax2)
   ax4.set_ylabel('N')
   ax4.yaxis.tick_right()
   ax4.yaxis.set_label_position('right') 
   #ax4.set_xlabel('Lag (days)')
   ax4.set_xlim(xmin, xmax)
   ax4.minorticks_on()
   ax4.text(0.2, 0.85, 'CCPD ', horizontalalignment = 'center', verticalalignment = 'center', transform = ax4.transAxes, fontsize = fontsize)
   ax4.hist(tlags_peak, bins = bins, color = 'b')

   plt.savefig(output_file_folder+'centroid_{0}_{1}{2}.pdf'.format(object_name,bands[0],bands[1]), format = 'pdf', orientation = 'landscape', bbox_inches = 'tight',transparent=True) 
   plt.close(fig)
   fig

   return centau, centau_loerr, centau_uperr