#%%
#-*- coding: utf-8 -*-
#!/usr/bin/python
"""
Macro for Carolina.
"""
import os
import curve_simul as c_sim
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import pyfftw
import multiprocessing
import matplotlib.ticker as mticker
from matplotlib.ticker import MultipleLocator

# to reimport the functions when modified:
#import sy
from importlib import reload
import curve_simul as c_sim
reload(c_sim)

from sklearn.preprocessing import StandardScaler

#%%
def progressBar(iterable, prefix = '', suffix = '', decimals = 1, length = 50, fill = '█', printEnd = "\r",percentage=False):
    """
    Call in a loop to create terminal progress bar
    @params:
        #iteration   - Required  : current iteration (Int)
        iterable    - Required  : iterable variable on which to loop on (Array-like)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    total = len(iterable)
    # Progress Bar Printing Function
    def printProgressBar (iteration):
        if percentage:
           percent = ("{0:." + str(decimals) + "f}%").format(100 * (iteration / float(total)))
        else:
           # print fraction
           percent = (f"{iteration}/{total}")
        filledLength = int(length * iteration // total)
        bar = fill * filledLength + '-' * (length - filledLength)
        print(f'\r{prefix} |{bar}| {percent} {suffix}', end = printEnd)
    # Initial Call
    printProgressBar(0)
    # Update Progress Bar
    for i, item in enumerate(iterable):
        yield item
        printProgressBar(i + 1)
    # Print New Line on Complete
    print()
# %%
def divide_in_bins(lc_all):
   try:
      lc_all['T(days)'].diff()
   except:
      Diff=pd.Series(lc_all.index).diff()
      Diff[0]=0
      Diff1=Diff > 10
      splits = np.split(lc_all.index.values,np.nonzero(Diff1.values)[0])
      durations=[i[-1]-i[0] if len(i)>1 else 1 for i in splits]
      n_length=np.max(taus)/np.array(durations)+1
      starts=[s[0] for s in splits]
      return splits,durations,starts

   else:   
      Diff=lc_all['T(days)'].diff()
      Diff[0]=0
      Diff1=Diff >= 100
      splits = np.split(lc_all['T(days)'].values,np.nonzero(Diff1.values)[0])
      durations=[i[-1]-i[0] if len(i)>1 else 1 for i in splits]
      n_length=np.max(taus)/np.array(durations)+1
      starts=np.concatenate(([0],lc_all.loc[Diff1, 'T(days)'].values))
      return n_length,durations,starts
# %%
def simulate(lc_all,seed,tau,duration,freq_nu,alpha_h,variances, i, n_length,ifft,ifft_array):
   if dt == 1e-4:
      #n_length, durations, starts = divide_in_bins(lc_all)
      
      times=np.array([])
      sim_curve=np.array([])
      sim_curve_sm=np.array([])
      for n, (start, duration,ifft,ifft_array,n_length) in enumerate(zip(starts,durations,ifft,ifft_array,n_lengths)):
         #curva di luce sim i - molto più lunga, 2 Delta T dati.
         curve_dur=np.ceil(n_length*duration)
         times_tmp, sim_curve_tmp, sim_curve_sm_tmp = c_sim.curve_simul(tau,ifft,ifft_array,T_max=curve_dur+1.,t_min=1.,dt=dt,freq_nu=freq_nu,alpha_h=alpha_h,A=1,seeds=seed[n,:])

         # shifting the simulated LC to the center of the piece of data
         N=np.size(times_tmp)
         sim_start=duration/2-times_tmp[int(N/2)]+start
         sim_start=np.trunc(sim_start/dt)
         times_tmp=(np.trunc(times_tmp/dt)+sim_start).astype(int)

         times=np.concatenate([times,times_tmp])
         sim_curve=np.concatenate([sim_curve,sim_curve_tmp])
         sim_curve_sm=np.concatenate([sim_curve_sm,sim_curve_sm_tmp])
      
      mjd1_shifted=(lc['T(days)'].to_numpy()/dt).astype(int)
      lc_all.set_index(mjd1_shifted,inplace=True)
   else:
      #curva di luce sim i - molto più lunga, 2 Delta T dati.
      curve_dur=np.ceil(n_length*duration)
      times, sim_curve, sim_curve_sm = c_sim.curve_simul(tau,ifft,ifft_array,T_max=curve_dur+1.,t_min=1.,dt=dt,freq_nu=freq_nu,alpha_h=alpha_h,A=1,seeds=seed)
   

      # shifting data to the center of the simulated LC
      #times=np.around(times,decimals=decimals)
      N=np.size(times)
      sim_start=times[int(N/2)]-duration/2
      data_shift=sim_start-int(lc['T(days)'].min())
      times=(np.trunc(times/dt)).astype(int)
      mjd1_shifted=(lc['T(days)'].to_numpy()/dt).astype(int)+(data_shift/dt).astype(int)
      lc_all.set_index(mjd1_shifted,inplace=True)
   
   """
   if i==0:
      plt.figure(figsize=[10,4])
      plt.minorticks_on()
      plt.plot(times,sim_curve, linewidth=0.2)
      plt.plot(times,sim_curve_sm)
      plt.grid(which='both',axis='x')
      plt.xlim(0, 4400)
      plt.savefig(output+f'/sim_LC_tau{tau}_{i}_test.pdf', 
                  format = 'pdf', bbox_inches = 'tight',transparent=True)
      plt.close()
   """
   
   #if dt == 1e-4:
   tau2= np.trunc(tau/2/dt).astype(int)
   #else:
   #   tau2= np.around(tau/2,decimals=decimals)

   lc_sim=pd.DataFrame({'sim_curve':sim_curve},index=times)
   lc_sim.index.name = 'times'
   lc_sim_sm=pd.DataFrame({'smooth_curve':sim_curve_sm},index=times+tau2)
   lc_sim_sm.index.name = 'times'

   # sample orginal simulated light curve
   times_sampled = np.sort(np.array(list(set(times).intersection(mjd1_shifted))))
   lc_sim_sampled=lc_sim.loc[times_sampled,:].copy()
   #[print(num, True) if num in lc_all.index else print(num, False) for num in lc_sim_sampled.index]
   lc_all=pd.merge(lc_all,lc_sim_sampled,left_index=True,right_index=True)
   
   # sample smoothed and delayed light curve
   lc_smooth_sampled=lc_sim_sm.loc[times_sampled,:]
   lc_all_tmp=pd.merge(lc_all,lc_smooth_sampled,left_index=True,right_index=True)
   if len(lc_all_tmp)!= len(lc):
      print(f'Warning! {len(lc_all_tmp)} points sampled, while light curve has {len(lc)} points.')

   #if dt == 1e-4:
   #reconvert time indexes to real units
   lc_all_tmp.set_index(lc_all_tmp.index.to_numpy()*dt, inplace=True)
   lc_sim.set_index(lc_sim.index*dt, inplace=True)
   lc_sim_sm.set_index(lc_sim_sm.index*dt, inplace=True)

   # add noise to simulated lc
   #print(lc_all_tmp[['sim_curve','smooth_curve']])
   norm_fac=(lc_all_tmp.obs_curve.var()/lc_all_tmp.sim_curve.var())**0.5
   ct_noise=np.random.normal(scale=lc_all['cont_err']) # scale=sigma
   lc_all_tmp['sim_curve_noise']=lc_all_tmp['sim_curve']*norm_fac+ct_noise
   li_noise=np.random.normal(scale=lc_all['line_err'])
   lc_all_tmp['smooth_curve_noise']=lc_all_tmp['smooth_curve']*norm_fac+li_noise
   #print(lc_all_tmp[['sim_curve','smooth_curve']])
   #print(ct_noise,li_noise)

   #void addnoise(double *flux,double *err,double *flux_more_noise, int points, long idum){
   #for (int i=0;i <points; i++){
   #   flux_more_noise[i] = flux[i]+gasdev(&idum)*err[i];
   #}

   #if not os.path.exists(output+'/test_nub'):
   #   print('Making output directory')
   #   os.makedirs(output+'/test_nub')
      
   # plot simulated and smoothed light curves
   plt.figure(figsize=[10,4])
   plt.minorticks_on()
   plt.plot(lc_sim.index,lc_sim.sim_curve, linewidth=0.2)
   plt.plot(lc_sim_sm.index,lc_sim_sm.smooth_curve, linewidth=1)
   plt.grid(which='both',axis='x')
   #plt.show;
   plt.savefig(output+f'/sim_LC_tau{tau}_{i}.pdf', 
               format = 'pdf', bbox_inches = 'tight',transparent=True)
   plt.close()

   # plot sampled curve and data
   #lc_all_plot=lc_all_tmp[['obs_curve','obs_line','sim_curve','smooth_curve']].copy()
   lc_all_plot=lc_all_tmp[['obs_curve','obs_line','sim_curve','sim_curve_noise','smooth_curve','smooth_curve_noise']].copy()
   scaler=StandardScaler()
   lc_all_plot[['obs_curve','obs_line','sim_curve','sim_curve_noise','smooth_curve','smooth_curve_noise']]=scaler.fit_transform(lc_all_plot.to_numpy())
   ax=lc_all_plot.plot(marker="o", ms=1,grid=True, lw=0.5)#linestyle="none", 
   ax.xaxis.set_minor_locator(ticker.AutoMinorLocator())
   ax.yaxis.set_minor_locator(ticker.AutoMinorLocator())
   #ax.yaxis.set_minor_locator(ticker.MultipleLocator(0.005))
   ax.set_title(f'Sampled light curves')
   #plt.show;
   plt.savefig(output+f'/sim_LC_tau{tau}_{i}_sampled.pdf', 
               format = 'pdf', bbox_inches = 'tight',transparent=True)
   plt.close()
   lc_all_tmp[['sim_curve','sim_curve_noise','smooth_curve','smooth_curve_noise']].to_csv(output+f'/sim_LC_tau{tau}_{i}.csv')

   # plot ALL sim curves and sampled ones
   lc_all_plot=lc_all_tmp[['sim_curve','sim_curve_noise','smooth_curve','smooth_curve_noise']].copy()
   lc_all_plot[['sim_curve','smooth_curve']]=lc_all_plot[['sim_curve','smooth_curve']]*norm_fac
   splits,durations,_=divide_in_bins(lc_all_plot)
   params = {'legend.fontsize': 'small',
            'legend.title_fontsize':'medium'}
   plt.rcParams.update(params)
   fig3 = plt.figure(constrained_layout=False,figsize=[10,6])#
   heights = [0.7, 0.3]
   gs = fig3.add_gridspec(nrows=2, ncols=len(splits), width_ratios=np.array(durations)+20,
                           height_ratios=heights)
   f3_ax1 = fig3.add_subplot(gs[0, :])
   f3_ax1.set_ylabel('flux (arbitrary units)')
   f3_ax1.xaxis.set_minor_locator(MultipleLocator(500))
   f3_ax1.plot(lc_sim.index,lc_sim.sim_curve*norm_fac, linewidth=0.2)
   f3_ax1.plot(lc_sim_sm.index,lc_sim_sm.smooth_curve*norm_fac, linewidth=0.2)
   lc_all_plot.plot(ax=f3_ax1, style=["o","s","P","X"],grid=True,ms=3, linestyle="none",lw=0.5,markeredgecolor='black',markeredgewidth=0.3)
   leg=f3_ax1.legend(loc='upper left',title=f'{gal}, $\\tau=\,${tau}',labels=['Simulated','Smoothed','Sampled simulated','Sampled smoothed','Sampled simulated + noise','Sampled smoothed + noise'])
   leg._legend_box.align= "left"
   for n,s in enumerate(splits):
      f3_ax2 = fig3.add_subplot(gs[1, n])
      lc_range=np.around([s[0]-5.0,s[-1]+5.0], decimals=1)
      
      lc_tmp=lc_sim.loc[lc_range[0]:lc_range[-1],:]
      f3_ax2.plot(lc_tmp.index,lc_tmp.sim_curve*norm_fac, linewidth=0.5)
      lc_tmp=lc_sim_sm.loc[lc_range[0]:lc_range[-1],:]
      f3_ax2.plot(lc_tmp.index,lc_tmp.smooth_curve*norm_fac, linewidth=0.5)
      lc_all_plot.loc[s,:].plot(ax=f3_ax2, style=["o","s","P","X"],ms=2, linestyle="none",lw=0.5,markeredgecolor='black',markeredgewidth=0.1,legend=False)

      f3_ax2.set_xlim(lc_range)
      f3_ax2.set_yticklabels([])
      f3_ax2.xaxis.set_major_locator(MultipleLocator(10))
      f3_ax2.xaxis.set_minor_locator(MultipleLocator(5))
      f3_ax2.ticklabel_format(axis='x',useOffset=False)
      f3_ax2.xaxis.set_major_formatter('{x:.0f}')
      f3_ax2.tick_params(axis='y',direction='in',length=0)  
      f3_ax2.tick_params(axis='x', which='both', labelrotation = 45,direction='in',labelsize='xx-small')  
      f3_ax2.set_xlabel(' ')
   fig3.subplots_adjust(wspace=0)
   fig3.text(0.5, 0.04, 'time (days)', va='center', ha='center')
   plt.savefig(output+f'/sim_LC_tau{tau}_{i}_matrix.pdf', 
               format = 'pdf', bbox_inches = 'tight',transparent=True)
   plt.close()
   
   # variances of lightcurves
   var_noise=lc_all_tmp[['sim_curve_noise','smooth_curve_noise']].apply(lambda x: np.mean(x**2)).values-lc_all_tmp[['cont_err','line_err']].apply(lambda x: np.mean(x**2)).values
   #variances[tau]=variances[tau].append(lc_all_tmp[['sim_curve','smooth_curve']].var().transpose(),ignore_index=True)
   variances[tau].loc[i,['sim_curve','smooth_curve']]=lc_all_tmp[['sim_curve','smooth_curve']].var(ddof=0)
   variances[tau].loc[i,['sim_curve_noise','smooth_curve_noise']]=pd.Series(var_noise,index=['sim_curve_noise','smooth_curve_noise'])
   return
# %%
def est_X(taus):
   #if (X.max() > 1) & (len(taus) >1):
      # in this case no need to explore taus
   #   return

   for tau in taus:

      if isinstance(tau, np.ndarray):
         tau=tau[0]
      print(F'tau={tau}')

      # if X[tau] was already estimated, skip:
      if tau in X.index:
         if not np.isnan(X[tau]):
            print(f'X[{tau}]={X[tau]} already estimated. Skipping.')
            continue

      if tau not in variances.keys():
         variances[tau]=pd.DataFrame(columns=['sim_curve','sim_curve_noise','smooth_curve','smooth_curve_noise'])
      #for i in range(nsim):
      for i in progressBar(range(nsim), prefix = 'Iteration number:', length = 30):
         #print('-----',i,'-----')
         if dt > 1e-4:
            simulate(lc_all,seeds[i,:],tau,duration,freq_nu,alpha_h,variances, i,n_length,ifft,ifft_array)
         else:
            simulate(lc_all,seeds[i,:,:],tau,duration,freq_nu,alpha_h,variances, i,n_length,iffts,ifft_arrays)

      # histogram of variances
      ax = variances[tau].plot.hist(bins=20, alpha=0.5)
      ax.set_title(fr'Variances for $\tau$={tau}')
      ax.ticklabel_format(axis='x',style='sci',scilimits=[-10,-2])
      plt.savefig(output+f'/var_distr_tau{tau}.pdf', 
                  format = 'pdf', bbox_inches = 'tight',transparent=True)
      plt.close()
      
      #ratios[tau]=variances[tau].smooth_curve/variances[tau].sim_curve
      ratios[tau]=variances[tau].smooth_curve_noise/variances[tau].sim_curve_noise
      variances[tau]['ratio']=variances[tau].smooth_curve/variances[tau].sim_curve
      variances[tau]['ratio_noise']=variances[tau].smooth_curve_noise/variances[tau].sim_curve_noise
      variances[tau].to_csv(output+f'/variances_tau{tau}.csv')
      # extimate relative variance ratio difference
      X[tau]=(ratio_real-ratios[tau].mean())/np.sqrt(np.mean(ratios[tau]**2)-np.mean(ratios[tau])**2)
      print('X:',X[tau])
      X.to_csv(output+'/X_tau_'+gal+'.csv')
      
      if (X[tau] > 1) and (len(taus)>1):
         print(f'You reached X(tau) > 1 - X({tau}) = {X[tau]}')
         return

   if not ratios.empty:
      # histogram of ratios
      ax = ratios.plot.hist(bins=20, alpha=0.3)
      #print(f"{lc['flux_Fe(ph/cm^2/s)'].var()}/{lc['flux_cont(ph/cm^2/s)'].var()}")
      ax.axvline(ratio_real,label=f"real ratio")
      ax.legend()
      ax.set_title(fr'Ratio of variances for all $\tau$ values')
      plt.savefig(output+f'/var_ratio.pdf', 
                  format = 'pdf', bbox_inches = 'tight',transparent=True)
      plt.close()

   # estimate relative variance ratio difference
   #X=(ratio_real-ratios.mean())/ratios.apply(lambda x: np.sqrt(np.mean(x**2)))
   #print('tau and X:')
   #print(X)
   return   
# %%
def bisection(tol,extr,value=0):
   a=extr[0]
   b=extr[1]
   Xa=X[a]-value
   Xb=X[b]-value
   if Xa*Xb > 0:
      print('Something is wrong')
      return
   elif (np.min(np.abs([Xa,Xb])) < tol) or (np.abs(a-b) <= 1):
      print('You already found the root')
      return
   else:
      print('Everything is fine')
      k=0

   while (np.abs(b-a) > 1) and (k < 20):
      k+=1
      # classic bisection
      #new_tau=np.trunc((a+b)/2).tolist()
      # tangent - newton method
      new_tau=np.trunc(a-(b-a)/(X[b]-X[a])*(X[a]-value)).tolist()
      print(new_tau)
      if (new_tau == a) or (new_tau == b):
         print(f'The root is tau={new_tau}')
         break
      est_X([new_tau])
      X_new=X[new_tau]-value
      if abs(X_new) < tol:
         print(f'The root is tau={new_tau}')
         break
      elif X_new*(X[a]-value) < 0:
         b=new_tau
      else:
         a=new_tau
   print(f'Number of iterations to converge: {k}')
   return
   # %%
def find_X(X,tol=0.005,root=0):
   # find where X(tau_i)*X(tau_i+1)<0
   X.sort_index(inplace=True)
   X_val=X.values-root
   abs_sign_diff = np.abs(np.diff(np.sign(X_val)))
   change_idx = np.flatnonzero(abs_sign_diff == 2)
   change_idx = np.append(change_idx, change_idx + 1)
   print(X.iloc[change_idx])

   extr=X.iloc[change_idx].index.values
   bisection(tol,extr,root)
#%%
#path = '/users/Ros/Google_drive/Valparaiso/Light_curves/Code_Py3/Carolina/'
path = '/share/cav/data/rcarraro/Carolina/'
taus= [1.,10.,50.,100.,200.,300.,400.,600.,1000.] # days
#taus= [3000.,5000.,10000.] # days
#taus= [100.] # days
#print('taus:',taus)
# galaxies that have X(tau)=0
best_tau={'MRK766':[2.],'MRK1040':[18.],'NGC1365':[1265.],'NGC2992':[4031.0],'NGC3516':[22.],'NGC3783':[26.],'NGC4151':[1117.],'NGC4388':[856.],'NGC5548':[1745.],'NGC7469':[10.],'NGC7582':[1245.],'PictorA':[5.],'2MASXJ11315154-1231587':[2812.0]}
# python sim_and_var.py > filedi.log 2>&1 &
nsim=50

# power spectrum parameters
ps_pars=pd.read_csv(path+'LightCurves/info_BHmass_nuB_days.csv',index_col=0,header=0)
#ps_pars.drop(ps_pars.loc[:'NGC3227'].index,inplace=True)
#ps_pars.drop(ps_pars.loc[:'NGC6240'].index,inplace=True)
ps_pars.drop(['NGC3227','NGC6240'],inplace=True)
#
#ps_pars.drop(['1H0707-495'],inplace=True)
#ps_pars.drop(ps_pars.loc['AXJ1737.4-2907':].index,inplace=True)
#
#ps_pars.drop(ps_pars.loc[:'AXJ1737.4-2907'].index,inplace=True)
#ps_pars.drop(ps_pars.loc[:'CenA'].index,inplace=True)
#ps_pars.drop(ps_pars.loc['M81':].index,inplace=True)
#
#ps_pars.drop(ps_pars.loc[:'M81'].index,inplace=True)
#ps_pars.drop(ps_pars.loc['MRK1040':].index,inplace=True)
#
#ps_pars.drop(ps_pars.loc[:'MRK1040'].index,inplace=True)
#ps_pars.drop(ps_pars.loc[:'MR2251-178'].index,inplace=True)
#ps_pars.drop(ps_pars.loc['NGC1068':].index,inplace=True)
#
#ps_pars.drop(ps_pars.loc[:'MRK766'].index,inplace=True)
#ps_pars.drop(ps_pars.loc['NGC4278':].index,inplace=True)
#
#ps_pars.drop(ps_pars.loc[:'NGC4051'].index,inplace=True)
#ps_pars.drop(ps_pars.loc[:'NGC6300'].index,inplace=True)

# create wisdom dictionary or read it from file
wisdom_path=path+'wisdom.csv' # wisdom_iMac.csv
if os.path.exists(wisdom_path):
   wisdom_df=pd.read_csv(wisdom_path,index_col=0)
   wisdom_dict=wisdom_df.to_dict('list')
else:
   wisdom_dict={}
# other fftw parameters
print("CPU Count: ",multiprocessing.cpu_count())
pyfftw.config.NUM_THREADS = multiprocessing.cpu_count()
pyfftw.config.PLANNER_EFFORT = 'FFTW_PATIENT'

# ratio of excess variance for test:
#new_pars=pd.read_csv(path+'excess_variance_values.csv',header=0,index_col=1)
#new_pars['target_ratio_real']=new_pars['ratio_exc_fe_cont']-new_pars['error_ratio_exc_fe_cont']sep
new_pars=pd.read_csv(path+'ratio_percentiles_new.csv',index_col=0,usecols=[0,3],header=None)
new_pars.rename(columns={3:'target_ratio_real'},inplace=True)
#%%
#for gal in ps_pars.index:
for gal in ['2MASXJ23444387-4243124']:#,'2MASXJ23444387-4243124','PKS2153-69','3C445'
   #if gal not in best_tau.keys():
   #   print(f"galaxy {gal} doesn't have a best tau")
   #   continue
   #if gal in best_tau.keys():
   #   print(f"galaxy {gal} has a best tau")
   #   continue

   print(gal)
   if np.isnan(ps_pars.loc[gal,'meas_vb_days']):
      print("Galaxy doesn't have power spectrum parameters")
      continue

   # determine simulation parameters
   freq_nu=ps_pars.loc[gal,'meas_vb_days']#*10
   alpha_h=ps_pars.loc[gal,'alpha_h']
   if np.isnan(alpha_h):
      alpha_h=2

   decimals=-int(np.floor(np.log10(abs(1/freq_nu/100))))
   dt=10**-decimals # days

   if dt > 0.1:
      dt=0.1
      decimals=1
   
   if dt >= 1e-3:
      taus= [1.,10.,50.,100.,200.,300.,400.,600.,1000.] # days
   if gal=='NGC1365' or gal == 'NGC2992' or gal == 'NGC3516' or gal == 'NGC4151' or gal == 'NGC4388' or gal == 'NGC5548' or gal =='NGC7582' or gal =='NGC3783' or gal =='NGC7469' or gal =='MRK1040' or gal =='IC4329A' or gal == '2MASXJ11315154-1231587' or gal == 'NGC1275' or gal == 'PictorA' or gal=='CygnusA' or gal=='MRK273' or gal=='MRK290' or gal=='MRK509' or gal=='MRK1040' or gal=='NGC3393' or gal=='M81':
      taus.append(5000.)
      taus.append(9999.)
      taus.append(10000.)

   #if dt >= 0.001:
   #   print(f'Wrong dt (dt={dt}), skipping')
   #   continue

   #read light_curve
   lc=pd.read_csv(path+'LightCurves/lightcurves/'+gal+'_lc.csv',header=0,usecols=['T(days)','flux_cont(ph/cm^2/s)','flux_cont_err(ph/cm^2/s)','flux_Fe(ph/cm^2/s)','flux_Fe_err(ph/cm^2/s)'],na_values='ul')
   #lc['T(days)']=lc['T(days)'].round(decimals=1)
   lc['T(days)']=lc['T(days)'].round(decimals=decimals)

   average_cont = lc['flux_cont(ph/cm^2/s)'].mean()
   lc['flux_cont(ph/cm^2/s)']=(lc['flux_cont(ph/cm^2/s)']-average_cont)/average_cont
   lc['flux_cont_err(ph/cm^2/s)']=lc['flux_cont_err(ph/cm^2/s)']/average_cont

   average_line = lc['flux_Fe(ph/cm^2/s)'].mean()
   lc['flux_Fe(ph/cm^2/s)']=(lc['flux_Fe(ph/cm^2/s)']-average_line)/average_line
   lc['flux_Fe_err(ph/cm^2/s)']=lc['flux_Fe_err(ph/cm^2/s)']/average_line

   duration = lc['T(days)'].max().astype(int) # days

   print(f'duration={duration}, dt={dt}, alpha_h={alpha_h:.2f}, nu_b={freq_nu:.2f}, decimals={decimals}')

   output=path+'output/'+gal#+'/test_ratio'
   if not os.path.exists(output):
      print('Making output directory')
      os.makedirs(output)
      
   lc_all=lc.copy()
   lc_all=lc_all.loc[:,['flux_cont(ph/cm^2/s)','flux_cont_err(ph/cm^2/s)','flux_Fe(ph/cm^2/s)','flux_Fe_err(ph/cm^2/s)']]
   lc_all.rename(columns={'flux_cont(ph/cm^2/s)':'obs_curve','flux_cont_err(ph/cm^2/s)':'cont_err','flux_Fe(ph/cm^2/s)':'obs_line','flux_Fe_err(ph/cm^2/s)':'line_err'},inplace=True)
   lc_all['obs_curve']=lc_all['obs_curve']*average_cont
   lc_all['obs_line']=lc_all['obs_line']*average_line

   # ratio of excess variances from real data
   excess_var=lc[['flux_cont(ph/cm^2/s)','flux_Fe(ph/cm^2/s)']].apply(lambda x: np.mean(x**2)).values - lc[['flux_cont_err(ph/cm^2/s)','flux_Fe_err(ph/cm^2/s)']].var().values
   ratio_real=excess_var[1]/excess_var[0]
   print(fr'Ct excess variance:  $\sigma^2$ = {excess_var[0]}')

   # ratio of excess variance for test:
   if gal in new_pars.index:
      ratio_real=new_pars.loc[gal,'target_ratio_real']
      print(f'Test target ratio: {ratio_real}')
      if ratio_real<0:
         print('Target ratio < 0. Skipping')
         #continue
   else:
      print(f'Galaxy {gal} is missing in new parameters file. Skipping.')
      continue

   #if excess_var[1] <0:
   #   print(fr'Fe line excess variance $\sigma^2$ = {excess_var[1]} < 0.')
   #   print(f'Impossible to constrain variability of Fe line')
   #   continue
   #else:
   #   print(fr'Fe excess variance:  $\sigma^2$ = {excess_var[1]}')

   # needed for visualization
   lc_all['obs_curve']=lc_all['obs_curve']/average_cont
   lc_all['obs_line']=lc_all['obs_line']/average_line

   # minimum length:
   #n_length=np.max(taus)/duration+1
   if dt >= 1e-3:
      n_length=np.max([3,np.max(taus)/duration+1])
  # elif dt==1e-3:
   #   n_length=2
   else:
      #dt == 1e-4
      # always use 10 as the highest, in order to use wisdom and be fast
      taus=[1,5,10,19,20]
      n_length=np.max(taus)/duration+1

   if os.path.exists(output+'/X_tau_'+gal+'.csv'):
      X = pd.read_csv(output+'/X_tau_'+gal+'.csv',squeeze=True,index_col=0) 
   else:
      X=pd.Series(dtype='float64',index=taus)
   X = X[~X.index.duplicated(keep='first')] # remove double indices in case they are inserted

   # FFTW settings for galaxy
   if gal in wisdom_dict.keys():
      print('importing wisdom')
      wisd_tmp=wisdom_dict[gal]
      pyfftw.import_wisdom(tuple([bytearray.fromhex(el) for el in wisd_tmp]))
   print('creating ifft objects')
   if dt >= 1e-3:
      seeds=np.arange(nsim*2).reshape(nsim,2)
      #print('length ifft_array:',int(np.ceil(n_length*duration)/dt)+1)
      ifft_array = pyfftw.empty_aligned(int(np.ceil(n_length*duration)/dt)+1, dtype='complex128')
      ifft = pyfftw.builders.ifft(ifft_array, overwrite_input=True, avoid_copy=True)
   else:
      n_lengths, durations, starts = divide_in_bins(lc)

      seeds=np.arange(len(n_lengths)*nsim*2).reshape(nsim,len(n_lengths),2)
      
      ifft_arrays=[pyfftw.empty_aligned(int(np.ceil(n_length*duration)/dt)+1, dtype='complex128') for n_length,duration in zip(n_lengths,durations)]
      iffts=[pyfftw.builders.ifft(ifft_array, overwrite_input=True, avoid_copy=True) for ifft_array in ifft_arrays]
   print('done')

   # save wisdom
   if os.path.exists(wisdom_path):
      # re-read file in case it was overwritten by another process meanwhile
      wisdom_df=pd.read_csv(wisdom_path,index_col=0)
      wisdom_dict=wisdom_df.to_dict('list')

   wisd_tmp=pyfftw.export_wisdom()
   wisdom_dict[gal]=tuple([el.hex() for el in wisd_tmp])
   wisdom_df=pd.DataFrame(wisdom_dict)
   wisdom_df.to_csv(wisdom_path)
   
   #################
   ##### Run the simulations
   #################
   variances={i:pd.DataFrame(columns=['sim_curve','sim_curve_noise','smooth_curve','smooth_curve_noise']) for i in taus}
   ratios=pd.DataFrame()

   #est_X(best_tau[gal])
   est_X(taus)

   # guardar en file
   X.to_csv(output+'/X_tau_'+gal+'.csv')
   """
   def exponential(x, a, k, b):
      return a*np.exp(x*k) + b
   def parabola(x, a, b, c):
      return a*x**2+b*x+c
   # fitting
   #popt_exponential, pcov_exponential = curve_fit(exponential, X, X.index, p0=[1, 1, -1])
   popt_exponential, pcov_exponential = curve_fit(parabola, X, X.index, p0=[1, 1, 1])

   ynew=np.arange(X.min(),X.max(),0.1)
   #my_exp=exponential(ynew,*popt_exponential)
   my_exp=parabola(ynew,*popt_exponential)
   X_101=parabola(np.array([-1,0.,1.]),*popt_exponential)
   print(f'tau values for X=-1,0,1: {X_101}')

   #print(p([0,1]))

   ax=X.plot(style='.',label='X($\\tau$)')
   #ax.plot(my_exp,ynew,label='quadratic_fit')
   #ax.axvline(exponential(0,*popt_exponential),label=f"X=0",c='C2')
   #ax.axvline(exponential(1,*popt_exponential),label=f"X=1",c='C3')  
   ax.legend()
   #ax.set_xscale('log')
   #ax.set_yscale('log')
   #ax.set_xlim(-1,1000)
   ax.set_xlabel(r'$\tau$')
   ax.set_ylabel('X');
   """

   tol=0.005
   X.dropna(inplace=True)

   # look for X=0 with bisection
   if (X.iloc[0]<0) and (X.iloc[-1]>0):
      find_X(X,tol=tol,root=0)

   # look for X=1 with bisection
   X.sort_index(inplace=True)
   if (X.iloc[0]<1) and (X.iloc[-1]>1):
      find_X(X,tol=tol,root=1)

   # look for X=-1 with bisection
   X.sort_index(inplace=True)
   if (X.iloc[0]<-1) and (X.iloc[-1]>-1):
      find_X(X,tol=tol,root=-1)


   X.sort_index(inplace=True)

   ax=X.plot(style='.',label='X($\\tau$)')
   ax.set_title(f'{gal}. Curve duration:{duration} days, n points: {len(lc)}')
   ax.legend()
   ax.set_xlabel(r'$\tau$')
   ax.set_ylabel('X')
   ax.xaxis.set_minor_locator(ticker.AutoMinorLocator())
   ax.yaxis.set_minor_locator(ticker.AutoMinorLocator())
   plt.savefig(output+'/X_tau_'+gal+'.pdf', 
               format = 'pdf', bbox_inches = 'tight',transparent=True);
   plt.close()

   # guardar en file
   X.to_csv(output+'/X_tau_'+gal+'.csv')
   
   # FFTW save wisdom
   wisd_tmp=pyfftw.export_wisdom()
   wisdom_dict[gal]=tuple([el.hex() for el in wisd_tmp])
   pyfftw.forget_wisdom()
   # %%
   
wisdom_df=pd.DataFrame(wisdom_dict)
wisdom_df.to_csv(wisdom_path)

# %%
for gal in ps_pars.index:
   print(gal)
   if np.isnan(ps_pars.loc[gal,'meas_vb_days']):
      print("Galaxy doesn't have power spectrum parameters")
      continue
   lc=pd.read_csv(path+'LightCurves/lightcurves/'+gal+'_lc.csv',header=0,usecols=['T(days)','flux_cont(ph/cm^2/s)','flux_cont_err(ph/cm^2/s)','flux_Fe(ph/cm^2/s)','flux_Fe_err(ph/cm^2/s)'],na_values='ul')
   
   output=path+'output/X_tau/'#+gal
   
   """
   # plot lc
   ax=plt.subplot()
   ax.scatter(lc['T(days)'],lc['flux_cont(ph/cm^2/s)']/lc['flux_cont(ph/cm^2/s)'].mean(),s=3,label='continuum')
   ax.scatter(lc['T(days)'],lc['flux_Fe(ph/cm^2/s)']/lc['flux_Fe(ph/cm^2/s)'].mean(),s=3,label='Fe line')
   ax.legend()
   ax.set_xlabel('Time (days)')
   ax.set_ylabel('norm flux')
   ax.xaxis.set_minor_locator(ticker.AutoMinorLocator())
   ax.yaxis.set_minor_locator(ticker.AutoMinorLocator())
   plt.savefig(output+'/LC_data_'+gal+'.pdf', 
               format = 'pdf', bbox_inches = 'tight',transparent=True);
   plt.close()
   """

   if not os.path.exists(output+'/X_tau_'+gal+'.csv'):
      print('Light curve not simulated yet')
      continue

   # plot X(tau)
   X = pd.read_csv(output+'/X_tau_'+gal+'.csv',squeeze=True,index_col=0) 

   ax=X.plot(style='.',label='X($\\tau$)')
   ax.axhline(y=0,linewidth=1,color='black',zorder=0)
   ax.axhline(y=1,linestyle='--',linewidth=1,color='black',zorder=0)
   ax.axhline(y=-1,linestyle='--',linewidth=1,color='black',zorder=0)
   ax.set_title(f'{gal}')
   #ax.legend()
   ax.set_xlabel(r'$\tau$ (days)')
   ax.set_ylabel('X')
   ax.xaxis.set_minor_locator(ticker.AutoMinorLocator())
   ax.yaxis.set_minor_locator(ticker.AutoMinorLocator())
   ax.tick_params(axis="x", direction="in")
   ax.tick_params(axis="y", direction="in")
   ax.tick_params(bottom=True, top=True, left=True, right=True)
   ax.tick_params(labelbottom=True, labeltop=False, labelleft=True, labelright=False)
   plt.savefig(output+'/X_tau_'+gal+'.pdf', 
               format = 'pdf', bbox_inches = 'tight',transparent=True);
   plt.close()

# %%
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
for gal in ps_pars.index:
   print(gal)
   if np.isnan(ps_pars.loc[gal,'meas_vb_days']):
      print("Galaxy doesn't have power spectrum parameters")
      continue
   
   output=path+'output/X_tau/'#+gal
   
   if not os.path.exists(output+'/X_tau_'+gal+'.csv'):
      print('Light curve not simulated yet')
      continue

   # plot X(tau)
   X = pd.read_csv(output+'/X_tau_'+gal+'.csv',squeeze=True,index_col=0) 

   X.plot(ax=ax, style='.',label=f'{gal}')
   #ax.set_title(f'{gal}')
ax.legend(loc=(1.02,0), mode="expand", ncol=2)
ax.axhline(y=0,linewidth=1,color='black',zorder=0)
ax.axhline(y=1,linestyle='--',linewidth=1,color='black',zorder=0)
ax.axhline(y=-1,linestyle='--',linewidth=1,color='black',zorder=0)
ax.set_xlabel(r'$\tau$ (days)')
ax.set_ylabel('X')
ax.xaxis.set_minor_locator(ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(ticker.AutoMinorLocator())
ax.tick_params(axis="x", direction="in")
ax.tick_params(axis="y", direction="in")
ax.tick_params(bottom=True, top=True, left=True, right=True)
ax.tick_params(labelbottom=True, labeltop=False, labelleft=True, labelright=False)
plt.savefig(output+'/X_tau_1all.pdf', 
            format = 'pdf', bbox_inches = 'tight',transparent=True);
plt.close()# %%
