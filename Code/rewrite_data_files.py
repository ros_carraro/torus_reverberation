#%%
"""
Reads the light curves' files which don't have an error column and rewrites them
"""
import numpy as np

#%%
# def nomi oggetti e path
object_name=["IRAS04505-2958","ESO354-G04"]

# define a common path to the light curve files:
p="../Data/Optical/"
lc1=[p+"Iras045B_norm.qdp_b", p+"ESO354_lc_conv_ratiosB_source_all.txt_b"]
lc2=[p+"Iras045V_norm.qdp_b", p+"ESO354_lc_conv_ratiosI_source_all.txt_b"]

lc1_n=[p+"Iras045B_norm.qdp", p+"ESO354_lc_conv_ratiosB_source_all.txt"]
lc2_n=[p+"Iras045V_norm.qdp", p+"ESO354_lc_conv_ratiosI_source_all.txt"]

# define the error percentage value
err_perc=0.05
#%%
# ciclo for sugli oggetti
for i in range(len(object_name)):

   mjd1, flux1 =  np.loadtxt(lc1[i], unpack = True, usecols = [0, 1])
   err1 = err_perc*flux1
   mjd2, flux2  =  np.loadtxt(lc2[i], unpack = True, usecols = [0, 1])
   err2 = err_perc*flux2

   data1 = np.array([mjd1, flux1, err1])
   data1 = data1.T
   data2 = np.array([mjd2, flux2, err2])
   data2 = data2.T

   header='Reprint of the light curve. Errors are calculated as exactly {0}*flux'.format(err_perc)

   with open(lc1_n[i], 'w+') as datafile_id:
      np.savetxt(datafile_id, data1, delimiter=' ', header=header)

   with open(lc2_n[i], 'w+') as datafile_id:
      np.savetxt(datafile_id, data2, delimiter=' ', header=header)

  # np.savetxt(lc1_n[i], (mjd1, flux1, err1) , delimiter=' ', comments=comment)
   #np.savetxt(lc2_n[i], (mjd2, flux2, err2) , delimiter=' ', comments=comment)

#%%
