#%%
"""
Macro to plot the spectrum from 6dfgs of IRAS01267 galaxy source.
"""
import matplotlib.pyplot as plt
from astropy.io import fits
import numpy as np
#%%
p='/Users/Ros/Google_drive/Valparaiso/Light_curves/Code_Py3/Spectrum_IRAS01267/'
filename=p+'./6dFJ0129105-214157_S_VR_j2009.fits'
#%%
# Enrico's way
#from mpdaf.obj import Spectrum, WaveCoord

#Spettro = Spectrum(filename, ext=2)
#Spettro.plot()

#%%
def read_6dfgs(file,index=0):
    # The 1st row is the observed reduced SPECTRUM, 
    # the 2nd row is the associated variance and 
    # the 3rd row stores the SKY spectrum as recorded for each data frame
    # The VR extension also has an additional 4th row that represents the wavelength axis explicitly
    hdu = fits.open(file,memmap=False)
    h = hdu[index].header

    flux=hdu[index].data[0]
    var=hdu[index].data[1]
    sky=hdu[index].data[2]
    if index==2:
      lam = hdu[index].data[3]
    else:
       lam = np.array(h['CRVAL1'] - h['CRPIX1']*h['CDELT1']+ h['CDELT1']*np.arange(h['NAXIS1']),dtype=np.float64)
       #CRVAL1 - (CRPIX1 - pixel no) x CDELT1
    
    hdu.close()
    del hdu
    
    return lam,flux,var,sky
# %%
def moving_average(v, n=3):
   ret = np.cumsum(v, dtype=float)
   ret[n:] = ret[n:]-ret[:-n]
   return ret[n-1:]/n
#%%
# Peppe's way
# hdu[1] R spectrum
# hdu[2] VR spectrum
lam,flux,_,_=read_6dfgs(filename,index=2)
#%%
# entire plot
plt.style.use('seaborn')
fig, ax = plt.subplots(1, 1,figsize=(10,7))
ax.set_ylim(-600,730)
ax.plot(lam,flux,linewidth=0.4);
#OII= 3727
# %%
# Halpha
fig, ax = plt.subplots(1, 1,figsize=(10,7))
ax.set_ylim(0,730)
ax.set_xlim(7000,7500)
ax.plot(lam,flux,linewidth=1);
# %%
# Smoothed plot
n=3
fig, ax = plt.subplots(1, 1,figsize=(10,7))
ax.set_ylim(-600,730)
ax.plot(lam[n-1:],moving_average(flux,n=n),linewidth=0.4);
# %%
