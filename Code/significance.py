#%%
#-*- coding: utf-8 -*-
#!/usr/bin/python
"""
Macro che calcola significanza delle correlazioni trovate. 
Simulo curve con caratteristiche come quelle dei dati (B band). Riduco il sampling a quello delle osservazioni. Studio la correlazione tra 
   1 una curva simulata e quella dei dati (I band)
   NO 2 due curve simulate con lo stesso seed ma diversi parametri della power law
al variare del tau, lag.
Le statistiche delle CCF delle curve simulate mi dicono il livello di significanza della CCF tra le curve dei dati (B and I bands)
"""

import numpy as np
from matplotlib import pyplot as plt
import curve_simul as c_sim

import sys
import argparse
import numpy as np
import scipy
import PYCCF as myccf
from scipy import stats 

import pandas as pd
from utilities import is_outlier

def significance(object_name,lc1,lc2,err1=None,err2=None,output_file_folder='./',nsim=500,bands=['B','H']):
   # if object_name == "ESO354-G04":
   #    err2=0.001
   
# mi servono 2 seed per ogni curva simulata
   seeds=np.arange(nsim*2).reshape(nsim,2)

   if err1 is not None:
      mjd1, flux1  =  np.loadtxt(lc1, unpack = True, usecols = [0, 1])
      err1 = err1*flux1
   else:
      mjd1, flux1, err1  =  np.loadtxt(lc1, unpack = True, usecols = [0, 1, 2])

   if err2 is not None:
      if lc2.endswith('qdp'):
         mjd2, flux2, err2 =  np.loadtxt(lc2, unpack = True, usecols = [0, 1])
      else:
         mjd2, flux2, err2  =  np.loadtxt(lc2, unpack = True, usecols = [2, 3])
         sort_idx = mjd2.argsort()
         mjd2=mjd2[sort_idx]-2450000
         flux2=1/flux2[sort_idx]
         err2=err2[sort_idx]

   else:
      if lc2.endswith('qdp'):
         mjd2, flux2, err2 =  np.loadtxt(lc2, unpack = True, usecols = [0, 1, 2])
      else:
         mjd2, flux2, err2  =  np.loadtxt(lc2, unpack = True, usecols = [2, 3, 4])
         sort_idx = mjd2.argsort()
         mjd2=mjd2[sort_idx]-2450000
         err2=err2[sort_idx]
         if object_name != "IRAS16355-2049":
            flux2=1/flux2[sort_idx]

   average=np.mean(flux1)
   err=np.median(err1)
   start_day=int(max(min(mjd2),min(mjd1)))
   end_day=int(min(max(mjd2),max(mjd1)))
   span=end_day-start_day

   #########################################
   ## Remove outliers
   #########################################
   bad1=is_outlier(flux1, thresh=2.5)
   mjd1=mjd1[np.logical_not(bad1)]
   flux1=flux1[np.logical_not(bad1)]
   err1=err1[np.logical_not(bad1)]

   bad2=is_outlier(flux2, thresh=2.5)
   mjd2=mjd2[np.logical_not(bad2)]
   flux2=flux2[np.logical_not(bad2)]
   err2=err2[np.logical_not(bad2)]

   #########################################
   ##Set Interpolation settings, user-specified
   #########################################
   # I need the cross correlation between the two curves, no MC stuff
   fraction=0.4
   lag_range = [-(span*fraction), span*fraction] #Time lag range to consider in the CCF (days). Must be small enough that there is some overlap between light curves at that shift (i.e., if the light curves span 80 days, these values must be less than 80 days)
   print 'lag range is {0}'.format(lag_range[1])

   interp = 2. #Interpolation time step (days). Must be less than the average cadence of the observations, but too small will introduce noise.

   for i in range(nsim):
      #curva di luce sim i - molto più lunga, 10 Delta T dati.
      times, sim_curve, sim_curve_noise = c_sim.curve_simul(seeds=seeds[i,:],flux_ave=average,flux_err=err)
      N=np.size(times)

      # #%%
      # plt.figure()
      # plt.minorticks_on()
      # plt.plot(times,sim_curve)
      # plt.plot(times,sim_curve_noise)
      # plt.show;

      # #%%
      #sampling - take central part of the sim_curve and sample it
      sim_start=times[N/2]-span/2
      data_shift=int(mjd1[0])-sim_start
      mjd1_shifted=mjd1.astype(int)-data_shift

      #times_sampled=np.array([times[i] for i in range(len(times)) if times[i] in mjd1_shifted])
      times_sampled = np.sort(np.array(list(set(times).intersection(mjd1_shifted))))
      idx=times_sampled.astype(int)-1
      sim_curve_noise_sampled=sim_curve_noise[idx]

      # #%%
      # # plot curva sampled e data
      # plt.figure()
      # plt.minorticks_on()
      # plt.plot(mjd1,flux1)
      # plt.plot(times_sampled+data_shift,sim_curve_noise_sampled)
      # plt.show;

      # #%%
      # pezzo per tutti i tau possibili immaginabili
      #tau=np.arange(obs_dur-obs_dur*0.2,2)

      # #%%
      ##########################################
      #Calculate lag with python CCF program
      ##########################################
      tlag_peak, status_peak, tlag_centroid, status_centroid, ccf_pack, max_rval, status_rval, pval = myccf.peakcent(times_sampled+data_shift,sim_curve_noise_sampled, mjd2, flux2, lag_range[0], lag_range[1], interp)

      lag = ccf_pack[1]
      r = ccf_pack[0]

      # #%%
      #metto via info correlazione - matrice CCF[i,tau]
      if i == 0:
         sim_CCF=pd.DataFrame(columns=lag)
         sim_CCF.loc[i]=r
      else:
         sim_CCF.loc[i]=r


   print sim_CCF.info()
   #%%
   # Cross correlation between the two observed light curves
   tlag_peak, status_peak, tlag_centroid, status_centroid, ccf_pack, max_rval, status_rval, pval = myccf.peakcent(mjd1, flux1, mjd2, flux2, lag_range[0], lag_range[1], interp)

   r = ccf_pack[0]

   #%%
   # Estimate quantiles and add CCF curve to dataframe for plot
   quantiles=sim_CCF.quantile([0.01,0.05,0.95,0.99])
   quantiles=quantiles.T
   quantiles.loc[:,"CCF"]=pd.Series(r, index=quantiles.index)

   #%%
   # Plot all curves and save
   quantiles.index.name = 'Time lag of {1} band behind {0} band [days]'.format(bands[0],bands[1])
   quantiles.plot(title='CCF as a function of lag between {1} and {2} band of {0}'.format(object_name,bands[0],bands[1]))
   plt.ylabel('CCF and significance threshold')
   plt.legend(loc='lower right')
   plt.savefig(output_file_folder+'{0}_CCF_{1}{2}.pdf'.format(object_name,bands[0],bands[1]), format = 'pdf', orientation = 'landscape',transparent=True)


