"""
file that collects info from different files to generate
a latex table to be printed in the thesis
"""
import pandas as pd
import numpy as np
import re
import os
from astropy.table import Table

path = '/users/Ros/Google_drive/Valparaiso/Light_curves/Code_Py3/Carolina/'
#path = '/share/cav/data/rcarraro/Carolina/'

ps_pars=pd.read_csv(path+'LightCurves/info_BHmass_nuB_days.csv',index_col=0,header=0)
ps_pars.drop(['NGC3227','NGC6240'],inplace=True)
ps_pars.drop(['EddRat', 'error_meas_vb_Hz'],inplace=True,axis=1)
#gals excluded from analysis:
ps_pars.drop(['3C445'],inplace=True)

# read table from Carolina's draft
tab = Table.read('/Users/Ros/Google_Drive/Valparaiso/Light_curves/Code_Py3/Carolina/table1_carolinas_draft.tex',format='latex').to_pandas()
tab.replace('--', np.NaN, inplace=True)
tab.set_index('Source',inplace=True)
tab.drop(['logMBH','Ref.'],inplace=True,axis=1)
tab=tab.astype('float')
ps_pars=tab.merge(ps_pars,left_index=True,right_index=True)
ps_pars.loc[ps_pars['meas_vb_Hz'].isnull(),'meas_vb_Hz']=ps_pars.loc[((ps_pars.meas_vb_Hz.isnull()) & (~ps_pars.logLbol.isnull())),'nub']*1e-6
ps_pars.loc[ps_pars['meas_vb_Hz'].isnull(),'meas_vb_days']=ps_pars.loc[ps_pars['meas_vb_Hz'].isnull(),'meas_vb_Hz']*60*60*24
ps_pars.drop(['nub'],inplace=True,axis=1)

new_pars=pd.read_csv(path+'ratio_percentiles_new.csv',index_col=0,header=None,names=['5','16','50','84','95'])
new_pars.drop(['5', '95'],inplace=True,axis=1)

def f_int(x):
   if np.isnan(x):
      return '-'
   else:
      return '%.0f' % x
def e2(x):
   if np.isnan(x):
      return '-'
   else:
      return '%.2e' % x
def g2(x):
   if np.isnan(x):
      return '-'
   else:
      #return "{:#.6g}".format(i)
      return '%#.2g' % (x,)
def g3(x):
   if np.isnan(x):
      return '-'
   else:
      #return "{:#.6g}".format(i)
      return '%#.3g' % (x,)
def exp(x):
   if np.isnan(x):
      return '-'
   else:
      y= '%.2e' % x
      pattern=re.compile('(-?\d.\d+)e(-?\d+)')
      base,exp=re.findall(pattern, y)[0]
      exp= exp.replace('-0', '-') 
      return f"${base}"+"\cdot10^{"+f'{exp}'+'}$'

for gal in ps_pars.index:
   if np.isnan(ps_pars.loc[gal,'meas_vb_days']):
      #print("Galaxy doesn't have power spectrum parameters")
      ps_pars.drop(gal,inplace=True)
   #read light_curve
   lc=pd.read_csv(path+'LightCurves/lightcurves/'+gal+'_lc.csv',header=0,usecols=['T(days)','flux_cont(ph/cm^2/s)','flux_cont_err(ph/cm^2/s)','flux_Fe(ph/cm^2/s)','flux_Fe_err(ph/cm^2/s)'],na_values='ul')
   
   ps_pars.loc[gal,'n_obs']=lc.shape[0]
   ps_pars.loc[gal,'duration'] = lc['T(days)'].max().astype(int) # days

   if ps_pars.loc[gal,'source']=='1':
      ps_pars.loc[gal,'source']='{\citet{summons_thesis}}'
   elif ps_pars.loc[gal,'source']=='2':
      ps_pars.loc[gal,'source']='{\citetalias{2012A&A...544A..80G}}'
   else:
      ps_pars.loc[gal,'source']='Eq.~\ref{Eq:T_b}'
   #print(lc.head())

#print(ps_pars.columns)
#print(new_pars.head())

final_df=new_pars.merge(ps_pars,left_index=True,right_index=True)

#$ -1.28_{-21.2}^{5.96}$ 
final_df = final_df.assign(xi = '$ '+ final_df['50'].map('{:#.2g}'.format) + '_{' + \
  final_df['16'].map('{:#.2g}'.format) +'}^{'+ final_df['84'].map('{:#.2g}'.format) + '} $' )
final_df = final_df[['xi','logMbh(Msun)','logLbol','meas_vb_Hz','meas_vb_days','alpha_h','source','n_obs','duration']]

#gals excluded from analysis:
final_df.drop(['H1821+643','NGC253'],inplace=True)

#header=['Source', "$\xi(50\%)_{16\%}^{84\%}$", '$\log\frac{\rm M_{\rm BH}}{\rm M_\odot}$', '$\nu_b$', '$\nu_b$', '$\alpha_h$', 'Par origin', 'N Obs']

#formatters={'16':g2,'50':g2,'84':g2,'meas_vb_Hz':exp,'meas_vb_days':g2,'duration':f_int,'n_obs':f_int}
formatters={'meas_vb_Hz':exp,'meas_vb_days':g2,'duration':f_int,'n_obs':f_int,'logLbol':g3}
print(final_df.to_latex(formatters=formatters,na_rep='-',escape=False,longtable=True,label='tab:gals_obs_par',caption='lallero',column_format='llrrllrlrr'))