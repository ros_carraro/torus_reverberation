#%%
#-*- coding: utf-8 -*-
#!/usr/bin/python
"""
Macro that does the CCF for observations in two photometric bands.
It calls optical_LC.py to estimate the CCF and centroid distribution.
Then it calls significance_ext.py to estimate the significance of the correlation, by simulating light curves with a similar shape and see if the CCF is above the "casual" threshold.
"""
from centroids import centroids
from significance import significance
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
#%%
bands=['B','J']
#bands=['B','H']
#bands=['B','K']

# def nomi oggetti e path
object_name=["IRAS04505-2958","LB 1727","TON S180","IRAS16355-2049","ESO 354-G004"]
#mags=[-27.1,-25.2,-22.5,-20.8,-20.4] # from Patricia's excel V band
mags=np.array([-27.1,-23,-22.47,-20.8,-20.4]) # from calibrate_fluxes.py B band

output_file_folder="../IR_CCF/"
nsim=500

# define a common path to the light curve files:
p="../Data/Light_curves_tables/Optical/"
pp="../Data/Light_curves_tables/Andrea/"
lc1=[p+"Iras045B_norm.qdp", p+"bin_LB1727_B.qdp", p+"bin_TONS180_B.qdp", p+"bin_IRAS16_B.qdp", p+"bin_ESO354_B.qdp"]

if bands[1] == 'H':
   galaxies=pd.DataFrame(index=object_name)
   lc2=[pp+"IRAS04505_H.flux",pp+"LB1727_H.flux",pp+"bin_TONS180_H.qdp",pp+"IRAS16355_H.flux",pp+"bin_ESO354_H.qdp"]
   galaxies.loc[:,"B"]=lc1
   galaxies.loc[:,"H"]=lc2
elif bands[1]=='J':
   object_name=object_name[1:]
   mags=mags[1:]
   galaxies=pd.DataFrame(index=object_name)
   lc2=[pp+"LB1727.fluxJ",pp+"bin_TONS180_J.qdp",pp+"IRAS16355.fluxJ",pp+"bin_ESO354_J.qdp"]
   galaxies.loc[:,"B"]=lc1[1:]
   galaxies.loc[:,"J"]=lc2
elif bands[1]=='K':
   object_name=object_name[2:]
   mags=mags[2:]
   galaxies=pd.DataFrame(index=object_name)
   lc2=[pp+"bin_TONS180_K.qdp",pp+"IRAS16355_K.flux",pp+"bin_ESO354_K.qdp"]
   galaxies.loc[:,"B"]=lc1[2:]
   galaxies.loc[:,"J"]=lc2
else:
   lc2=[p+"Iras045V_norm.qdp", p+"LB1727_I.qdp", p+"Mrk509_I_bin.qdp", p+"TONS180_I.qdp", p+"bin_IRAS16_I.qdp", p+"ESO354_lc_conv_ratiosI_source_all.txt"]

# in case I want to manually define the error in percentage % units (does not work with None)
#err1=[0.5,0.5,None,0.5,0.5,0.5]
#err2=[0.5,0.5,None,None,0.5,0.5]
# galaxies.loc[:,"err1"]=err1
# galaxies.loc[:,"err2"]=err2

#%%
# ciclo for sugli oggetti
lag=np.zeros([len(galaxies.iloc[:,0]),3])

for i,row in enumerate(galaxies.itertuples()): 
   # chiamo test.py che mi calcola la CCF, distr centroide etc e mi dà alcuni parametri utili per dopo (obs_time, etc)
   lag[i] =centroids(*row,output_file_folder=output_file_folder,nsim=nsim,bands=bands)

   # chiamo significance_extimation e vedo se la CCF è good enough - c'è un oggetto che ha bisogno che gli traslo le date
   significance(*row,output_file_folder=output_file_folder,nsim=nsim,bands=bands)

#%%
# lags=pd.DataFrame(lag[:,0],index=mags,columns='lag')
# lags.insert(loc=0,column='source',value=object_name)
# #lags.loc['mv',:]=pd.Series(['source',lag,upper,lower])
# errors=pd.DataFrame(lag[:,2:0],index=mags,columns=['lower','upper'])

#%%
# Adding objects from previous papers
obs=['NGC 3783','MR 2251-178','MCG-6-30-15']
# more_mags=[-19.2,-22.75,-18.6] # from NED search V band
more_mags=np.array([-19.,-22.05,-16.3]) #from calibrate_fluxes.py B band

# centroid median, lower, upper errors
if bands[1] == 'H':
   more_lags=np.array([[66.0,6.5,6.],[-2.1,4.4,4.2],[16.7,4.2,3.7]]) # B/H lag
elif bands[1]=='J':
   more_lags=np.array([[40.8,10.8,5.1],[9,3.5,4.],[11,2.9,3]]) # B/J lag
elif bands[1]=='K':
   more_lags=np.array([[76.3,17.2,10.9],[np.nan,np.nan,np.nan],[18.5,3.6,3.8]]) # B/K lag

object_name=object_name+obs
lag=np.vstack((lag,more_lags))
mags=np.append(mags,more_mags)
#mags=mags+more_mags
#mags=np.asarray(mags)
object_name=np.asarray(object_name)
#%%
idx=mags > -20
nidx=mags < -20
axes_size=16
name_size=10
# for a global change use this instead of with...context
#plt.style.use(['seaborn-pastel','seaborn-paper'])
#print(plt.style.available)
with plt.style.context(['seaborn-white','seaborn-bright']):
   fig, ax =plt.subplots()
   ax.scatter(mags[idx],lag[idx,0])
   ax.errorbar(mags[idx],lag[idx,0], yerr=lag[idx,1:].T, fmt='o', ms=3)
   ax.scatter(mags[nidx],lag[nidx,0])
   ax.errorbar(mags[nidx],lag[nidx,0], yerr=lag[nidx,1:].T, fmt='o', ms=3)
   plt.xlabel('Absolute B mag', fontsize=axes_size)
   plt.ylabel('Lag {0} and {1} band (days)'.format(bands[0],bands[1]), fontsize=axes_size)
   plt.ylim(-100,300)
   if bands[1]=='H':
      for i, txt in enumerate(object_name):
         if mags[i] == max(mags):
            ax.annotate(txt, (mags[i]-2.1,lag[:,0][i]-25),size=name_size)
         elif txt == object_name[2]:
            ax.annotate(txt, (mags[i]-1.9,lag[:,0][i]-25),size=name_size)
         elif txt == object_name[-2]:
            ax.annotate(txt, (mags[i]+0.15,lag[:,0][i]-4),size=name_size)
         else:
            ax.annotate(txt, (mags[i]+0.05,lag[:,0][i]+5),size=name_size)
   elif bands[1]=='J':
      for i, txt in enumerate(object_name[idx]):
         if mags[idx][i] == max(mags[idx]):
            ax.annotate(txt, (mags[idx][i]-1.5,lag[idx,0][i]-10),size=name_size)
         elif txt == object_name[-2]:
            ax.annotate(txt, (mags[idx][i]-0.3,lag[idx,0][i]-10),size=name_size)
         else:
            ax.annotate(txt, (mags[idx][i]+0.05,lag[idx,0][i]+3),size=name_size)
   fig.savefig(output_file_folder+'Lags_{0}{1}.pdf'.format(bands[0],bands[1]), format = 'pdf', orientation = 'landscape',transparent=True)


#%%
