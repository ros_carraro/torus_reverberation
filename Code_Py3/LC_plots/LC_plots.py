#%%
#!/usr/bin/python
#-*- coding: utf-8 -*-
"""
Macro inspired by the macro in ../Code/ to plot all the light curves 
of the same galaxy together.

Makes plots of the light curves in all available bands
for each object
"""

import glob
import os
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from astropy.table import Table
sys.path.append('/Users/Ros/Google_Drive/Valparaiso/Light_curves/')
#from utilities import is_outlier
#%%
names_dic={'ESO244':"ESO244-G17", 'ESO354':"ESO354-G04",'ESO434':"ESO434-G40", 'HE0502':"HE0502-2948", 'IRAS01089':"IRAS01089-4743", 
          'IRAS01267':"IRAS01267-2157", 'IRAS04505':"IRAS 04505-2958", 'IRAS16355':"IRAS 16355-2049", 'IRASL06229':"IRASL06229-6434", 
           'LB1727':"LB1727", 'MRK509':"MRK 509", 'PKS0521':"PKS0521-36", 'TONS180':"TON S 180"}
gal_name=names_dic.get(gal)
#%%
#input paths
p='/Users/Ros/Google_Drive/Valparaiso/Light_curves/'
optical_path=p+'Data/Optical_light_curves_tables/'
# Find all the galaxies that have optical light curves
gals_list=next(os.walk(lc_path))[1]
gals_list.remove('TONS180')

ir_path=p+'Data/My_light_curves_tables/'

# output path
output_file_folder=p+"Code_Py3/LC_plots/plots/"

bands={'B':445,'V':551,'R':658,'I':806,'Y':1020,'J':1220,'H':1630,'K':2190}
#%%
print(gals_list)
for gal in gals_list:
   optical_LC_list=glob.glob(optical_path+gal+'/LC_cal_*.txt')
   optical_bands_list=[el[-5:-4] for el in optical_LC_list]
   optical_bands_list.sort(key=lambda x: bands[x])
   print(optical_bands_list)

   irbands_list=next(os.walk(ir_path+gal))[1]
   irbands_list=[el for el in irbands_list if len(el)==1]
   irbands_list.sort(key=lambda x: bands[x])
   print(irbands_list)
   IR_LC_list=[ir_path+gal+'/'+band+'/LC.txt' for band in irbands_list]

   if gal=='IRAS04505':
      irbands_list.remove('K')
      irbands_list.remove('Y')
      IR_LC_list=[el for el in IR_LC_list if not ('/K/' or '/Y/') in el]
      #IR_LC_list=[el for el in IR_LC_list if not '/Y/' in el]

   with plt.style.context(['seaborn-white','seaborn-bright','seaborn-ticks']):
      #print(obj)
      labels=optical_bands_list+irbands_list
      print(labels)
      dm = 4

      fig = plt.figure()
      ax1 = fig.add_subplot(111)
      ax1.minorticks_on()
      ax1.tick_params(which='both',direction='in',top=True,right=True)
      #ax1.set_title('{0}'.format(object_name[i]), size=20)#,fontweight="bold"

      #for k, column in enumerate(galaxies):

      for lc_file,band in zip(optical_LC_list,optical_bands_list):
         LC = pd.read_csv(lc_file,sep='\t',header=0,index_col=0)

         LC['flux'].plot(style='.')

         #plt.errorbar(np.array(LC['mjd']), LC['mean_flux'], yerr=LC['flux_diff'], fmt='o', ms=3)

      for lc_file, band in zip(IR_LC_list,irbands_list):
         LC=Table.read(lc_file, format='ascii')

         plt.errorbar(np.array(LC['JD']), LC['mean_flux'], fmt='o', ms=3) #, yerr=LC['flux_diff']


            #LC=Table.read(galaxies.loc[obj,column],format='ascii',names=('mjd', 'flux', 'err'))
            #norm_flux=np.array((LC['flux']-np.mean(LC['flux']))/np.std(LC['flux']))
            #plt.errorbar(np.array(LC['mjd']), k*dm+norm_flux, yerr=LC['err'], fmt='o', ms=3)


      ax1.legend(labels, frameon= True)
            
      axes_size=12
      plt.xlabel('mjd-2450000.5', fontsize=axes_size)
      plt.ylabel('Flux (Jy)', fontsize=axes_size)

      plt.savefig(output_file_folder+'light_curves_{0}.pdf'.format(gal), format = 'pdf', orientation = 'landscape', bbox_inches = 'tight',transparent=True) 
      plt.close(fig)
      fig


#%%
